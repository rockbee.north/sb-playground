{
    "expertReview": null,
    "rating": {
        "average": 3.7,
        "reviewCount": 32,
        "reviews": {
            "1": 3,
            "2": 3,
            "3": 7,
            "4": 7,
            "5": 12
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2016-01-05T01:50:29.524",
            "dislikeCount": 0,
            "id": 17295,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I like it! There's definitely a powdery undertone I could do without but it's still nice. I have to keep going to my daughters room to get it back lol!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 212027,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 9,
                "url": "/p/212027",
                "username": "Rose"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-02T21:37:29.009",
            "dislikeCount": 0,
            "id": 16957,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I'm in love with this floral fragrance. Definitely makes me feel feminine and flirty.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12372449811448712182398.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 163227,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 2,
                "url": "/p/163227",
                "username": "Reun"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-30T15:02:34.298",
            "dislikeCount": 0,
            "id": 15302,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Not my favorite but it's nice",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12158952501460006572719.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 194021,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/194021",
                "username": "Marquita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-29T17:02:49.987",
            "dislikeCount": 0,
            "id": 14639,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Hate it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 83110,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/83110",
                "username": "Dawn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-22T04:07:30.374",
            "dislikeCount": 0,
            "id": 14143,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "A great rose scent with a modern twist. If you love floral, rose perfume you will love this!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11524239961448830607433.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 218699,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 0,
                "url": "/p/218699",
                "username": "Judy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-19T00:32:11.838",
            "dislikeCount": 0,
            "id": 13988,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I love floral scents, but this isn't for me. My first impression is it smelled like a bathroom...a clean bathroom, but still a bathroom. Artificial, forced, unappetizing, and not unique.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 190617,
                "likeTotal": 9,
                "reviewsTotal": 12,
                "shippedCount": 5,
                "url": "/p/190617",
                "username": "Breana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-06T00:12:29.445",
            "dislikeCount": 0,
            "id": 12948,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not a fan.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 84027,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 3,
                "url": "/p/84027",
                "username": "Margie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-04T23:04:56.347",
            "dislikeCount": 0,
            "id": 12683,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "A little bit spicey for me.  Goes on very strong but wears off quickly.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 217135,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/217135",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-02T00:51:49.622",
            "dislikeCount": 0,
            "id": 12099,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Just meh, but I'm not a flowery type of person.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 28589,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 36,
                "url": "/p/28589",
                "username": "Maggie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-29T20:53:50.812",
            "dislikeCount": 0,
            "id": 11551,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "very nice scent but doesn't seem to have that much staying power.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20397188151450378689116.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 204280,
                "likeTotal": 11,
                "reviewsTotal": 8,
                "shippedCount": 5,
                "url": "/p/204280",
                "username": "Khia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-25T06:35:16.437",
            "dislikeCount": 0,
            "id": 11167,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Very strong floral scent. If you spray this product sparingly, it is absolutely beautiful. The scent does not linger on the skin--so no issues with smelling it all day. I love it for an evening scent. Not my favorite floral scent, but definitely not the worst.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 194492,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 1,
                "url": "/p/194492",
                "username": "Yashica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-24T21:05:26.295",
            "dislikeCount": 0,
            "id": 10735,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Loved this scent. It blended well with my chemistry as the day went on.?",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 179938,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/179938",
                "username": "Scheria"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-23T01:04:02.727",
            "dislikeCount": 0,
            "id": 10474,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "My favorite scent is an older one called \"Royal Secret\" so scents that are similar to this would be good for me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-3270714431474505355810.JPG",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 226225,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/226225",
                "username": "Eve"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-30T22:23:57.429",
            "dislikeCount": 0,
            "id": 8133,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This perfume smells amazing :)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 164826,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 0,
                "url": "/p/164826",
                "username": "donna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-29T19:14:24.787",
            "dislikeCount": 0,
            "id": 7102,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "so light and floral. It has a lighter scent than the very irresistible I have used.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 43720,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/43720",
                "username": "Mallory"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-03T01:40:18.078",
            "dislikeCount": 0,
            "id": 4979,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 122448,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/122448",
                "username": "Amanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T23:30:35.166",
            "dislikeCount": 0,
            "id": 4500,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This smelled nice.  It has a traditional floral scent to it.  It was not too strong but lasted throughout the day.  Not my favorite, but definitely something I will wear while it lasts.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 66127,
                "likeTotal": 11,
                "reviewsTotal": 11,
                "shippedCount": 16,
                "url": "/p/66127",
                "username": "Letitia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T14:50:34.519",
            "dislikeCount": 0,
            "id": 4266,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not a fan, the perfume does not last.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 152369,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/152369",
                "username": "Cassendre"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T13:29:53.201",
            "dislikeCount": 0,
            "id": 4210,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this for summer into fall.. it isn't too sweet a rose and dries down powdery.. not too light.. great for day.. it holds fairly long..",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 120105,
                "likeTotal": 23,
                "reviewsTotal": 17,
                "shippedCount": 28,
                "url": "/p/120105",
                "username": "cindi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-29T23:29:22.087",
            "dislikeCount": 0,
            "id": 3377,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Definitely a super clean floral scent.  If you like other Givenchy scents, this one won't disappoint.  Definitely not one of my all-time favorite scents (personally I am obsessed with Viktor & Rolf Flowerbomb, J'adore by Dior--just to name a couple for comparison), but I do enjoy it all the same.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 143198,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/143198",
                "username": "Melissa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-07T20:38:49.417",
            "dislikeCount": 0,
            "id": 2488,
            "isUseful": null,
            "likeCount": 6,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "As with other Givenchy perfumes, the first description that comes to mind is \"ladylike\". The scent feels feminine and refined. The rose note is very central for me, with lemon and verbena accenting, but the overall scent is definitely floral, not fruity or herbal. There isn't a ton of throw-- it mostly stays pretty close to the skin. While I think this is a nice fragrance, it's not for me-- I associate the scent with a lovely woman, but someone I don't want to imitate.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 75722,
                "likeTotal": 64,
                "reviewsTotal": 26,
                "shippedCount": 33,
                "url": "/p/75722",
                "username": "Alysson"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-01T14:41:58.007",
            "dislikeCount": 0,
            "id": 2082,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I really love the way this perfume smell, it's one of my favorite perfume.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 51615,
                "likeTotal": 7,
                "reviewsTotal": 6,
                "shippedCount": 8,
                "url": "/p/51615",
                "username": "Marie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-01T14:27:21.354",
            "dislikeCount": 0,
            "id": 2081,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this one, but I seem to have a definite affinity to Givenchy fragrances. It's light enough to wear during the day, but has good lasting power on my skin.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 33790,
                "likeTotal": 21,
                "reviewsTotal": 15,
                "shippedCount": 19,
                "url": "/p/33790",
                "username": "Amber"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-31T04:07:18.571",
            "dislikeCount": 0,
            "id": 1921,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love this smell :-)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 74158,
                "likeTotal": 7,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/74158",
                "username": "Cherie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T16:34:31.246",
            "dislikeCount": 0,
            "id": 1553,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It was initially stronger than I thought I would like, but it mellowed to a very nice scent that I ended up wearing regularly.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 77964,
                "likeTotal": 6,
                "reviewsTotal": 8,
                "shippedCount": 10,
                "url": "/p/77964",
                "username": "Heather"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-21T02:00:30.333",
            "dislikeCount": 0,
            "id": 1009,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "This such an easy to wear scent for me. The floral note is gentle and the underlying fresh citrus smell is wonderful.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 97219,
                "likeTotal": 7,
                "reviewsTotal": 4,
                "shippedCount": 8,
                "url": "/p/97219",
                "username": "Lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-07T16:21:12.005",
            "dislikeCount": 0,
            "id": 742,
            "isUseful": null,
            "likeCount": 3,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Citrus fresh smell. Not overwhelming. Pleasant",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 81138,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/81138",
                "username": "Julia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T15:59:00.59",
            "dislikeCount": 0,
            "id": 492,
            "isUseful": null,
            "likeCount": 4,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This is just ok. Something about it smells like an old lady so I will not be wearing this very often.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 41744,
                "likeTotal": 4,
                "reviewsTotal": 1,
                "shippedCount": 9,
                "url": "/p/41744",
                "username": "Lauren "
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T01:42:14.948",
            "dislikeCount": 0,
            "id": 246,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I absolutely loved this scent!  Very fresh and floral with a hint of citrus.  It's a great scent for the person who loves to smell good without being overwhelming.  It's good for the workplace, lunch dates and spring/summer.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 45091,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/45091",
                "username": "Danada"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T01:20:55.896",
            "dislikeCount": 1,
            "id": 231,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "This is an absolutely delightful scent; can't get enough of it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 60623,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/60623",
                "username": "Ami"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-07-22T16:53:51",
            "dislikeCount": 0,
            "id": -260,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this scent. It's not a single pure rose scent but it's delicious and romantic. I am definitely considering by the full size.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 81424,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 5,
                "url": "/p/81424",
                "username": "Joyce"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-06-20T23:30:42",
            "dislikeCount": 0,
            "id": -126,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"My first scent is...delightfully fresh, crisp, but also evokes the elegant, rich tones I admired on women I would pass in the mall as a young girl. Fantastic first month! I cannot wait to collect more.\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 71339,
                "likeTotal": 16,
                "reviewsTotal": 4,
                "shippedCount": 14,
                "url": "/p/71339",
                "username": "Anita"
            },
            "userAge": null
        }
    ],
    "userReview": null
}