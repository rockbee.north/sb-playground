{
    "expertReview": null,
    "rating": {
        "average": 3.2,
        "reviewCount": 66,
        "reviews": {
            "1": 11,
            "2": 10,
            "3": 15,
            "4": 13,
            "5": 17
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2016-08-04T12:33:53.453",
            "dislikeCount": 0,
            "id": 53040,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I never received this",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 456941,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 9,
                "url": "/p/456941",
                "username": "malikah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-02T06:12:42.151",
            "dislikeCount": 0,
            "id": 51876,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Oh my this is, cleam smelling and sexy all in 1! The scent last All DAY!! So much different from CK1, I love this smell!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 458022,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/458022",
                "username": "Kimberly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T19:21:59.854",
            "dislikeCount": 0,
            "id": 50632,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The scent was too strong i really did not like it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 315840,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/315840",
                "username": "Tawawa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-08T12:16:06.658",
            "dislikeCount": 0,
            "id": 46997,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "My only complaint about this one is sometimes it seems a bit too masculine. Most of the time though, I like it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 438115,
                "likeTotal": 0,
                "reviewsTotal": 4,
                "shippedCount": 3,
                "url": "/p/438115",
                "username": "Amy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-06T16:23:30.863",
            "dislikeCount": 0,
            "id": 46401,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I did not like it at all...it was bug replet.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 407122,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 10,
                "url": "/p/407122",
                "username": "Constance"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-30T16:38:13.705",
            "dislikeCount": 0,
            "id": 42213,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this fresh light scent- perfect for summer",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 269416,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 17,
                "url": "/p/269416",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-30T10:38:33.274",
            "dislikeCount": 0,
            "id": 42098,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Great!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 338624,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/338624",
                "username": "Sade"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-07T11:38:43.448",
            "dislikeCount": 0,
            "id": 40035,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The smell is light yet spicy. Good daytime fragrance.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 2,
                "gender": "female",
                "id": 45203,
                "likeTotal": 8,
                "reviewsTotal": 10,
                "shippedCount": 33,
                "url": "/p/45203",
                "username": "Cindy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-07T04:32:05.105",
            "dislikeCount": 0,
            "id": 40003,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Very old lady. Do not like this one",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 448761,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 24,
                "url": "/p/448761",
                "username": "Julie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T22:55:15.564",
            "dislikeCount": 0,
            "id": 38906,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "A hint of sweetness but not my favorite.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 404207,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/404207",
                "username": "Gerand"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T19:40:17.591",
            "dislikeCount": 0,
            "id": 38717,
            "isUseful": null,
            "likeCount": 0,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 341734,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 12,
                "url": "/p/341734",
                "username": "Jasmine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T10:41:29",
            "dislikeCount": 0,
            "id": 38125,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Clean fragrance, I could see myself wearing this just not all the time!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9177169031469372547020.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 324413,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/324413",
                "username": "Evita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-02T17:24:22.291",
            "dislikeCount": 0,
            "id": 37014,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not my favorite scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 335187,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/335187",
                "username": "Sara"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-02T01:24:10.735",
            "dislikeCount": 0,
            "id": 36807,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Smells good.... a bit like cucumbers a bit like baby powder.  It made my allergies act up big time and haven't stopped sneezing since I put it on though.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13901516751465846732241.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 441353,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/441353",
                "username": "mONICA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-31T19:44:28.174",
            "dislikeCount": 0,
            "id": 36278,
            "isUseful": null,
            "likeCount": 0,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Typically like all Calvin Klein scents.  While this is different from the others it is still reminiscent of previous CK scents.  Something distinctive about CK frangrances.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 282749,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 26,
                "url": "/p/282749",
                "username": "Tracy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-23T15:37:32.81",
            "dislikeCount": 0,
            "id": 35257,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Enjoyed this one. It was a little stronger than the perfumes I'm use to though",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 341896,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/341896",
                "username": "Rebecca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-16T03:15:32.691",
            "dislikeCount": 0,
            "id": 34660,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It smell ok, however, it did not mix well with the chemistry of my body. \"Thanks Scentbird I was going to purchase this in January\" now I know it's not for me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7850111261463368963904.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 291227,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 9,
                "url": "/p/291227",
                "username": "Sherri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-14T15:47:34.925",
            "dislikeCount": 0,
            "id": 34611,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Beautiful scent. Not over powering but not too light. Love it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 382469,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 11,
                "url": "/p/382469",
                "username": "Philonise"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-07T19:49:25.087",
            "dislikeCount": 0,
            "id": 34153,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I did not really like this scent. A co worker said it smelled very \"old lady\". It's got a classic smell, not sweet or fruity at all, which I prefer.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 302483,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 12,
                "url": "/p/302483",
                "username": "Diana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-06T14:51:23.151",
            "dislikeCount": 0,
            "id": 33849,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It's not bad at all, but the scent is a bit too strong for me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16814317371466625754552.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 336454,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 15,
                "url": "/p/336454",
                "username": "Ann"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-06T01:39:31.32",
            "dislikeCount": 0,
            "id": 33704,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I Love this perfume !",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 305821,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/305821",
                "username": "Brittany"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-04T19:53:24.384",
            "dislikeCount": 0,
            "id": 33174,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I enjoy this scent, but it tends to wear off quickly. I got CK Reveal last time and loved it, it lasted much longer.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 312721,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 5,
                "url": "/p/312721",
                "username": "MARY"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-02T20:37:57.257",
            "dislikeCount": 0,
            "id": 32421,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Not my scent, it was \ntoo stout for me",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 343091,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/343091",
                "username": "Simonia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-01T22:02:38.44",
            "dislikeCount": 0,
            "id": 32159,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I love this scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 338127,
                "likeTotal": 3,
                "reviewsTotal": 5,
                "shippedCount": 13,
                "url": "/p/338127",
                "username": "Triana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-30T03:53:50.983",
            "dislikeCount": 0,
            "id": 31680,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I hadn't smelled this before I got it through Scentbird, but it was recommended to me. It smells mature and \"spicy.\" It kind of reminds me of a perfume older women would wear on date night. You know how some scents smell young and fruity? This is not like that. I would say it smells like more of an occasion perfume.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-12037731101461988491425.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 352791,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 18,
                "url": "/p/352791",
                "username": "Emily"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-29T22:23:49.909",
            "dislikeCount": 0,
            "id": 31576,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I dont like it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 327675,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/327675",
                "username": "Lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-27T03:02:03.575",
            "dislikeCount": 0,
            "id": 30986,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Something very different to my taste but I actually like that woodsy smell it has to it.. I love discovering new scents",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 318181,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 1,
                "url": "/p/318181",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-15T19:34:05.282",
            "dislikeCount": 0,
            "id": 30368,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I love this. It smells fresh, clean, and just slightly woodsy. I only wish it lasted a little longer.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-11205511881460748874537.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 339288,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/339288",
                "username": "Katy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-08T02:58:37.99",
            "dislikeCount": 0,
            "id": 29888,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It smells like... Plants. That is all that I have to say about it. I am not talking about freshness of plants either... When I put it on, all I can think of is green plants. If you like to smell like green plants, then this perfume is for you.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 106287,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/106287",
                "username": "Deborah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-05T01:46:31.556",
            "dislikeCount": 0,
            "id": 29215,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is a clean simple smell.  It's nice for everyday but not a signature scent I would say.  It doesn't last long though.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-15688555351459820334025.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 113156,
                "likeTotal": 23,
                "reviewsTotal": 13,
                "shippedCount": 7,
                "url": "/p/113156",
                "username": "Mari"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-01T11:38:13.043",
            "dislikeCount": 0,
            "id": 28677,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Wasn't bad, but a little too woodsy for me.  Wouldn't consider it a spring/summer scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 312322,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 12,
                "url": "/p/312322",
                "username": "Jenn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-31T02:32:08.779",
            "dislikeCount": 0,
            "id": 28357,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Loved it!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 283060,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/283060",
                "username": "Yolanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-30T01:14:06.978",
            "dislikeCount": 0,
            "id": 27580,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Loved this smooth scent :-)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 298883,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 9,
                "url": "/p/298883",
                "username": "MaryAnn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T22:44:38.873",
            "dislikeCount": 0,
            "id": 27319,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Nice scent. Many compliments",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 307525,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/307525",
                "username": "Brandy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T22:19:59.207",
            "dislikeCount": 0,
            "id": 27260,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It smells like flowers and reminds me of my childhood! Loved the scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 309065,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 9,
                "url": "/p/309065",
                "username": "Shannon"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T15:47:40.923",
            "dislikeCount": 0,
            "id": 26194,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Nice fragrance.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 248152,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 2,
                "url": "/p/248152",
                "username": "Terri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-28T19:14:21.202",
            "dislikeCount": 0,
            "id": 25998,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this scent.  Very woodsy and natural but feminine.  Doesn't last long though.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 343217,
                "likeTotal": 7,
                "reviewsTotal": 9,
                "shippedCount": 4,
                "url": "/p/343217",
                "username": "Karla"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-19T21:27:28.046",
            "dislikeCount": 0,
            "id": 25535,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Not a fan! Too woodsy.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 313065,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 12,
                "url": "/p/313065",
                "username": "Ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-18T01:39:46.744",
            "dislikeCount": 0,
            "id": 25401,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Clean fresh smell. Great for daytime.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 208319,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/208319",
                "username": "Erica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-04T23:48:54.141",
            "dislikeCount": 0,
            "id": 24324,
            "isUseful": null,
            "likeCount": 4,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                }
            ],
            "text": "From the desctiptions and reviews I expected more from this one. A nondescript opening that vanished to nothing after half an hour, despite generous application.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 69525,
                "likeTotal": 7,
                "reviewsTotal": 9,
                "shippedCount": 31,
                "url": "/p/69525",
                "username": "Lee"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-29T22:36:58.42",
            "dislikeCount": 0,
            "id": 23148,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I absolutely hate this!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 155388,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/155388",
                "username": "Mary"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-18T23:08:58.82",
            "dislikeCount": 0,
            "id": 22307,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I DO NOT like this fragrance.  Not sure what it is that I find so unappealing but it is definitely NOT FOR ME.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 138854,
                "likeTotal": 9,
                "reviewsTotal": 5,
                "shippedCount": 7,
                "url": "/p/138854",
                "username": "Daizy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-17T16:49:25.112",
            "dislikeCount": 0,
            "id": 22173,
            "isUseful": null,
            "likeCount": 4,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "HATED THIS, way to heavy and too strong and smells like older ladies.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 255598,
                "likeTotal": 5,
                "reviewsTotal": 4,
                "shippedCount": 2,
                "url": "/p/255598",
                "username": "cHARLENE"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-17T13:08:52.844",
            "dislikeCount": 0,
            "id": 22156,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It's a very soft smell. Almost powdery. Not my style at all. I'll be passing this on to my mom. Doesn't smell bad, just not for me",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 241897,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 0,
                "url": "/p/241897",
                "username": "Rhonda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-08T19:43:29.494",
            "dislikeCount": 0,
            "id": 21939,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Too heavy/strong for me",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 259796,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/259796",
                "username": "Danielle"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-03T19:31:10.218",
            "dislikeCount": 0,
            "id": 20928,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I don't hate it but I don't love it either.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 138854,
                "likeTotal": 9,
                "reviewsTotal": 5,
                "shippedCount": 7,
                "url": "/p/138854",
                "username": "Daizy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-29T18:47:33.886",
            "dislikeCount": 0,
            "id": 18743,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "The drydown is wonderful.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 259071,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 0,
                "url": "/p/259071",
                "username": "Shilpa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-07T16:00:20.023",
            "dislikeCount": 0,
            "id": 17831,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I didn't realize that this was a very flowery scent. Not so much for me, but once the scent faded a bit it was actually not so bad.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 233401,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/233401",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-31T01:44:35.139",
            "dislikeCount": 0,
            "id": 16310,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 228018,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 18,
                "url": "/p/228018",
                "username": "Terra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-30T16:06:56.576",
            "dislikeCount": 0,
            "id": 15398,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Beautiful scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 63059,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/63059",
                "username": "Latesha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-30T10:28:09.855",
            "dislikeCount": 0,
            "id": 15008,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Loved!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 62320,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/62320",
                "username": "Juanita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-18T21:27:33.158",
            "dislikeCount": 0,
            "id": 13973,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I went old school..this was one that I loved a long time ago, so wasn't sure I still would.. but I do! It's sexy and sweet at the same time",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 184949,
                "likeTotal": 8,
                "reviewsTotal": 6,
                "shippedCount": 9,
                "url": "/p/184949",
                "username": "Michele"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-08T13:59:32.806",
            "dislikeCount": 0,
            "id": 13444,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This perfume is amazing. I'm actually going to be buying the big bottle. It is stronger than I normally like but it works so well with my body chemistry. My only complaint is that it doesn't last very long",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 157443,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/157443",
                "username": "Karlee"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-03T14:45:56.394",
            "dislikeCount": 0,
            "id": 12375,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Great smell",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 201172,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 0,
                "url": "/p/201172",
                "username": "kenya"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-29T20:01:56.932",
            "dislikeCount": 0,
            "id": 11542,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Was my first scentbird perfume and I'm happy I chose this. Smells very fresh then tranforms into a cuddly warm scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 30632,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 20,
                "url": "/p/30632",
                "username": "Cherryl"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-25T23:30:57.554",
            "dislikeCount": 0,
            "id": 11287,
            "isUseful": null,
            "likeCount": 5,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This is one of my old time favorites. I'm very picky with scents and I found women that love sweet synthetic scents hate this one, is not mature at all but sophisticated.\nIs a successful composition of scents: rich but light, elegant but wearable.. The greens balance the flower notes with a good staying power.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/4028605071451268444669.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 204425,
                "likeTotal": 37,
                "reviewsTotal": 11,
                "shippedCount": 22,
                "url": "/p/204425",
                "username": "Itzia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-24T20:56:51.024",
            "dislikeCount": 0,
            "id": 10721,
            "isUseful": null,
            "likeCount": 2,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Honestly I thought it was going to be chaos and not truth. Truth is an old fragrance that smells a little \"out of style.\" Sorry.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 79613,
                "likeTotal": 4,
                "reviewsTotal": 5,
                "shippedCount": 15,
                "url": "/p/79613",
                "username": "Bliss-Ann"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-17T00:55:44.182",
            "dislikeCount": 0,
            "id": 10041,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Loving this one! It starts out a little heavy on the floral, but once it develops it goes much more into the mellow woody. It really is a refreshing scent and my husband and a few friends have already complimented me on this! Awesome day to day scent that's not to overpowering and would layer nicely with my other perfumes.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20341356631441319042353.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 106377,
                "likeTotal": 11,
                "reviewsTotal": 7,
                "shippedCount": 3,
                "url": "/p/106377",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-30T01:07:29.611",
            "dislikeCount": 0,
            "id": 7560,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I did not really like it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 103808,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/103808",
                "username": "Casey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-06T15:56:47.376",
            "dislikeCount": 0,
            "id": 5582,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It was a nice smell",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 106408,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/106408",
                "username": "lilliane"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T00:39:02.206",
            "dislikeCount": 0,
            "id": 3603,
            "isUseful": null,
            "likeCount": 3,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Ugh. I can't imagine waking up and wanting to smell like \"generic white lady,\" but if I did, this would be perfect. Bonus: it /lingers/ like no other perfume I've ever experienced. If only it had any discernible personality.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 51183,
                "likeTotal": 14,
                "reviewsTotal": 5,
                "shippedCount": 8,
                "url": "/p/51183",
                "username": "Karen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-29T19:38:40.02",
            "dislikeCount": 0,
            "id": 3213,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "This was in my wardrobe as a scent i might like and it smelled like an older woman's perfume even though my husband said it smelled great i myself did not like it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 116180,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/116180",
                "username": "Tonya "
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-24T23:44:34.906",
            "dislikeCount": 0,
            "id": 2934,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                }
            ],
            "text": "I did not like this scent.  It smelled like a departmet store.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 128848,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 1,
                "url": "/p/128848",
                "username": "arian"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-24T23:43:52.641",
            "dislikeCount": 0,
            "id": 2933,
            "isUseful": null,
            "likeCount": 1,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I did not like this scent.  It smells like a department store.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 128848,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 1,
                "url": "/p/128848",
                "username": "arian"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T22:20:29.113",
            "dislikeCount": 0,
            "id": 1698,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 33,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sweet.svg",
                    "name": "Sweet"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Fairly sweet and vanilla-heavy",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 41441,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/41441",
                "username": "Bettina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T13:32:40.139",
            "dislikeCount": 0,
            "id": 1405,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It's a lovely scent. It's not as complex, or seductive as I'd like and it seems to wear off quickly. I do think it's a good fragrance for day time.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 44326,
                "likeTotal": 7,
                "reviewsTotal": 2,
                "shippedCount": 29,
                "url": "/p/44326",
                "username": "Mandilyn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T15:01:06.436",
            "dislikeCount": 0,
            "id": 470,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                }
            ],
            "text": "very nice frangrance",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 52698,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/52698",
                "username": "Elizabeth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T01:19:42.627",
            "dislikeCount": 0,
            "id": 230,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Lovely scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 60623,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/60623",
                "username": "Ami"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-03T23:03:48.549",
            "dislikeCount": 0,
            "id": 161,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The scent is light, not overpowering at all. Good for the summertime.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 81757,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/81757",
                "username": "Nancy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-03T23:03:48.464",
            "dislikeCount": 0,
            "id": 160,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "The scent is light, not overpowering at all. Good for the summertime.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 81757,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/81757",
                "username": "Nancy"
            },
            "userAge": null
        }
    ],
    "userReview": null
}