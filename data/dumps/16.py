{
    "expertReview": null,
    "rating": {
        "average": 3.9,
        "reviewCount": 62,
        "reviews": {
            "1": 5,
            "2": 6,
            "3": 8,
            "4": 14,
            "5": 29
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2016-10-01T17:42:30.121",
            "dislikeCount": 0,
            "id": 68482,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love this perfume. It's had a light and soft smell to it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 273325,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 27,
                "url": "/p/273325",
                "username": "Teresa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T02:14:43.047",
            "dislikeCount": 0,
            "id": 67581,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "excellente fragance",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5215791561475608553570.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 348539,
                "likeTotal": 1,
                "reviewsTotal": 5,
                "shippedCount": 25,
                "url": "/p/348539",
                "username": "SONIA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-04T19:48:28.856",
            "dislikeCount": 0,
            "id": 62464,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love it!! One of my favorites.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16551562031468673170074.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 352213,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 9,
                "url": "/p/352213",
                "username": "Chris"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T14:51:01.625",
            "dislikeCount": 0,
            "id": 60843,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "This scent is lovely! Plus it has staying power! I love how long it lasts!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 455765,
                "likeTotal": 0,
                "reviewsTotal": 5,
                "shippedCount": 26,
                "url": "/p/455765",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T01:43:39.079",
            "dislikeCount": 0,
            "id": 60331,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Smells great!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 328709,
                "likeTotal": 0,
                "reviewsTotal": 8,
                "shippedCount": 27,
                "url": "/p/328709",
                "username": "lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T20:30:36.752",
            "dislikeCount": 0,
            "id": 59041,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Hubs didn't care for it.  A little heavy for summer.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 386124,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 13,
                "url": "/p/386124",
                "username": "Marti"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-30T03:07:19.042",
            "dislikeCount": 0,
            "id": 56826,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "It's...there's more alcohol than scent. It just wasn't me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10079263951463276415961.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 307327,
                "likeTotal": 16,
                "reviewsTotal": 13,
                "shippedCount": 27,
                "url": "/p/307327",
                "username": "Audra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-07T04:27:06.147",
            "dislikeCount": 0,
            "id": 54150,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love love love",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16951879631447333936236.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 214371,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 10,
                "url": "/p/214371",
                "username": "Deborah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-04T19:42:25.313",
            "dislikeCount": 0,
            "id": 53322,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "It's good",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 361615,
                "likeTotal": 4,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/361615",
                "username": "Renee"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-02T14:41:11.521",
            "dislikeCount": 0,
            "id": 51990,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "It's ok a little bit to strong for me not one of my favorite",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7043957761462373283188.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 348203,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/348203",
                "username": "Brandi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T17:02:52.394",
            "dislikeCount": 0,
            "id": 50485,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It's OK,  not one of my favorite but good for work.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 327683,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 12,
                "url": "/p/327683",
                "username": "Penny"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T14:05:51.845",
            "dislikeCount": 0,
            "id": 50315,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love the smell!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 321068,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 28,
                "url": "/p/321068",
                "username": "Kenya"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T08:12:27.741",
            "dislikeCount": 0,
            "id": 50113,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I liked the smell , I got a lot of compliments on the perfume",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 409731,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 10,
                "url": "/p/409731",
                "username": "Nicole"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T14:48:01.04",
            "dislikeCount": 0,
            "id": 49309,
            "isUseful": null,
            "likeCount": 2,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I didn't really care for this perfume. It reminded me of a men's cologne. It's more of a masculine scent",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9262713011469890607775.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 430727,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 13,
                "url": "/p/430727",
                "username": "Amy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-29T13:28:42.002",
            "dislikeCount": 0,
            "id": 48578,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Oh wow!!!!! is all I can say, I really love this scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 351582,
                "likeTotal": 1,
                "reviewsTotal": 6,
                "shippedCount": 14,
                "url": "/p/351582",
                "username": "latitia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-06T18:16:18.022",
            "dislikeCount": 0,
            "id": 46489,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Wonderful",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 417886,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 13,
                "url": "/p/417886",
                "username": "Rosalyn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-05T07:34:54.226",
            "dislikeCount": 0,
            "id": 45828,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 426540,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/426540",
                "username": "GENEVA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-03T12:30:39.51",
            "dislikeCount": 0,
            "id": 45206,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Smells amazing",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 450791,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 16,
                "url": "/p/450791",
                "username": "Sheila"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-04T19:39:06.793",
            "dislikeCount": 0,
            "id": 39234,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "A bit strong for me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 314282,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/314282",
                "username": "Missy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T19:35:09.452",
            "dislikeCount": 0,
            "id": 38703,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this perfume",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7389605851449099484278.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 224814,
                "likeTotal": 5,
                "reviewsTotal": 4,
                "shippedCount": 24,
                "url": "/p/224814",
                "username": "Jamie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T11:07:27.327",
            "dislikeCount": 0,
            "id": 38145,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "The scent stayed on all day , loved it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 336995,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 21,
                "url": "/p/336995",
                "username": "Theresa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T01:01:55.025",
            "dislikeCount": 0,
            "id": 37565,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Smells great",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "male",
                "id": 291735,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/291735",
                "username": "Christopher"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-02T21:54:52.046",
            "dislikeCount": 0,
            "id": 37211,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not my favorite, seemed better suited for my mom",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 352175,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/352175",
                "username": "Angela"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-25T01:08:24.929",
            "dislikeCount": 0,
            "id": 35421,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love the Calvin Klein Eternity collection.  This one did not disappoint.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 38391,
                "likeTotal": 10,
                "reviewsTotal": 10,
                "shippedCount": 35,
                "url": "/p/38391",
                "username": "Elizabeth Roxanne"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-21T23:19:58.855",
            "dislikeCount": 0,
            "id": 35140,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love the soft scent and its lingering time.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/4059633301471297863737.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 414674,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 20,
                "url": "/p/414674",
                "username": "Tamicka"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-05T11:38:33.249",
            "dislikeCount": 0,
            "id": 33414,
            "isUseful": null,
            "likeCount": 1,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Wonderful scent. Very light and playful",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 369410,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/369410",
                "username": "Susie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-03T04:31:18.109",
            "dislikeCount": 0,
            "id": 32563,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It's a bit old lady ish, not how I remember it! :(",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/21436914471462249887737.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 375777,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/375777",
                "username": "Janice"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-01T21:43:05.436",
            "dislikeCount": 0,
            "id": 32151,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love this fragrance!!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/18222350291486505078631.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 335389,
                "likeTotal": 3,
                "reviewsTotal": 8,
                "shippedCount": 15,
                "url": "/p/335389",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-01T18:42:59.262",
            "dislikeCount": 0,
            "id": 32106,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 305413,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 10,
                "url": "/p/305413",
                "username": "MARIELENE"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-27T03:34:35.084",
            "dislikeCount": 0,
            "id": 30999,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I like this scent a lot! It's a light scent without giving up it's elegance. The only drawback is that it mellows too quickly for me and I just can't seem to keep it for longer than a few minutes.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 165741,
                "likeTotal": 6,
                "reviewsTotal": 6,
                "shippedCount": 5,
                "url": "/p/165741",
                "username": "Krysta"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-13T15:55:49.786",
            "dislikeCount": 0,
            "id": 30270,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "a bit strong when you first spray but it goes away and gets light. Received many compliments. Scent gets soft after some time but still lingers. I am happy with it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 321138,
                "likeTotal": 5,
                "reviewsTotal": 7,
                "shippedCount": 19,
                "url": "/p/321138",
                "username": "Tiffany"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-31T02:32:47.606",
            "dislikeCount": 0,
            "id": 28358,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "smells fresh and yum!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 197248,
                "likeTotal": 4,
                "reviewsTotal": 5,
                "shippedCount": 27,
                "url": "/p/197248",
                "username": "Somarly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-30T02:09:46.452",
            "dislikeCount": 0,
            "id": 27646,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Not a big fan. I got an instant headache from this scent.  It's kind of powdery smelling.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 301243,
                "likeTotal": 8,
                "reviewsTotal": 3,
                "shippedCount": 18,
                "url": "/p/301243",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T21:54:30.401",
            "dislikeCount": 0,
            "id": 27190,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "My first scent and I absolutely LOVE it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 307910,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/307910",
                "username": "Amber"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T21:20:55.942",
            "dislikeCount": 0,
            "id": 27062,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It smells good when you put it on, but wear off within an hour",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 307411,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 26,
                "url": "/p/307411",
                "username": "tonia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-21T02:15:16.104",
            "dislikeCount": 0,
            "id": 25630,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This is my favorite perfume right now. I receive a ton of compliments. Good for work or a night out",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 82775,
                "likeTotal": 15,
                "reviewsTotal": 7,
                "shippedCount": 10,
                "url": "/p/82775",
                "username": "Sarah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-16T08:10:51.781",
            "dislikeCount": 0,
            "id": 25294,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Wow this is a wonderful  deal. (: ty so much for the perfume !!! Cant wait to  try the all ...",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 332962,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/332962",
                "username": "Scentbird"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-05T17:21:34.287",
            "dislikeCount": 0,
            "id": 24470,
            "isUseful": null,
            "likeCount": 2,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It's a nice smell",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 116921,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/116921",
                "username": "denise"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-05T04:27:46.573",
            "dislikeCount": 0,
            "id": 24360,
            "isUseful": null,
            "likeCount": 4,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "The scent is lovely. It may seem a bit over powering when first sprayed but the scent quickly diffuses. Also note that this scent doesn't all day long.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 262753,
                "likeTotal": 7,
                "reviewsTotal": 3,
                "shippedCount": 12,
                "url": "/p/262753",
                "username": "MIKITRA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-05T04:25:14.211",
            "dislikeCount": 0,
            "id": 24359,
            "isUseful": null,
            "likeCount": 3,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The scent is lovely. It seemed a bit over-powering when first sprayed however the scent quickly diffuses. Also note that it doesn't last all day long.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 262753,
                "likeTotal": 7,
                "reviewsTotal": 3,
                "shippedCount": 12,
                "url": "/p/262753",
                "username": "MIKITRA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-01T23:00:53.062",
            "dislikeCount": 0,
            "id": 23641,
            "isUseful": null,
            "likeCount": 5,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                }
            ],
            "text": "...",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 281736,
                "likeTotal": 5,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/281736",
                "username": "Maricelys"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-01T06:00:04.784",
            "dislikeCount": 0,
            "id": 23363,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Smells good. Starts out a bit strong but that fades away quick. It's a bit powdery but for the most part I enjoy the scent. The smell doesn't last all day at all tho, it tends to fade away kinda quick.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 255590,
                "likeTotal": 5,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/255590",
                "username": "Amanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T16:02:00.771",
            "dislikeCount": 0,
            "id": 19596,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This is my favorite so far!!!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 151378,
                "likeTotal": 5,
                "reviewsTotal": 2,
                "shippedCount": 10,
                "url": "/p/151378",
                "username": "Tracy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-29T17:24:04.905",
            "dislikeCount": 0,
            "id": 18709,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Starts out strongly sweet and floral, almost powdery but not quite. It almost smells like a fancy hotel soap to me.  A bad fit for me. I don't like it. It fades out to almost a nondescript scent on me. My husband doesn't think it's too bad, but I prefer Truth to Eternity Moment.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20341356631441319042353.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 106377,
                "likeTotal": 11,
                "reviewsTotal": 7,
                "shippedCount": 3,
                "url": "/p/106377",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-08T23:06:20.621",
            "dislikeCount": 0,
            "id": 17984,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "When I first put it on,  I didn't like it. It was too strong and too floral for me. But once it meshed with my body chemistry it smelled great. I get compliments on it all the time. I'm not convinced it's a perfect fit but, but I do like it now.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 214513,
                "likeTotal": 4,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/214513",
                "username": "Ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-30T22:19:03.83",
            "dislikeCount": 0,
            "id": 16095,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "not long lasting or very good smelling",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 91959,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 1,
                "url": "/p/91959",
                "username": "Cindy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-30T18:16:28.042",
            "dislikeCount": 0,
            "id": 15611,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Didnt like the smell, it was more for like a older lady and it is strong too.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 226553,
                "likeTotal": 7,
                "reviewsTotal": 2,
                "shippedCount": 12,
                "url": "/p/226553",
                "username": "Yaritza"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-30T15:10:07.888",
            "dislikeCount": 0,
            "id": 15324,
            "isUseful": null,
            "likeCount": 0,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Excellent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 232307,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/232307",
                "username": "Tiffany"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-28T20:38:46.234",
            "dislikeCount": 0,
            "id": 14437,
            "isUseful": null,
            "likeCount": 3,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "This smells lovely.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7712776131462034828505.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 170385,
                "likeTotal": 9,
                "reviewsTotal": 6,
                "shippedCount": 12,
                "url": "/p/170385",
                "username": "Elizabeth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-06T03:52:50.165",
            "dislikeCount": 0,
            "id": 13012,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Too strong",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 98900,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 2,
                "url": "/p/98900",
                "username": "Isabel"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-28T21:18:32.832",
            "dislikeCount": 0,
            "id": 11434,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Smells so good, nice size bottle,  very chic and packed well to fit in my purse",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 210471,
                "likeTotal": 4,
                "reviewsTotal": 3,
                "shippedCount": 2,
                "url": "/p/210471",
                "username": "Lakita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-25T17:17:02.645",
            "dislikeCount": 0,
            "id": 11240,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I honestly loved this perfume. I kept smelling to see if there was anything negative about it, and surprisingly, there wasn't. It's strong, but not too strong. The scent is very pleasant and nice. I love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 206806,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/206806",
                "username": "Lenore"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-24T18:40:17.798",
            "dislikeCount": 0,
            "id": 10555,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Smells great but doesn't last long. .",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 204036,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 11,
                "url": "/p/204036",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-08T01:57:00.577",
            "dislikeCount": 0,
            "id": 9671,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I really enjoy Eternity.  Any form of it.  This was no different.  Love,",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 38391,
                "likeTotal": 10,
                "reviewsTotal": 10,
                "shippedCount": 35,
                "url": "/p/38391",
                "username": "Elizabeth Roxanne"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-21T22:34:18.077",
            "dislikeCount": 0,
            "id": 6421,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I absolutely love it! It smells very clean with a hint of floral & it actually lasts more than 24 hours... Can't wait for Calvin Klein Eternity Now to come out on here!!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11953555971459448352022.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 166239,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 2,
                "url": "/p/166239",
                "username": "Dessiree"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-11T17:17:52.973",
            "dislikeCount": 0,
            "id": 5935,
            "isUseful": null,
            "likeCount": 4,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is a wonderfully clean floral that smells great on the skin.  Not offensive whatsoever, like a white cotton blanket.  Great for everyday use.  I had the body cream too and it was amazing!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 186676,
                "likeTotal": 11,
                "reviewsTotal": 8,
                "shippedCount": 1,
                "url": "/p/186676",
                "username": "diane"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-07T18:50:33.525",
            "dislikeCount": 0,
            "id": 5756,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love it!! It's such a clean wonderful smell",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 163890,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/163890",
                "username": "Letty"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-06T11:22:32.206",
            "dislikeCount": 0,
            "id": 5520,
            "isUseful": null,
            "likeCount": 1,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Smells delish",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 155388,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/155388",
                "username": "Mary"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T19:23:02.529",
            "dislikeCount": 0,
            "id": 4422,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I love the scent, I use it everyday!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14760854091448214733610.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 120260,
                "likeTotal": 4,
                "reviewsTotal": 6,
                "shippedCount": 3,
                "url": "/p/120260",
                "username": "Tanya"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T09:50:20.246",
            "dislikeCount": 0,
            "id": 4117,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Smells great but doesnt last very long. .",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 129991,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 14,
                "url": "/p/129991",
                "username": "Christina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T00:28:01.612",
            "dislikeCount": 0,
            "id": 3560,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Wonderful scent when you first put it on! however after an hour you can't smell it anymore if it was a little more long last thing I would have given it 5 hearts. I would repurchase in use as an everyday scent. Very fresh and youthful.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 155853,
                "likeTotal": 4,
                "reviewsTotal": 5,
                "shippedCount": 14,
                "url": "/p/155853",
                "username": "christina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-28T22:28:45.877",
            "dislikeCount": 0,
            "id": 3040,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I really didn't know if I'd like this.  I LOVE IT!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 148349,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/148349",
                "username": "Stefany"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-19T17:12:53.053",
            "dislikeCount": 0,
            "id": 2688,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I absolutely love the smell of this perfume!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14760854091448214733610.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 120260,
                "likeTotal": 4,
                "reviewsTotal": 6,
                "shippedCount": 3,
                "url": "/p/120260",
                "username": "Tanya"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-05T02:51:53.859",
            "dislikeCount": 0,
            "id": 2351,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Forgot to rate with hearts",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 77091,
                "likeTotal": 6,
                "reviewsTotal": 3,
                "shippedCount": 1,
                "url": "/p/77091",
                "username": "Rhonda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-05T02:49:31.156",
            "dislikeCount": 0,
            "id": 2350,
            "isUseful": null,
            "likeCount": 4,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "A very sophisticated scent, nice for the office or everyday. Subtle notes that are close to being crisp makes this a great perfume for all ages.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 77091,
                "likeTotal": 6,
                "reviewsTotal": 3,
                "shippedCount": 1,
                "url": "/p/77091",
                "username": "Rhonda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-31T18:03:02.551",
            "dislikeCount": 0,
            "id": 2000,
            "isUseful": null,
            "likeCount": 0,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Smells wonderful and fragrant and last a long time.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 82775,
                "likeTotal": 15,
                "reviewsTotal": 7,
                "shippedCount": 10,
                "url": "/p/82775",
                "username": "Sarah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-30T08:27:32.968",
            "dislikeCount": 0,
            "id": 1796,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It definitely was not my favorite. It didn't smell bad. It just smelt like something my mom or grandma would wear.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 88145,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/88145",
                "username": "CHRISTINA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-20T01:35:38.974",
            "dislikeCount": 0,
            "id": 964,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I actually got this for my first one. I think the flowery scent really stands out with this one.  A very feminine smell. The first time you spritz, the smell really gets you but then it settles down through the rest of the day. To me, I gave it four because it's okay and all but it doesn't match me 100%.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 96573,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 3,
                "url": "/p/96573",
                "username": "Camille"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-03T21:25:10.905",
            "dislikeCount": 0,
            "id": 91,
            "isUseful": null,
            "likeCount": 5,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I absolutely love this scent!!! It was my first one from this subscription and it has me so excited to get my next!!! I'm a perfume junkie but I'm also very picky so I wasn't sure how this would work out getting them blind 1/1 so far!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 78146,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 14,
                "url": "/p/78146",
                "username": "Vanessa"
            },
            "userAge": null
        }
    ],
    "userReview": null
}