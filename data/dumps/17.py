{
    "expertReview": null,
    "rating": {
        "average": 3.7,
        "reviewCount": 47,
        "reviews": {
            "1": 4,
            "2": 4,
            "3": 10,
            "4": 12,
            "5": 17
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2016-04-07T04:44:19.579",
            "dislikeCount": 0,
            "id": 29741,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I thought it would have more of a black current fragrance but instead the vetiver is predominate.  This one is not for me!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3125859571442681160262.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 33050,
                "likeTotal": 10,
                "reviewsTotal": 11,
                "shippedCount": 9,
                "url": "/p/33050",
                "username": "Ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-07T00:34:39.117",
            "dislikeCount": 0,
            "id": 29702,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 334098,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/334098",
                "username": "Hollie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-04T16:27:54.705",
            "dislikeCount": 0,
            "id": 29134,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The scent is a little too floral for my taste, an okay fragrance maybe for an elegant garden party.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/4276929981452737600412.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 189561,
                "likeTotal": 5,
                "reviewsTotal": 5,
                "shippedCount": 14,
                "url": "/p/189561",
                "username": "tabette"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-31T12:14:22.317",
            "dislikeCount": 0,
            "id": 28434,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Awesome",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 236344,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 0,
                "url": "/p/236344",
                "username": "BRANDI"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-31T00:52:20.634",
            "dislikeCount": 0,
            "id": 28312,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It's ok",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 82632,
                "likeTotal": 12,
                "reviewsTotal": 22,
                "shippedCount": 28,
                "url": "/p/82632",
                "username": "Shaketia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T21:19:41.889",
            "dislikeCount": 0,
            "id": 27054,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Very pretty floral, but the lasting power is literally minutes.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 281008,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/281008",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T18:39:16.424",
            "dislikeCount": 0,
            "id": 26592,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I like the smell although i wouldnt wear it too often because is too sweet for my taste.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 312701,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/312701",
                "username": "Carmen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-23T14:54:54.367",
            "dislikeCount": 0,
            "id": 25783,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Was not for me at all but never know till you try it. It smells like violets to me .",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20049771001458744916183.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 306387,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/306387",
                "username": "April"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-20T00:40:11.078",
            "dislikeCount": 0,
            "id": 25551,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This perfume smells absolutely amazing! I am glad I picked this one for my first sample, I will definitely wear it every day and probably buy a full size bottle. Love, love, love it! <3",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-230314191458434474376.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 307991,
                "likeTotal": 3,
                "reviewsTotal": 8,
                "shippedCount": 26,
                "url": "/p/307991",
                "username": "HANNAH"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-17T19:38:53.5",
            "dislikeCount": 0,
            "id": 25386,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This perfume is my favorite so far - so delicate and feminine! It's light and sweet and absolutely perfect for my style.  I will be wearing this one daily!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 287493,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/287493",
                "username": "Aimee"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-13T06:51:02.693",
            "dislikeCount": 0,
            "id": 25226,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This perfume is incredibly whimsical! It's beautifully and sweet and perfect for the spring time. I loved it soo much I purchased the full sized bottle, and the bottle is absolutely beautiful!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 207872,
                "likeTotal": 55,
                "reviewsTotal": 37,
                "shippedCount": 17,
                "url": "/p/207872",
                "username": "Elisa "
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-13T01:13:20.964",
            "dislikeCount": 0,
            "id": 25223,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I was not a fan and didn't try the scent more than a handful of times.  Something in this was artificial; perhaps it was my body chemistry at the time or it disagrees with me.  Either way, I didn't finish the bottle.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12424487181480518834302.JPG",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 74543,
                "likeTotal": 57,
                "reviewsTotal": 41,
                "shippedCount": 40,
                "url": "/p/74543",
                "username": "Quanita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-03T13:34:49.969",
            "dislikeCount": 0,
            "id": 24034,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I didn't really care for the scent.  I couldn't really detect what I didn't like about it.  Just didn't smell right with my body chemistry",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 198609,
                "likeTotal": 9,
                "reviewsTotal": 8,
                "shippedCount": 18,
                "url": "/p/198609",
                "username": "Rebecca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-01T06:09:08.131",
            "dislikeCount": 0,
            "id": 23365,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Not for me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5313603671455650583473.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 287199,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/287199",
                "username": "Kate"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-01T01:14:50.185",
            "dislikeCount": 0,
            "id": 23248,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It's pretty but not for me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8901463251446087715882.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 185444,
                "likeTotal": 28,
                "reviewsTotal": 21,
                "shippedCount": 6,
                "url": "/p/185444",
                "username": "Ericka"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-29T20:08:47.333",
            "dislikeCount": 0,
            "id": 23011,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Very light, airy, and sharp. If I were making \"families\" of perfumes, this would be the more sophisticated sister to Marchesa's Parfum D'Extase. It has an air of whimsy to it, just as if i'm following a fairy deeper into a forest. I did expect it to perform better as I only get about 2 hours on my skin, but the bottle is worth the price. I can see me using it to hold my rings!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7825741201447904914809.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 197534,
                "likeTotal": 25,
                "reviewsTotal": 9,
                "shippedCount": 2,
                "url": "/p/197534",
                "username": "Stassja"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-29T17:36:46.082",
            "dislikeCount": 0,
            "id": 22822,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "It is a strong fragrance at first but dries down to almost nothing on my skin.  I love the floral notes, they're beautiful and soft.  The only issue I have with the scent is the strong berry notes when I first sprayed it.  I'm not a fan of berry scents because they feel too juvenile for me, but that is a personal preference.  The berry does fade after a short while and does smell like a flower garden reminiscent of rose, freesia and sweet pea flowers.   Only four stars because of the lack of fragrance after only a couple of hours into it.  I may end up with  the full bottle during the summer months, it really isn't a cold weather fragrance IMHO because of the lightness.  Again, it may just be my skin chemistry.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10546255041452204404783.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 235963,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 1,
                "url": "/p/235963",
                "username": "Sameara"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-25T00:12:05.357",
            "dislikeCount": 0,
            "id": 22584,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love love love!  Smells like spring and great for an outdoor date or the office",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-4337106321446123733784.JPG",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 109895,
                "likeTotal": 5,
                "reviewsTotal": 4,
                "shippedCount": 3,
                "url": "/p/109895",
                "username": "Brandy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-08T13:20:58.983",
            "dislikeCount": 0,
            "id": 21869,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This is a sweet, green citrus-floral. Very bright smelling, as you would expect from the name. It may pull just a tad too sweet on me, but I still love it. This is a very unique spring-summer fragrance!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 52587,
                "likeTotal": 8,
                "reviewsTotal": 7,
                "shippedCount": 8,
                "url": "/p/52587",
                "username": "Jessie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-06T15:24:17.244",
            "dislikeCount": 0,
            "id": 21558,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love the scent, clean floral but not too sweet smelling like a candy! I will purchase full size..;)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 91242,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/91242",
                "username": "Sherie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-03T00:45:22.691",
            "dislikeCount": 0,
            "id": 20449,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Sweet always smells so good on me and this ones a winner winner chicken dinner.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 209920,
                "likeTotal": 18,
                "reviewsTotal": 15,
                "shippedCount": 22,
                "url": "/p/209920",
                "username": "April"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T21:23:03.412",
            "dislikeCount": 0,
            "id": 20047,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I didn't like it. Smells good but didn't work well with me chemistry.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1084448911447102644140.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 190538,
                "likeTotal": 7,
                "reviewsTotal": 12,
                "shippedCount": 13,
                "url": "/p/190538",
                "username": "elaine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T17:52:57.436",
            "dislikeCount": 0,
            "id": 19712,
            "isUseful": null,
            "likeCount": 7,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "3.5. I like it, but at the same time I am aware of its weaknesses. The scent is basically sugared violets, which I find very appealing. I find violet to be a less common fragrance, which I think makes this somewhat unique, although I admit that it is simple and slightly juvenile. I don't really get any of the berry scent at all, or much iris. It has a modest amount of throw, but isn't particularly lasting. As others have said, it's inoffensive and fairly appropriate for work settings.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 75722,
                "likeTotal": 64,
                "reviewsTotal": 26,
                "shippedCount": 33,
                "url": "/p/75722",
                "username": "Alysson"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-17T04:35:59.909",
            "dislikeCount": 0,
            "id": 18121,
            "isUseful": null,
            "likeCount": 6,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I really like this scent. To me it smells just like Lolita Lempicka does anyone else think that?? I don't know maybe it's just me. I think I will buy this because the bottle is so pretty and I do like the way it smells.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13980507221447800229021.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 174783,
                "likeTotal": 6,
                "reviewsTotal": 3,
                "shippedCount": 1,
                "url": "/p/174783",
                "username": "Frances"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-07T15:51:03.309",
            "dislikeCount": 0,
            "id": 17828,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Very floral, can be a bit overwhelming if you spray on too much. You really only need a tiny bit.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 218765,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 0,
                "url": "/p/218765",
                "username": "Alexa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-29T01:14:09.747",
            "dislikeCount": 0,
            "id": 14495,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "This is such a romantic scent! You do feel like a beautiful Feerie fluttering thru the room when you wear this. It is a must have in my collection.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9421533321451351662282.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 183217,
                "likeTotal": 5,
                "reviewsTotal": 2,
                "shippedCount": 21,
                "url": "/p/183217",
                "username": "Patricia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-08T01:36:11.104",
            "dislikeCount": 0,
            "id": 13399,
            "isUseful": null,
            "likeCount": 10,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I wrote up a review and don't think I clicked all the way through to submit it. Drats. I'll try again. Sorry if it shows up twice. This is a really difficult one to describe. It's one of those scents  where I can take it or leave it. I'll use the bottle up, but never revisit it once I'm done. It's not bad, its just not great. \n\nThe first spray definitely smells like fruit. Grapes or plums, maybe. It's kind of sweet, but not sticky sweet. You can get away with it in coolish temps (75 degrees and under). It reminds me of incense. And if you're a smoker, the smell of the smoke compliments it. It also reminds me of those gumdrops that are different colors so you get the impression they're supposed to be different flavors, but they all taste the same: sweet, a little bit spicy, with a very slight hint of licorice. \n\nI say give it a try if it's on your radar, I don't think you would downright hate it. And depending on your body chemistry - it could be a really pretty scent. At worst, it's one of those scents you reach for once-in-awhile when you want to wear SOMETHING, but you don't feel like reaching for any of your staple scents.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 132332,
                "likeTotal": 35,
                "reviewsTotal": 13,
                "shippedCount": 10,
                "url": "/p/132332",
                "username": "Tracey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-06T02:42:04.295",
            "dislikeCount": 0,
            "id": 12992,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It's cool, feminine, not very outstanding but I like it.. My first Van Cleef and Arpels.. It's similar to ME by Lanvin",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 155602,
                "likeTotal": 7,
                "reviewsTotal": 6,
                "shippedCount": 29,
                "url": "/p/155602",
                "username": "Josanne"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-30T11:33:09.661",
            "dislikeCount": 0,
            "id": 11661,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The scent was fresh clean and floral, but it wasn't the right one for me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 81014,
                "likeTotal": 8,
                "reviewsTotal": 6,
                "shippedCount": 30,
                "url": "/p/81014",
                "username": "Ginny"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-24T23:24:43.281",
            "dislikeCount": 0,
            "id": 10971,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Like it a lot :)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 50771,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 17,
                "url": "/p/50771",
                "username": "Judita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-24T22:20:56.046",
            "dislikeCount": 0,
            "id": 10885,
            "isUseful": null,
            "likeCount": 5,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Way too strong for me!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 184507,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 0,
                "url": "/p/184507",
                "username": "Kristin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-06T06:18:34.941",
            "dislikeCount": 0,
            "id": 9413,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love this! It's very pretty and smells green and floral.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17618928511442846425408.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 154895,
                "likeTotal": 6,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/154895",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-01T23:04:54.575",
            "dislikeCount": 0,
            "id": 8479,
            "isUseful": null,
            "likeCount": 5,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I love this scent. It is light and floral with a touch of powder. It doesn't stay strong very long. It fades until you can only smell it when you are very close. Perfect for day wear. Beautiful.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 58119,
                "likeTotal": 20,
                "reviewsTotal": 10,
                "shippedCount": 8,
                "url": "/p/58119",
                "username": "Caroline"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-01T23:01:47.687",
            "dislikeCount": 0,
            "id": 8478,
            "isUseful": null,
            "likeCount": 7,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this scent. It is strong at first but fades until you can only smell it when you are very close. It is floral and light and makes me feel like I have my stuff together. Clean and fresh but doesn't smell anything like soap. Beautiful",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 58119,
                "likeTotal": 20,
                "reviewsTotal": 10,
                "shippedCount": 8,
                "url": "/p/58119",
                "username": "Caroline"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-30T00:16:37.015",
            "dislikeCount": 0,
            "id": 7437,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love it makes you feel beautiful! Gonna buy a bottle as a gift for myself!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/18225308311447248552505.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 146275,
                "likeTotal": 12,
                "reviewsTotal": 9,
                "shippedCount": 6,
                "url": "/p/146275",
                "username": "Kim"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-25T19:37:32.724",
            "dislikeCount": 0,
            "id": 6552,
            "isUseful": null,
            "likeCount": 4,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I thought it smelled horrible.  Very overpowering scent that made my nose itch.  It also gave me a rash where I sprayed it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 106931,
                "likeTotal": 7,
                "reviewsTotal": 4,
                "shippedCount": 11,
                "url": "/p/106931",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-24T16:31:01.236",
            "dislikeCount": 0,
            "id": 6518,
            "isUseful": null,
            "likeCount": 5,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I love this one..on me it smells like pears&theres a buncha flavors just rolling around..1 min it smells kinda powder,then floral but to me theres always pears even tho i dont see that in the smell description thats what i get on my skin..its complex,simple,fresh,mysterious all at the same time just wish it lasted longer.get about 2hrs b4 it either starts to fade or i get used to it but my husband can catch wiffs of it on his sweaters days later tho it isnt strong..its a surprizing perfume",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10927542851460958629262.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 42827,
                "likeTotal": 15,
                "reviewsTotal": 8,
                "shippedCount": 25,
                "url": "/p/42827",
                "username": "Crystal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-20T02:09:57.223",
            "dislikeCount": 0,
            "id": 6266,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I get a very powdery floral scent.  It's quite strong when applied, but then it starts to fade.  The scent only lasts for about 2 - 3 hours and then it's gone.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 119712,
                "likeTotal": 6,
                "reviewsTotal": 5,
                "shippedCount": 38,
                "url": "/p/119712",
                "username": "Tracie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-16T21:33:06.033",
            "dislikeCount": 0,
            "id": 6074,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This a nice soft floral that's a bit powdery. It makes for a great daily wear scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 97219,
                "likeTotal": 7,
                "reviewsTotal": 4,
                "shippedCount": 8,
                "url": "/p/97219",
                "username": "Lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-15T22:22:19.125",
            "dislikeCount": 0,
            "id": 6002,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "love love light and sweet just like a fairy",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/18225308311447248552505.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 146275,
                "likeTotal": 12,
                "reviewsTotal": 9,
                "shippedCount": 6,
                "url": "/p/146275",
                "username": "Kim"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-07T23:58:06.012",
            "dislikeCount": 0,
            "id": 5788,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": ".",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 2,
                "gender": "female",
                "id": 52125,
                "likeTotal": 7,
                "reviewsTotal": 4,
                "shippedCount": 36,
                "url": "/p/52125",
                "username": "Krissyl"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-06T12:56:04.614",
            "dislikeCount": 0,
            "id": 5535,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "love the fresh smell and it last all day everyone tells me I smell good",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12926781291441030814147.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 69678,
                "likeTotal": 4,
                "reviewsTotal": 10,
                "shippedCount": 14,
                "url": "/p/69678",
                "username": "FAYE"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-01T02:22:02.112",
            "dislikeCount": 0,
            "id": 4545,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Sweet with a hint of dusk. Love it!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/345773951446255350382.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 56694,
                "likeTotal": 6,
                "reviewsTotal": 6,
                "shippedCount": 27,
                "url": "/p/56694",
                "username": "Winna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T22:17:06.261",
            "dislikeCount": 0,
            "id": 4476,
            "isUseful": null,
            "likeCount": 5,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I think is an appropriate every day scent. It is not overpowering, but it quite pleasant. I would describe it as a slightly powdery floral, with a tiny bit of sharpness from the citrus note. It is less sweet than I anticipated, especially because the first two notes are violet and black currant.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 48902,
                "likeTotal": 12,
                "reviewsTotal": 8,
                "shippedCount": 23,
                "url": "/p/48902",
                "username": "Brandyne"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T17:25:43.467",
            "dislikeCount": 0,
            "id": 1590,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 91832,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/91832",
                "username": "Mariesa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T15:13:43.07",
            "dislikeCount": 0,
            "id": 1482,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It is a very fresh floral smell. This scent is great for school or work, because it is not too overpowering. The scent goes on strong initially; however, it does not last all day.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 100540,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/100540",
                "username": "Marcela"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T02:53:20.42",
            "dislikeCount": 0,
            "id": 1218,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Going to get a full bottle next!  :)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 44521,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 19,
                "url": "/p/44521",
                "username": "Beth"
            },
            "userAge": null
        }
    ],
    "userReview": null
}