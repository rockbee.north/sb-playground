{
    "expertReview": null,
    "rating": {
        "average": 2.9,
        "reviewCount": 13,
        "reviews": {
            "1": 3,
            "2": 3,
            "3": 2,
            "4": 2,
            "5": 3
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2016-03-31T17:37:44.623",
            "dislikeCount": 0,
            "id": 28521,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "A nice alternative to Chanel No 5 without the powdery notes, and at a much better price point.  You can find this scent for around $35 compared to Chanel EDT of around $80. This fragrance is a very elegant, ladylike scent.  A more mature scent probably not as suitable for teenagers or most college-age students, but not an 'old lady' fragrance either.  I could see 30 and up wearing this scent.  Also, for an EDT this was fairly strong upon application.  Can't even imagine how strong the EDP is on application.  I would say this EDT version lasted around 4hrs on me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 173314,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 35,
                "url": "/p/173314",
                "username": "Shannon"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-03T17:39:44.136",
            "dislikeCount": 0,
            "id": 24079,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This was my first Van Cleef & Arpels scent and I must say. Wow.  Really fabulously lux unique scent that is truly different to others you might try. Usually I can compare it to something else, this one i can't.  You should try it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 32307,
                "likeTotal": 10,
                "reviewsTotal": 10,
                "shippedCount": 24,
                "url": "/p/32307",
                "username": "AthenaRunninginHeels"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-17T21:30:24.602",
            "dislikeCount": 0,
            "id": 22205,
            "isUseful": null,
            "likeCount": 2,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Holy cow, this is one potent fragrance. It is also extremely long lasting, which would be good things if I liked it. This scent reminds me of something every one of my elementary school teachers wore in the late 80s. It smells similar to Elizabeth Taylor's White Diamonds. So if you like that you'll love this. Definitely not for anyone under the age of 30.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/4844318301451946367582.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 254748,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 0,
                "url": "/p/254748",
                "username": "Sarah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T19:42:41.109",
            "dislikeCount": 0,
            "id": 19826,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I didn't like it, i went off of the quiz i took and chose this one but it did not go well with me and my prefference in scents",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13333811221447984443442.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 192534,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/192534",
                "username": "Emily"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-28T04:25:29.469",
            "dislikeCount": 0,
            "id": 18612,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This was one of the first grown-up fragrances that I wore after I started working in cosmetics about 30 years ago so when I saw it was available here I immediately added it to my queue. I still like it but I have found other fragrances that I love so I will probably not re-order when I finish this one. It is a classic; elegant and powerful. Definitely for a woman who is assured and wants to make a strong statement with her fragrance but doesn't want something sweet, flowery, or fruity; but it isn't musky or spicy either.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14308011521451534332895.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 244438,
                "likeTotal": 5,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/244438",
                "username": "Rebecca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-20T08:16:49.507",
            "dislikeCount": 0,
            "id": 18260,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                }
            ],
            "text": "This was the first perfume I ever owned and I never liked it for myself. It is a very classic scent and is a little out-of-date in my opinion. Very strong, and just not a scent I like to wear.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/782618021451679933751.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 252557,
                "likeTotal": 6,
                "reviewsTotal": 5,
                "shippedCount": 3,
                "url": "/p/252557",
                "username": "Emilee"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-19T12:51:30.09",
            "dislikeCount": 0,
            "id": 18193,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "ABSOLUTELY HORRID!!! SMELLS LIKE URINE!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 168402,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 3,
                "url": "/p/168402",
                "username": "doretta"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-31T01:02:01.002",
            "dislikeCount": 0,
            "id": 16279,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Classic scent!  One spray lasts a very long time.  Love to wear to out for a special occasion.  Love this.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 180253,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 3,
                "url": "/p/180253",
                "username": "Linda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-18T22:38:07.935",
            "dislikeCount": 0,
            "id": 13980,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It's very nice, but I don't think that it is the BEST fit, for me. However, I would recommend it for those that truly love a classic fragrance.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1981071563216351082.jpeg",
                "dislikeTotal": 2,
                "gender": "female",
                "id": 198107,
                "likeTotal": 78,
                "reviewsTotal": 24,
                "shippedCount": 5,
                "url": "/p/198107",
                "username": "Shannon"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-04T18:57:47.223",
            "dislikeCount": 0,
            "id": 12635,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "It has a bite to it.  I won't be purchasing again.  Old fashion perfume.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/6636415151446659866945.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 50439,
                "likeTotal": 5,
                "reviewsTotal": 7,
                "shippedCount": 28,
                "url": "/p/50439",
                "username": "Aubrey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-25T00:07:23.791",
            "dislikeCount": 0,
            "id": 11006,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "This is one of my favorite all time scents; I am 56 so that may make a difference",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20427715811448410217279.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 109025,
                "likeTotal": 8,
                "reviewsTotal": 6,
                "shippedCount": 16,
                "url": "/p/109025",
                "username": "Shauneille"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-29T15:44:47.343",
            "dislikeCount": 0,
            "id": 3152,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is not the scent for me. Too strong.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 90275,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/90275",
                "username": "Vanessa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-06T14:29:11.607",
            "dislikeCount": 0,
            "id": 2425,
            "isUseful": null,
            "likeCount": 6,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Super strong. One spritz & you're good for the day. Definitely has a powdery overall note to it. This is absolutely an \"older\" perfume in that it smells nothing like today's concoctions. Which is fine!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 79350,
                "likeTotal": 6,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/79350",
                "username": "Amanda"
            },
            "userAge": null
        }
    ],
    "userReview": null
}