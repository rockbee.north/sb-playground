{
    "expertReview": null,
    "rating": {
        "average": 3.6,
        "reviewCount": 51,
        "reviews": {
            "1": 2,
            "2": 6,
            "3": 14,
            "4": 15,
            "5": 14
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2016-04-02T14:20:36.85",
            "dislikeCount": 0,
            "id": 28862,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "i hated it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 57473,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 10,
                "url": "/p/57473",
                "username": "Keri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-22T03:59:02.976",
            "dislikeCount": 0,
            "id": 25717,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Starts out way too citrusy but (at least on my skin) moved towards the spiced fruit, my partner and I both agreed it smelled best after an hour of wear. Very mellow.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 315067,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/315067",
                "username": "Jen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-21T22:40:11.348",
            "dislikeCount": 0,
            "id": 25697,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Enchanting. No staying power.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 69525,
                "likeTotal": 7,
                "reviewsTotal": 9,
                "shippedCount": 31,
                "url": "/p/69525",
                "username": "Lee"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-21T16:59:53.532",
            "dislikeCount": 0,
            "id": 25662,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I love this smell. Very woodsy, musky. Reminds me of the smell of the woods after the rain. I love it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 290570,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/290570",
                "username": "Jamilatu"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-20T01:44:34.566",
            "dislikeCount": 0,
            "id": 25559,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The scent to me smells just like a pine tree definately not one I'll be using much",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 217363,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 4,
                "url": "/p/217363",
                "username": "Nobie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-01T14:11:58.696",
            "dislikeCount": 0,
            "id": 23436,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "It smells sensual, not sure that the fragrance lasts as long as I would like it to, throughout the day.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12073239911455227576701.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 286259,
                "likeTotal": 4,
                "reviewsTotal": 5,
                "shippedCount": 26,
                "url": "/p/286259",
                "username": "Kay"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-01T04:01:11.671",
            "dislikeCount": 0,
            "id": 23333,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love this scent.  It's warm,seductive very alluring.  The scent is long lasting but not over powering.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8376783321451945682061.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 122062,
                "likeTotal": 9,
                "reviewsTotal": 14,
                "shippedCount": 25,
                "url": "/p/122062",
                "username": "Lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-29T16:51:28.542",
            "dislikeCount": 0,
            "id": 22776,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Very sexy and spicy fragrance, but still feminine and delicate. Many people complimented me on it, it's a subtle showstopper!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14225691821452017344188.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 255049,
                "likeTotal": 9,
                "reviewsTotal": 4,
                "shippedCount": 13,
                "url": "/p/255049",
                "username": "terri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-20T00:56:05.461",
            "dislikeCount": 0,
            "id": 22388,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This is a strong and sophisticated scent. A little mature for me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14527692011446697249722.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 196554,
                "likeTotal": 3,
                "reviewsTotal": 7,
                "shippedCount": 4,
                "url": "/p/196554",
                "username": "karen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-16T21:42:03.62",
            "dislikeCount": 0,
            "id": 22112,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "A slight hint of powder lays underneath light florals...  this is a lovely scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 122464,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/122464",
                "username": "Mary"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-16T20:55:06.384",
            "dislikeCount": 0,
            "id": 22105,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "The scent didn't last at all, so I can't really say if it's a remarkable one.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 170486,
                "likeTotal": 4,
                "reviewsTotal": 7,
                "shippedCount": 2,
                "url": "/p/170486",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T20:05:57.392",
            "dislikeCount": 0,
            "id": 19873,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Loved it!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 204552,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 8,
                "url": "/p/204552",
                "username": "Christina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T05:13:53.127",
            "dislikeCount": 0,
            "id": 19482,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I absolutely love this fragrance. I used the entire bottle in just two weeks.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/18518623451460777871224.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 47117,
                "likeTotal": 10,
                "reviewsTotal": 10,
                "shippedCount": 6,
                "url": "/p/47117",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T00:34:29.827",
            "dislikeCount": 0,
            "id": 19432,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I think this perfume has a nice smell probably something I would wear on a date night. It has a nice deep smell that wear nicely.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 196803,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 1,
                "url": "/p/196803",
                "username": "Rebecca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-30T09:01:46.838",
            "dislikeCount": 0,
            "id": 18905,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It smelled nice but faded away quickly",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-9113678791453369792853.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 35851,
                "likeTotal": 8,
                "reviewsTotal": 6,
                "shippedCount": 10,
                "url": "/p/35851",
                "username": "Kitamu"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-20T19:20:45.782",
            "dislikeCount": 0,
            "id": 18297,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "this fragrance is  delicious scent that i will be indulging in repeatedly",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/2211698561453317650189.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 35254,
                "likeTotal": 16,
                "reviewsTotal": 19,
                "shippedCount": 37,
                "url": "/p/35254",
                "username": "rhonda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-05T16:30:37.631",
            "dislikeCount": 0,
            "id": 17402,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Too mature for my taste",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 231318,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/231318",
                "username": "Erin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-05T08:40:58.846",
            "dislikeCount": 0,
            "id": 17347,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "it was ok. nothing to get excited about. A nice clean scent, slightly woodsy. nothing is go out and buy",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/2834055711446411646019.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 104152,
                "likeTotal": 11,
                "reviewsTotal": 11,
                "shippedCount": 12,
                "url": "/p/104152",
                "username": "Bobbi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-04T21:24:56.68",
            "dislikeCount": 0,
            "id": 17238,
            "isUseful": null,
            "likeCount": 4,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This fragrance captures spices. I find it to be ambery, gingery, and warm in a fresh sense, however I also find it lacking something. It's almost as if I'm wishing for an oud note or something \"sticky\". If I may, I feel like this perfume contains some of the best ingredients from the East, but was concocted by a Westerner and therefore, doesn't fully capture the perfume's name sake. The longevity is only a couple of hours, maybe three tops. After that it settles on the skin.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7825741201447904914809.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 197534,
                "likeTotal": 25,
                "reviewsTotal": 9,
                "shippedCount": 2,
                "url": "/p/197534",
                "username": "Stassja"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-29T23:59:49.79",
            "dislikeCount": 0,
            "id": 14827,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "This is a \"grown up scent\" . It's great for winter. It lasts all day.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14527692011446697249722.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 196554,
                "likeTotal": 3,
                "reviewsTotal": 7,
                "shippedCount": 4,
                "url": "/p/196554",
                "username": "karen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-04T22:13:31.643",
            "dislikeCount": 0,
            "id": 12680,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Very nice perfume!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 193704,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/193704",
                "username": "Kristen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-03T15:27:28.128",
            "dislikeCount": 0,
            "id": 12382,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I love this scent so much I plan on purchasing a big bottle to make it my signature scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/18518623451460777871224.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 47117,
                "likeTotal": 10,
                "reviewsTotal": 10,
                "shippedCount": 6,
                "url": "/p/47117",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-24T21:52:32.091",
            "dislikeCount": 0,
            "id": 10829,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": ".",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 199537,
                "likeTotal": 3,
                "reviewsTotal": 5,
                "shippedCount": 8,
                "url": "/p/199537",
                "username": "Kimberly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-16T22:21:29.624",
            "dislikeCount": 0,
            "id": 10012,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This is my favorite scent so far.  Great for daytime and not too much nose for a work perfume.  LOVE!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 52411,
                "likeTotal": 8,
                "reviewsTotal": 13,
                "shippedCount": 20,
                "url": "/p/52411",
                "username": "Jane"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-16T20:56:57.622",
            "dislikeCount": 0,
            "id": 9995,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Spicy. Mature. The faded scent is nice. Would not purchase.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 199537,
                "likeTotal": 3,
                "reviewsTotal": 5,
                "shippedCount": 8,
                "url": "/p/199537",
                "username": "Kimberly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-15T15:43:32.942",
            "dislikeCount": 0,
            "id": 9941,
            "isUseful": null,
            "likeCount": 3,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Very, very mature fragrance. And when I say mature I mean I think of my 80 year old grandmother, because this is something she would wear.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 135510,
                "likeTotal": 7,
                "reviewsTotal": 9,
                "shippedCount": 8,
                "url": "/p/135510",
                "username": "sierra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-14T18:13:05.898",
            "dislikeCount": 0,
            "id": 9900,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I love this one!  I will probably wear this everyday even though it's a bit heavy.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 251034,
                "likeTotal": 8,
                "reviewsTotal": 6,
                "shippedCount": 16,
                "url": "/p/251034",
                "username": "Mindy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-06T18:27:22.837",
            "dislikeCount": 0,
            "id": 9515,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I honestly didn't care for this scent. No staying power.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8101968541474692159744.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 75551,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 14,
                "url": "/p/75551",
                "username": "Alyssa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-06T00:27:51.865",
            "dislikeCount": 0,
            "id": 9327,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I LOVE this scent.  I want to wear it every single day.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/18518623451460777871224.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 47117,
                "likeTotal": 10,
                "reviewsTotal": 10,
                "shippedCount": 6,
                "url": "/p/47117",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-03T21:57:25.932",
            "dislikeCount": 0,
            "id": 8871,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Lovely, sophisticated, sexy, slightly spicy, romantic. I am happy to add this to my collection.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12219563841445800777116.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 94989,
                "likeTotal": 7,
                "reviewsTotal": 5,
                "shippedCount": 4,
                "url": "/p/94989",
                "username": "Ann Elizabeth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-03T17:50:53.728",
            "dislikeCount": 0,
            "id": 8828,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 123532,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/123532",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-29T17:17:26.572",
            "dislikeCount": 0,
            "id": 6915,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "My favorite perfume is Miss Dior, and this was a bit similar, but it does not last on me nearly as long. It's less sweet smelling than Miss Dior, so I wouldn't say this is a good scent for younger girls, but overall its a good, somewhat mature smelling (without being overpowering at all), scent for the autumn :)",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12655442941443585361431.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 107672,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 2,
                "url": "/p/107672",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-29T08:51:43.804",
            "dislikeCount": 0,
            "id": 6744,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Very unique :)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 86454,
                "likeTotal": 7,
                "reviewsTotal": 13,
                "shippedCount": 25,
                "url": "/p/86454",
                "username": "Macadamia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-05T21:49:34.737",
            "dislikeCount": 0,
            "id": 5374,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I had samples of this fragrance years ago and never bought it so I'm so excited to have it now and I can travel with it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 50586,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/50586",
                "username": "Olivia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-01T16:22:34.132",
            "dislikeCount": 0,
            "id": 4655,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This scent was not for me. It had a strong musty scent that I didn't like. I could barely get the hints of chocolate and raspberry.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 97452,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/97452",
                "username": "Cindy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T22:33:36.652",
            "dislikeCount": 0,
            "id": 4482,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The scent is pretty nice, but it has NO staying power... wore off before I got out of the car to go into work.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 165660,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/165660",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T15:48:45.064",
            "dislikeCount": 0,
            "id": 4304,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "very nice a light spicy note, however the fragrance wore off.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/4091413581446219910392.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 92083,
                "likeTotal": 13,
                "reviewsTotal": 6,
                "shippedCount": 7,
                "url": "/p/92083",
                "username": "Rosemary"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T02:31:55.55",
            "dislikeCount": 0,
            "id": 3876,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Perfect for summer/fall smells. I didn't like it at first try, but after the second day...i am in love with it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 42380,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 14,
                "url": "/p/42380",
                "username": "shinta"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-23T16:27:50.665",
            "dislikeCount": 0,
            "id": 2862,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "LOVE this.  Everyone I've shared it with loves it.  What a great scent.  Perfect spring/fall scent with a bit of spice and a lot of fruit/citrus high notes.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 52411,
                "likeTotal": 8,
                "reviewsTotal": 13,
                "shippedCount": 20,
                "url": "/p/52411",
                "username": "Jane"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-11T22:51:25.682",
            "dislikeCount": 0,
            "id": 2570,
            "isUseful": null,
            "likeCount": 3,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I prefer more woodsy, vanilla, creamy smells ....that being said  This was not really my favorite. It was so sweet and fruity it came across as a very heavy alcohol smell .The amber smell was a bit heavy and ruined because of the fruity smell . It didn't  smell horrible just kind of different not at all an intoxicating I got to know what your wearing perfume .",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-12093492141442012068954.JPG",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 32720,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 36,
                "url": "/p/32720",
                "username": "issiemel"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-07T19:29:12.584",
            "dislikeCount": 0,
            "id": 2485,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Second only to Prada Candy. I love a long lasting parfum.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 2,
                "gender": "female",
                "id": 66885,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 30,
                "url": "/p/66885",
                "username": "Mysheriamore511@gmail.com"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-03T16:18:54.126",
            "dislikeCount": 0,
            "id": 2245,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Did not like. Was very mature. I felt it had too much amber.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 56167,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 11,
                "url": "/p/56167",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-03T15:32:01.728",
            "dislikeCount": 0,
            "id": 2243,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "At 1st i thought this smelled more like an old lady but after it dried down this beautiful kinda peachy smell came thru..i love it&would never have tried it,would passed it up in the store,so thanks scentbird i think i found a new love",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10927542851460958629262.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 42827,
                "likeTotal": 15,
                "reviewsTotal": 8,
                "shippedCount": 25,
                "url": "/p/42827",
                "username": "Crystal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-02T01:09:14.586",
            "dislikeCount": 0,
            "id": 2128,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Very nice scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 105952,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/105952",
                "username": "Kelley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-01T17:36:47.009",
            "dislikeCount": 0,
            "id": 2094,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This was an interesting scent to me. I tend to prefer scents that are light and fresh, which this isn't. It is definitely a patchouli influenced scent, which doesn't tend to be my thing. However, it did dry down a lot milder than on first spray, and I ended up not minding it. I'll reach for it when I want something a bit exotic, but won't buy it when I'm out.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 46467,
                "likeTotal": 3,
                "reviewsTotal": 6,
                "shippedCount": 12,
                "url": "/p/46467",
                "username": "Rosa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T03:28:36.823",
            "dislikeCount": 0,
            "id": 1225,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Oriens smells better than I imagined!   It's a lovely scent; a hint of floral and background of fresh, light powder.  I recommend trying it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 80493,
                "likeTotal": 3,
                "reviewsTotal": 6,
                "shippedCount": 13,
                "url": "/p/80493",
                "username": "Emmy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T15:14:00.553",
            "dislikeCount": 0,
            "id": 475,
            "isUseful": null,
            "likeCount": 4,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "this scent surprised me.... I thought it would be a special occasions only scent... wow!!! trust me it is not... this is an everyday,  everywhere scent... needless to say I will be purchasing the full size...",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17622143311448423443564.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 40513,
                "likeTotal": 9,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/40513",
                "username": "claudette"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-06-20T00:25:15",
            "dislikeCount": 0,
            "id": -352,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"this smells good but if you have any sensitivities to strong scents i.e. migraines consider yourself warned. its a bit strong, which would be good for the evening and the winter.\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 3,
                "gender": "female",
                "id": 57772,
                "likeTotal": 9,
                "reviewsTotal": 4,
                "shippedCount": 22,
                "url": "/p/57772",
                "username": "Lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-05-02T22:22:38",
            "dislikeCount": 1,
            "id": -242,
            "isUseful": null,
            "likeCount": 5,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"I really like this scent but it gave me a migraine. I am super sensitive to smells so when I sprayed this I thought it had a great smell to it and it was sexy but I got the migraine soon after. Sad face, I have to swap this out now. As for the notes, I feel this is a more mature fragrance and comes off a bit older than my age but I do love the spiciness of this fragrance. So far my favorite to date would be Flowerbomb if you are looking for something that goes along with the notes from this scent.\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 48503,
                "likeTotal": 6,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/48503",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-04-29T23:08:30",
            "dislikeCount": 0,
            "id": -197,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love this scent. It's sophisticated and versatile. I wore this when I knew I had plans after work. I would put it on in the morning and it would last all day. It isn't overly sweet and has a spiciness to it. I actually plan on purchasing a full bottle when my sample runs out.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 34173,
                "likeTotal": 6,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/34173",
                "username": "Erin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-03-01T21:13:37",
            "dislikeCount": 1,
            "id": -37,
            "isUseful": null,
            "likeCount": 4,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"This is a beautiful sweet sophisticated scent that could blend into any season or any time of day, I think. The patchouli isn't too strong, but keeps the florals and the sweetness a little bit sexy. I think the berries come out pretty strong, but this is definitely not a little girl scent. You could wear this to work or out on a date and you would smell great. I took off one star because it has a pretty soft projection, and I prefer fragrances like this to have a little more gusto.\"",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12672348401456772367704.jpg",
                "dislikeTotal": 5,
                "gender": "female",
                "id": 34077,
                "likeTotal": 69,
                "reviewsTotal": 24,
                "shippedCount": 31,
                "url": "/p/34077",
                "username": "Ashleigh"
            },
            "userAge": null
        }
    ],
    "userReview": null
}