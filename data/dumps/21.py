{
    "expertReview": null,
    "rating": {
        "average": 3.1,
        "reviewCount": 34,
        "reviews": {
            "1": 11,
            "2": 3,
            "3": 2,
            "4": 8,
            "5": 10
        }
    },
    "reviews": [
        {
            "ageCategory": "18 to 24",
            "date": "2019-07-09T18:36:12.219",
            "dislikeCount": 0,
            "id": 307290,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": "Oily",
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 2060,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/warm.svg",
                    "name": "Warm"
                },
                {
                    "id": 2068,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/refined.svg",
                    "name": "Refined"
                }
            ],
            "text": "Delightful figgy-floral! I honestly don't understand the controversy over this perfume at all.",
            "title": "Please Bring Back!",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 4011511,
                "likeTotal": 9,
                "reviewsTotal": 8,
                "shippedCount": 1,
                "url": "/p/4011511",
                "username": "Arden"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-08T18:22:29.964",
            "dislikeCount": 0,
            "id": 13533,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "It was a bit too floral for me. The scent was really strong and it gave me a headache. I only used it 2 times.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 150908,
                "likeTotal": 6,
                "reviewsTotal": 6,
                "shippedCount": 11,
                "url": "/p/150908",
                "username": "Jenny"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-08T03:08:32.192",
            "dislikeCount": 0,
            "id": 13406,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This smells more masculine than feminine. Takes some getting used to at first but gets better after about an hour however, I hadn't gotten any comments on it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 207101,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/207101",
                "username": "Shijun"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-05T20:48:17.968",
            "dislikeCount": 0,
            "id": 12888,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "did not like the smell, it was too strong and not what I expected, so glad I got to sample before purchase. But there is another Womanity Eau Pour Elles that I have a sample on and does smell good and clean and fresh, nothing like the original.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 194784,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/194784",
                "username": "Bonnie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-03T01:35:04.526",
            "dislikeCount": 0,
            "id": 12293,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I love the sent but I put it in my purse and it all leaked out. sooo sad",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13980507221447800229021.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 174783,
                "likeTotal": 6,
                "reviewsTotal": 3,
                "shippedCount": 1,
                "url": "/p/174783",
                "username": "Frances"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-30T16:58:40.431",
            "dislikeCount": 0,
            "id": 11704,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this perfume as well as thierry mugler other frangrances",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12090855721455905271705.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 117525,
                "likeTotal": 9,
                "reviewsTotal": 11,
                "shippedCount": 30,
                "url": "/p/117525",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-05T02:30:50.212",
            "dislikeCount": 0,
            "id": 9111,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Nice!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13319574621459360075056.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 40521,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 28,
                "url": "/p/40521",
                "username": "Holly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-04T22:05:16.751",
            "dislikeCount": 0,
            "id": 9073,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "At first spray i thought that I would like it because of the sweet smell, but after 2 to  seconds it gave me smokey motel flavor. ( and yes, I know what a smokey motel smells like). If you are a smoker this might be your cup of tea, but for me NO!!!!!!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 167814,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 2,
                "url": "/p/167814",
                "username": "Naomi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-03T11:43:00.05",
            "dislikeCount": 0,
            "id": 8753,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Given the apparent controversy about this scent, I was surprised to discover how gentle and low-key it is. Or perhaps I shouldn't be-- I come from the world of indie and niche perfume, where unique and evocative fragrances are strongly valued. I 'm having a little bit of culture shock in a community where \"It's not feminine\" is considered an insult to a scent. At any rate, I find this perfume to be a pleasant, airy scent-- a bit of an ocean breeze, but mostly green fig leaves with grounding from fig wood, and a hint of sweet from fig fruit. I think it's appropriate for most situations-- not sickly sweet, not too attention-grabbing, not too sexy. Very adaptable.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 75722,
                "likeTotal": 64,
                "reviewsTotal": 26,
                "shippedCount": 33,
                "url": "/p/75722",
                "username": "Alysson"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-01T22:51:23.859",
            "dislikeCount": 0,
            "id": 8476,
            "isUseful": null,
            "likeCount": 1,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "all of thieery mugler scents suck too strong, too old lady like",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 170150,
                "likeTotal": 7,
                "reviewsTotal": 3,
                "shippedCount": 1,
                "url": "/p/170150",
                "username": "abbey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-31T01:24:15.189",
            "dislikeCount": 0,
            "id": 8169,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Beautiful scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 162252,
                "likeTotal": 2,
                "reviewsTotal": 6,
                "shippedCount": 2,
                "url": "/p/162252",
                "username": "Deseree"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-30T12:47:07.755",
            "dislikeCount": 0,
            "id": 7905,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I thought it had a strange after spray scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 137746,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 13,
                "url": "/p/137746",
                "username": "Marla"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-29T21:26:11.215",
            "dislikeCount": 0,
            "id": 7187,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love and hate for me.. I had received a sample a years and years ago and I thought I remembered being absolutely enamored with the scent.. Well I received it and did not have the same feeling.. It's woodsy and a little musky... Definitely not an everyday wear for me.. But decent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12199962571446740189344.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 70318,
                "likeTotal": 4,
                "reviewsTotal": 6,
                "shippedCount": 19,
                "url": "/p/70318",
                "username": "Sia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-20T04:59:59.028",
            "dislikeCount": 0,
            "id": 6281,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "its strong at 1st spray but not overpowering.i get the fig as the 1st note but as it blends and settle with your chemistry .it become very pleasant light fresh clean straight out the shower scent .like a cool breeze on a hot beach.although i like it... i'm not crazy about it but its wearable..wouldn't purchase",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/2593493791449804778663.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 35420,
                "likeTotal": 12,
                "reviewsTotal": 18,
                "shippedCount": 32,
                "url": "/p/35420",
                "username": "rellina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-18T01:42:13.946",
            "dislikeCount": 0,
            "id": 6178,
            "isUseful": null,
            "likeCount": 0,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Nope nope nope nope.  At first whiff, I liked the sweetness of the fig.  Then I spritzed it on my wrists and the caviar came out, and the more I smelled it, the more I disliked it.  Tried to wash it off but it's still there . . . ick.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 80879,
                "likeTotal": 11,
                "reviewsTotal": 7,
                "shippedCount": 4,
                "url": "/p/80879",
                "username": "Kimberly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-05T18:40:16.896",
            "dislikeCount": 0,
            "id": 5315,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                }
            ],
            "text": "Didn't care for this at all, it is sickly sweet and also deep and woody but both at once...very overpowering.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 38750,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 12,
                "url": "/p/38750",
                "username": "Chelsea"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-01T23:17:58.607",
            "dislikeCount": 0,
            "id": 4748,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This is one of the most sexy scents that I have! It's soft, shy and innocent with a little \"vamp\" to it! LOVE IT!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 58801,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/58801",
                "username": "Shafeeqah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T00:11:51.827",
            "dislikeCount": 0,
            "id": 3508,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Not bad but not for me",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1281788541446184646654.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 85625,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 13,
                "url": "/p/85625",
                "username": "Richye"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T00:10:02.708",
            "dislikeCount": 0,
            "id": 3499,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It's not bad but definitely not for me. A friend suggested it and I'm pretty sure it smelled different on her than on me. NOT A FAN AT ALL!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1281788541446184646654.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 85625,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 13,
                "url": "/p/85625",
                "username": "Richye"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-22T14:28:43.937",
            "dislikeCount": 0,
            "id": 2814,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I'm in love with this perfume!  I love the scent of figs and this one definitely shines in that area.  It's spicy but not too overpowering.  It is a sexy scent but also light and beautiful.  It's a unique scent,  which I adore.  I hate smelling like everyone else.  This one lets you stand out in a crowd!!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1328313901442932137621.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 66157,
                "likeTotal": 35,
                "reviewsTotal": 11,
                "shippedCount": 26,
                "url": "/p/66157",
                "username": "Katrina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-19T23:49:38.558",
            "dislikeCount": 0,
            "id": 2704,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This is a such a lovely fragrance. The hint of caviar gives it a sexy twist, it's very intimate and feminine, not fishy at all. I'm going to put this one into rotation right away.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 131775,
                "likeTotal": 19,
                "reviewsTotal": 8,
                "shippedCount": 3,
                "url": "/p/131775",
                "username": "Shannon"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-19T21:00:02.538",
            "dislikeCount": 0,
            "id": 2698,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 33,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sweet.svg",
                    "name": "Sweet"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I do like this scent...might be over powering for some, defiantly a night perfume. its a mix between heavy cardamom, flora and sheer remnants of vanilla. once settled and relaxes its fruity flowery... girly, very womanly! My b/f said he liked it...thats the 1st!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 106864,
                "likeTotal": 9,
                "reviewsTotal": 7,
                "shippedCount": 19,
                "url": "/p/106864",
                "username": "brittany"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-09T02:52:58.911",
            "dislikeCount": 0,
            "id": 2551,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I loooooovvvvvveeee this scent!  The fragrance lasts all day, and until you shower the next day!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 72315,
                "likeTotal": 5,
                "reviewsTotal": 5,
                "shippedCount": 9,
                "url": "/p/72315",
                "username": "cheryl"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-03T22:03:08.247",
            "dislikeCount": 0,
            "id": 2269,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Definitely \"woody\" and musky...a little citrus & mystery...very winter/fall scent to me. Felt heavy...not a favorite of mine",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 102345,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/102345",
                "username": "Kristen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-02T11:45:55.85",
            "dislikeCount": 0,
            "id": 2156,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 91516,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/91516",
                "username": "Alicia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-02T10:47:43.818",
            "dislikeCount": 0,
            "id": 2154,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I love it. It's spicy and earthy with out being overly masculine. I don't like a lot of.things but.I love this!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 90516,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/90516",
                "username": "Alicia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-01T17:25:14.922",
            "dislikeCount": 0,
            "id": 2090,
            "isUseful": null,
            "likeCount": 6,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "TM fragrances are so polarizing, you either hate em or love em... \nI LOVE Womanity, it's so fresh and figgy, yet salty/savory. Lasts for hours, days even on clothes.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14348506641458595052140.jpg",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 77284,
                "likeTotal": 56,
                "reviewsTotal": 20,
                "shippedCount": 12,
                "url": "/p/77284",
                "username": "elly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T06:47:23.658",
            "dislikeCount": 0,
            "id": 1261,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It's really too bad I can't actually rate this with negative hearts. This stuff smells so bad it is currently sitting in a plastic sandwich bag on my table waiting for me to take it to the garbage can outside. It is putrid and disgusting. WTF! I'm not even sure how this earns the label perfume.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 47795,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 15,
                "url": "/p/47795",
                "username": "Veronica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T02:11:58.648",
            "dislikeCount": 0,
            "id": 1207,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Not a big fan of this one.\ud83d\ude1f",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 32858,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/32858",
                "username": "Constance"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-17T08:50:14.735",
            "dislikeCount": 0,
            "id": 839,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I gave it to my mom because I couldn't stand how disgusting it is.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 64284,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 7,
                "url": "/p/64284",
                "username": "Adrian"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-07T19:48:57.091",
            "dislikeCount": 1,
            "id": 753,
            "isUseful": null,
            "likeCount": 3,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Nothing special.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 47953,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/47953",
                "username": "Christy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-06T21:04:14.985",
            "dislikeCount": 0,
            "id": 711,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                }
            ],
            "text": "Too strong",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 62659,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/62659",
                "username": "Christina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-05T01:24:18.137",
            "dislikeCount": 1,
            "id": 589,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 33,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sweet.svg",
                    "name": "Sweet"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "The undertones are lovely.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 54869,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/54869",
                "username": "Aida"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-07-12T15:50:33",
            "dislikeCount": 0,
            "id": -564,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This smelt heavenly! spray it on in the morning and you would smell it all day! I love this one almost as much as i love &quot;Angel&quot;. A definite must have!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 66327,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 1,
                "url": "/p/66327",
                "username": "Sabrina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-05-27T14:51:27",
            "dislikeCount": 0,
            "id": -624,
            "isUseful": null,
            "likeCount": 5,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"This fragrance is extremely bold and sexy without being spicy or floral. It's an extremely unique complex scent. I found myself smelling my wrists all day. The caviar produces a clean, salty smell that dare I say smells a little like a romp in the sheets... In a GOOD way. While wearing this my boyfriend leans in extremely close. This will be a fragrance that I would make the leap and purchase straight out.\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 46763,
                "likeTotal": 11,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/46763",
                "username": "Teshema"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-05-01T19:04:49",
            "dislikeCount": 0,
            "id": -38,
            "isUseful": null,
            "likeCount": 5,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "\"This fragrance is not for everyone. People tend to love it or hate it. The caviar note to me doesn't smell fishy at all, but rather beachy, like the air near an ocean. This smells fantastic, but it's strong and very different than typical &quot;feminine&quot; perfumes. Thierry Mugler is a master at making controversial fragrances, so beware if you have issues with his other fragrances Angel or Alien. This is definitely my favorite scentbird scent thus far though, so I'm really happy with it!\"",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12672348401456772367704.jpg",
                "dislikeTotal": 5,
                "gender": "female",
                "id": 34077,
                "likeTotal": 69,
                "reviewsTotal": 24,
                "shippedCount": 31,
                "url": "/p/34077",
                "username": "Ashleigh"
            },
            "userAge": null
        }
    ],
    "userReview": null
}