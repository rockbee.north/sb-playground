{
    "expertReview": null,
    "rating": {
        "average": 4.2,
        "reviewCount": 20,
        "reviews": {
            "1": 0,
            "2": 0,
            "3": 6,
            "4": 4,
            "5": 10
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2018-05-03T02:17:58.698",
            "dislikeCount": 0,
            "id": 208336,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2058,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/uplifting.svg",
                    "name": "Uplifting"
                },
                {
                    "id": 2053,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/feminine.svg",
                    "name": "Feminine"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 2057,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/light.svg",
                    "name": "Light"
                },
                {
                    "id": 2067,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/easy-going.svg",
                    "name": "Easy-going"
                }
            ],
            "text": "This is a lovely & girly scent for spring and summer!",
            "title": "Lovely",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/15632829371446778759265.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 65923,
                "likeTotal": 19,
                "reviewsTotal": 22,
                "shippedCount": 29,
                "url": "/p/65923",
                "username": "Jamee"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-06T22:33:07.409",
            "dislikeCount": 0,
            "id": 24732,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "The best smell by far love it so much",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 168907,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 0,
                "url": "/p/168907",
                "username": "sylvia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T22:53:33.967",
            "dislikeCount": 0,
            "id": 20259,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It is an ok scent. I have not worn it yet.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13031549441447728347773.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 56213,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/56213",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T22:48:35.794",
            "dislikeCount": 0,
            "id": 20248,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love this scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 3,
                "gender": "female",
                "id": 64203,
                "likeTotal": 7,
                "reviewsTotal": 10,
                "shippedCount": 7,
                "url": "/p/64203",
                "username": "odessa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-01T01:23:06.307",
            "dislikeCount": 0,
            "id": 16698,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Nice clean scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 222389,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 0,
                "url": "/p/222389",
                "username": "Cassidy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-31T04:25:25.774",
            "dislikeCount": 0,
            "id": 16429,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Smells amazing. Would highly recommend this to anyone who likes citrus notes.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 157468,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 22,
                "url": "/p/157468",
                "username": "Angela"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-31T02:27:18.544",
            "dislikeCount": 0,
            "id": 16354,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love the fact that it's not overbearing",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 166811,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 13,
                "url": "/p/166811",
                "username": "Christine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-30T14:35:49.523",
            "dislikeCount": 0,
            "id": 15259,
            "isUseful": null,
            "likeCount": 3,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Very floral, a slight musk-y scent to it at the end. Better for Spring/Summer wear, IMHO!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/18399458671451486158670.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 243701,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/243701",
                "username": "Heather"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-12T20:56:44.583",
            "dislikeCount": 0,
            "id": 9826,
            "isUseful": null,
            "likeCount": 3,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Cute lovely simple perfume.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 57227,
                "likeTotal": 16,
                "reviewsTotal": 10,
                "shippedCount": 8,
                "url": "/p/57227",
                "username": "Alanna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-12T06:23:56.965",
            "dislikeCount": 0,
            "id": 9814,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Pretty good. Im a huge fan of romance, this is an ok version.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 103818,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 25,
                "url": "/p/103818",
                "username": "Corey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-08T00:02:08.219",
            "dislikeCount": 0,
            "id": 9664,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Smells so good\ud83d\ude4c\ud83c\udffe\ud83d\ude4c\ud83c\udffe\ud83d\ude4c\ud83c\udffe",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 181126,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/181126",
                "username": "Veronica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-30T01:45:40.311",
            "dislikeCount": 0,
            "id": 7626,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love, love, love this scent!  Wish I had more.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/21435654301446169559925.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 179993,
                "likeTotal": 7,
                "reviewsTotal": 7,
                "shippedCount": 11,
                "url": "/p/179993",
                "username": "Natalie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-08T03:26:35.664",
            "dislikeCount": 0,
            "id": 5800,
            "isUseful": null,
            "likeCount": 3,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I like it not my every day favorite but a twice a weaker.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/18225308311447248552505.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 146275,
                "likeTotal": 12,
                "reviewsTotal": 9,
                "shippedCount": 6,
                "url": "/p/146275",
                "username": "Kim"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-03T03:28:26.16",
            "dislikeCount": 0,
            "id": 4989,
            "isUseful": null,
            "likeCount": 0,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Mmmm",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 2,
                "gender": "female",
                "id": 39977,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 10,
                "url": "/p/39977",
                "username": "TIFFANY"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-01T00:47:45.501",
            "dislikeCount": 0,
            "id": 4517,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It smells great! Doesn't last long,but is a great scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 55481,
                "likeTotal": 4,
                "reviewsTotal": 6,
                "shippedCount": 10,
                "url": "/p/55481",
                "username": "Bianca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T00:59:53.483",
            "dislikeCount": 0,
            "id": 3670,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This scent has a very 'pur",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 77157,
                "likeTotal": 4,
                "reviewsTotal": 7,
                "shippedCount": 5,
                "url": "/p/77157",
                "username": "Lauren"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-22T05:57:57.27",
            "dislikeCount": 0,
            "id": 2806,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I think I was expecting something more summery. It smells good, but wasn't what I was expecting. Lasts only maybe a third of the day so not my stay power compared to romance.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 135683,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/135683",
                "username": "Brooke"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T16:04:14.686",
            "dislikeCount": 0,
            "id": 1528,
            "isUseful": null,
            "likeCount": 5,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I think it smells really good BUT ....it doesn't last long enough for me. It is a very good summery scent but it does not have to much stay power... at least on my skin. If you do decide to try this or purchase an actual bottle be prepared to have to reapply your scent throughout the day.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/15776731131449240076157.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 77417,
                "likeTotal": 12,
                "reviewsTotal": 6,
                "shippedCount": 5,
                "url": "/p/77417",
                "username": "Chelsea"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-06T15:59:30.203",
            "dislikeCount": 0,
            "id": 700,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 33,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sweet.svg",
                    "name": "Sweet"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Its a great light floral scent. It fades throughout the day fast so reapplication is needed.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 81869,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/81869",
                "username": "Natalie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T18:39:18.414",
            "dislikeCount": 0,
            "id": 532,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Romantic is the word that comes to mind.  Floral, but not overpowering, slight muskiness detected after some time has passed.  Beautiful fragrance!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 34682,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 14,
                "url": "/p/34682",
                "username": "Helene"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T14:29:58.805",
            "dislikeCount": 1,
            "id": 461,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This scent has a delicious light scent.  I absolutely love it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 52705,
                "likeTotal": 5,
                "reviewsTotal": 11,
                "shippedCount": 21,
                "url": "/p/52705",
                "username": "Mauri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-03T01:30:02.83",
            "dislikeCount": 0,
            "id": 53,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love this scent.  Thank you for introducing me to a new favorite.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 38391,
                "likeTotal": 10,
                "reviewsTotal": 10,
                "shippedCount": 35,
                "url": "/p/38391",
                "username": "Elizabeth Roxanne"
            },
            "userAge": null
        }
    ],
    "userReview": null
}