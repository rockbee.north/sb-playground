{
    "expertReview": null,
    "rating": {
        "average": 4.0,
        "reviewCount": 526,
        "reviews": {
            "1": 50,
            "2": 46,
            "3": 58,
            "4": 60,
            "5": 312
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2018-12-22T15:10:52.2",
            "dislikeCount": 0,
            "id": 262660,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 127,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/classic.svg",
                    "name": "Classic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2068,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/refined.svg",
                    "name": "Refined"
                }
            ],
            "text": "I love this fragrance!  It has an awesome sillage and lasts a long time.  It has a distinct scent that is classic and clean.  I highly recommend",
            "title": "LOVE",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 108152,
                "likeTotal": 22,
                "reviewsTotal": 13,
                "shippedCount": 31,
                "url": "/p/108152",
                "username": "Kathie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-07T13:31:34.681",
            "dislikeCount": 0,
            "id": 103388,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I loved the scent but wished it lasted longer",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/19066411981475947139258.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 565791,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 17,
                "url": "/p/565791",
                "username": "Naressa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-06T16:46:48.595",
            "dislikeCount": 0,
            "id": 103122,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this smell.  It is refreshing and light.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 897158,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/897158",
                "username": "susan"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-06T13:22:24.34",
            "dislikeCount": 0,
            "id": 103047,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not a big fan.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 921605,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/921605",
                "username": "rebecca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-06T01:12:27.702",
            "dislikeCount": 0,
            "id": 102915,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Absolutely love this light, fresh scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 735887,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/735887",
                "username": "Kaylin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-05T20:11:42.805",
            "dislikeCount": 0,
            "id": 102806,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "5 hearts love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 942115,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 19,
                "url": "/p/942115",
                "username": "Sherri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-04T18:30:32.975",
            "dislikeCount": 0,
            "id": 102147,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Lovely scent. It's not overly feminine or floral. It's just a nice bright, lightly floral scent. I found it to be perfect for winter.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9738433371481322640020.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 675927,
                "likeTotal": 8,
                "reviewsTotal": 5,
                "shippedCount": 19,
                "url": "/p/675927",
                "username": "Carli"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-04T16:32:28.607",
            "dislikeCount": 0,
            "id": 102047,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "it ok but does not last long",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 275091,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/275091",
                "username": "Tonya"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-04T03:41:25.1",
            "dislikeCount": 0,
            "id": 101536,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "My absolute favorite!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 812643,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 9,
                "url": "/p/812643",
                "username": "Courtney"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T23:28:50.165",
            "dislikeCount": 0,
            "id": 101120,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love it very light and fresh smelling.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 338307,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 18,
                "url": "/p/338307",
                "username": "India"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T20:28:51.186",
            "dislikeCount": 0,
            "id": 100986,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Smells great",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9901318191481872222878.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 674499,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 9,
                "url": "/p/674499",
                "username": "Constance "
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T18:31:32.76",
            "dislikeCount": 0,
            "id": 100857,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I absolutely love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 428543,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 9,
                "url": "/p/428543",
                "username": "Ellen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T18:29:24.807",
            "dislikeCount": 0,
            "id": 100856,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Not a fan",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16288175071463452371772.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 439029,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 9,
                "url": "/p/439029",
                "username": "Lynette N Leo"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T18:18:43.997",
            "dislikeCount": 0,
            "id": 100842,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it!!!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 787996,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/787996",
                "username": "Nancy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T18:03:22.437",
            "dislikeCount": 0,
            "id": 100828,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I absolutly love this scent. It's by far my favorite so far.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 574932,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 15,
                "url": "/p/574932",
                "username": "Julie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T15:38:02.045",
            "dislikeCount": 0,
            "id": 100636,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12585736041475769519502.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 853947,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/853947",
                "username": "Amie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T15:27:04.32",
            "dislikeCount": 0,
            "id": 100610,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "An oldie but a goodie. Brings back memories :)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 56928,
                "likeTotal": 1,
                "reviewsTotal": 6,
                "shippedCount": 12,
                "url": "/p/56928",
                "username": "Shana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T13:48:23.534",
            "dislikeCount": 0,
            "id": 100515,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love this ! Everyone commented on my perfume while I was wearing it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 940337,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 16,
                "url": "/p/940337",
                "username": "julie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T12:07:36.193",
            "dislikeCount": 0,
            "id": 100477,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love the smell of this perfume. Delicate and floral. I will definitely buy a larger bottle soon!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3106199231469327147019.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 586404,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/586404",
                "username": "Melissa "
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T11:53:01.607",
            "dislikeCount": 0,
            "id": 100473,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "the smell is very good it's a sweet smell I like it very much",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 506075,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 11,
                "url": "/p/506075",
                "username": "tera"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-03T02:48:37.231",
            "dislikeCount": 0,
            "id": 100291,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It's a nice sent, but not one of my top favorite.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 889579,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/889579",
                "username": "Lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T23:42:03.159",
            "dislikeCount": 0,
            "id": 100124,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Great scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 931168,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 13,
                "url": "/p/931168",
                "username": "Amanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T23:33:19.717",
            "dislikeCount": 0,
            "id": 100114,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this one! A fresh, clean, beautiful scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 282258,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 15,
                "url": "/p/282258",
                "username": "Jamie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T21:49:42.124",
            "dislikeCount": 0,
            "id": 99999,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I've loved this perfume for a long time.  It's a very sophisticated scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17830803241488149171320.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 716143,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 13,
                "url": "/p/716143",
                "username": "Chrystal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T21:18:49.024",
            "dislikeCount": 0,
            "id": 99967,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love it!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9746934081474935107132.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 815513,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/815513",
                "username": "Heather"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T14:53:43.724",
            "dislikeCount": 0,
            "id": 99444,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I Love This Scent, So Far I've Enjoyed Every Scent I've Recieved..",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 731413,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 21,
                "url": "/p/731413",
                "username": "Ramonita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T12:11:44.538",
            "dislikeCount": 0,
            "id": 99342,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Blue is a prefect everyday going to the office scent.  Its has a fresh sweet smell. I would buy a full bottle",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 886668,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/886668",
                "username": "Camie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T07:23:25.492",
            "dislikeCount": 0,
            "id": 99257,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "The fragrance is nice but it doesn't last long.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 699670,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/699670",
                "username": "Teresa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T05:49:25.394",
            "dislikeCount": 0,
            "id": 99203,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love it..",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 841845,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 19,
                "url": "/p/841845",
                "username": "Angela"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T02:20:23.291",
            "dislikeCount": 0,
            "id": 98894,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Looove this one! My favorite !!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 713357,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/713357",
                "username": "beth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-02T00:24:29.507",
            "dislikeCount": 0,
            "id": 98621,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17739941251446140090124.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 81062,
                "likeTotal": 10,
                "reviewsTotal": 6,
                "shippedCount": 13,
                "url": "/p/81062",
                "username": "Holly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T22:40:24.92",
            "dislikeCount": 0,
            "id": 98494,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I never received  it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/317334871479609760520.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 925574,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/925574",
                "username": "Deborah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T22:34:13.931",
            "dislikeCount": 0,
            "id": 98483,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I like it well enough, its a tad sweeter than what I expected.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 765334,
                "likeTotal": 1,
                "reviewsTotal": 6,
                "shippedCount": 11,
                "url": "/p/765334",
                "username": "Libby"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T22:11:15.347",
            "dislikeCount": 0,
            "id": 98460,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I love the scent!  It is light and airy but it is bold. I love the bottom notes!  I recommend this as an everyday fragrance.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 444622,
                "likeTotal": 0,
                "reviewsTotal": 4,
                "shippedCount": 28,
                "url": "/p/444622",
                "username": "Ramona"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T21:53:36.54",
            "dislikeCount": 0,
            "id": 98438,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Loved it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 902603,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 15,
                "url": "/p/902603",
                "username": "Denise"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T21:39:55.717",
            "dislikeCount": 0,
            "id": 98424,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love it. Very clean crisp smell... Wonderful!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 773311,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 15,
                "url": "/p/773311",
                "username": "Tamra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T19:51:24.151",
            "dislikeCount": 0,
            "id": 98306,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                }
            ],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 926617,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/926617",
                "username": "veronica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T15:08:59.406",
            "dislikeCount": 0,
            "id": 98082,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Not a huge fan, scent was pretty strong",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 618421,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/618421",
                "username": "Sabrina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T07:20:47.946",
            "dislikeCount": 0,
            "id": 97939,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 327166,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 20,
                "url": "/p/327166",
                "username": "Tashelle"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T05:20:33.627",
            "dislikeCount": 0,
            "id": 97882,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This scent smells like blue looks. Its clean and fresh",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13141448191477968578916.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 898088,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/898088",
                "username": "suzy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2017-01-01T05:08:29.823",
            "dislikeCount": 0,
            "id": 97875,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Didn't really care for this fragrance. Kind of disappointed.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 631442,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 18,
                "url": "/p/631442",
                "username": "Rhonda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-31T22:31:14.395",
            "dislikeCount": 0,
            "id": 97448,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Nice scent",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/21440992741471845895974.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 659943,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 11,
                "url": "/p/659943",
                "username": "shereace"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-31T20:34:05.172",
            "dislikeCount": 0,
            "id": 97236,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I was not over the moon about this fragrance.   I tdoesn't seem to last as long as some of the others I have tried.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 825904,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 21,
                "url": "/p/825904",
                "username": "Karen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-31T17:25:57.533",
            "dislikeCount": 0,
            "id": 96911,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Smells nice,  doesn't last.  I was disappointed.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 912359,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/912359",
                "username": "Dana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-31T08:55:06.753",
            "dislikeCount": 0,
            "id": 96447,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Luv it?",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 779157,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/779157",
                "username": "Alice"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-31T06:51:03.44",
            "dislikeCount": 0,
            "id": 96409,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Nice surprise",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 753898,
                "likeTotal": 10,
                "reviewsTotal": 11,
                "shippedCount": 11,
                "url": "/p/753898",
                "username": "Alicia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-30T17:58:36.79",
            "dislikeCount": 0,
            "id": 95961,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love this scent!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 783487,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/783487",
                "username": "Isabel"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-30T16:56:59.196",
            "dislikeCount": 0,
            "id": 95892,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Nice fragrance",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 657179,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 17,
                "url": "/p/657179",
                "username": "LC"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-30T16:12:06.358",
            "dislikeCount": 0,
            "id": 95843,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "It smells ok. It's not my favorite. It does smell very clean and flowery.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7409764701473843876723.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 508871,
                "likeTotal": 0,
                "reviewsTotal": 5,
                "shippedCount": 14,
                "url": "/p/508871",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-30T15:47:01.525",
            "dislikeCount": 0,
            "id": 95808,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This is an awesome sent. it's actually my second favorite scent since I started getting scentbird. And my first would have to be black opium.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 571084,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 23,
                "url": "/p/571084",
                "username": "Melissa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-30T14:58:28.563",
            "dislikeCount": 0,
            "id": 95723,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "My favorite",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 923137,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/923137",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-30T14:28:25.124",
            "dislikeCount": 0,
            "id": 95681,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I like the smell of this one",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 753496,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 14,
                "url": "/p/753496",
                "username": "lacey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-30T09:52:44.934",
            "dislikeCount": 0,
            "id": 95429,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I love this, I will be purchasing a full size.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 783678,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 15,
                "url": "/p/783678",
                "username": "Keishs"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-29T21:19:01.951",
            "dislikeCount": 0,
            "id": 95141,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It just was strong for me",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 808375,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/808375",
                "username": "crystal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-29T16:48:53.647",
            "dislikeCount": 0,
            "id": 94954,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "ONE OF MY FAVORITE",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 855681,
                "likeTotal": 1,
                "reviewsTotal": 5,
                "shippedCount": 15,
                "url": "/p/855681",
                "username": "OPIDELLA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-29T11:54:34.538",
            "dislikeCount": 0,
            "id": 94834,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Nice floral scent, very light can be worn in the spring.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 360920,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 15,
                "url": "/p/360920",
                "username": "Barbara"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-29T05:03:39.987",
            "dislikeCount": 0,
            "id": 94793,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It's very nice, not too strong, last through the day. I give it all hearts. I recommend it to all.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 924439,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/924439",
                "username": "Dianna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-08T18:19:05.173",
            "dislikeCount": 0,
            "id": 93255,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 533341,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/533341",
                "username": "jaylene"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-08T14:11:34.218",
            "dislikeCount": 0,
            "id": 93186,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love this",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 777584,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 22,
                "url": "/p/777584",
                "username": "donna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-08T13:56:52.861",
            "dislikeCount": 0,
            "id": 93183,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The best fragrance yet, this is such an extra warm, spicy smell. Love it .",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 507342,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 16,
                "url": "/p/507342",
                "username": "Terri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-07T15:38:08.213",
            "dislikeCount": 0,
            "id": 92855,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I did not get the fragrance l selected, l got pinrose wild child. Don't like it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 919236,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/919236",
                "username": "Rhonda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-07T14:18:06.781",
            "dislikeCount": 0,
            "id": 92821,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love love love",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 73651,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 12,
                "url": "/p/73651",
                "username": "Ishica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-07T03:13:19.979",
            "dislikeCount": 0,
            "id": 92705,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I LOVE this scent!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 864954,
                "likeTotal": 4,
                "reviewsTotal": 9,
                "shippedCount": 19,
                "url": "/p/864954",
                "username": "Carrie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-07T03:03:07.812",
            "dislikeCount": 0,
            "id": 92700,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "This scent is really fresh and neutral smelling. Sometimes it can smell like a cologne, to me at least. If you're into that sporty woman's smell this is for you.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/2038620521450467214509.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 243217,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 10,
                "url": "/p/243217",
                "username": "Millen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-05T19:18:49.422",
            "dislikeCount": 0,
            "id": 92001,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I absolutely love this fragrance  (Ralph Lauren, Blue). It has a fresh clean smell with a citrus/honeysuckle after tone. I imagine someone well put together wearing this fragrance. Great for the boardroom and a day of lounging around...very versatile, it's my new go to!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14525306581480023633450.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 537396,
                "likeTotal": 7,
                "reviewsTotal": 3,
                "shippedCount": 11,
                "url": "/p/537396",
                "username": "Chris"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-05T12:37:54.285",
            "dislikeCount": 0,
            "id": 91753,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 848598,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 12,
                "url": "/p/848598",
                "username": "makka"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-04T21:30:21.799",
            "dislikeCount": 0,
            "id": 91400,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Very nice",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 760624,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 13,
                "url": "/p/760624",
                "username": "holly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-04T20:52:02.475",
            "dislikeCount": 0,
            "id": 91382,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it \u2764\u2764\u2764",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 926322,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 10,
                "url": "/p/926322",
                "username": "Racheal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-04T18:16:48.267",
            "dislikeCount": 0,
            "id": 91308,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "My very favorite",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 808688,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 9,
                "url": "/p/808688",
                "username": "susan"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-04T17:24:24.134",
            "dislikeCount": 0,
            "id": 91282,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Beautiful everyday scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 52539,
                "likeTotal": 1,
                "reviewsTotal": 5,
                "shippedCount": 13,
                "url": "/p/52539",
                "username": "Christian"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-04T17:21:00.333",
            "dislikeCount": 0,
            "id": 91281,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Not a big fan of the scent.  Too floral for me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 913994,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/913994",
                "username": "grace"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-04T14:54:18.186",
            "dislikeCount": 0,
            "id": 91218,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Nice for an everday smell.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 718848,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/718848",
                "username": "Cathryn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-04T04:21:34.548",
            "dislikeCount": 0,
            "id": 91060,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "i love it, ive gotten 2 bottles from ralph lauren n i have loved both of them",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 449566,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 11,
                "url": "/p/449566",
                "username": "tracy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-03T18:10:17.512",
            "dislikeCount": 0,
            "id": 90634,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It smells really good but not as good on me. I guess my body chemistry didn't mix that we'll with it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 645094,
                "likeTotal": 0,
                "reviewsTotal": 6,
                "shippedCount": 13,
                "url": "/p/645094",
                "username": "Roxanne"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-03T15:59:46.533",
            "dislikeCount": 0,
            "id": 90479,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I did not like this scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 524051,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 10,
                "url": "/p/524051",
                "username": "Jasmin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-03T15:47:56.352",
            "dislikeCount": 0,
            "id": 90467,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I did not care for this scent at all!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 505900,
                "likeTotal": 6,
                "reviewsTotal": 8,
                "shippedCount": 23,
                "url": "/p/505900",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-03T14:02:08.215",
            "dislikeCount": 0,
            "id": 90351,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I used to love this perfume when It first came out. I haven't smelt it in a while and decided to get it. It doesn't smell bad but i don't like it as much as I used to :(",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 761669,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 4,
                "url": "/p/761669",
                "username": "Deona"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-03T13:16:19.792",
            "dislikeCount": 0,
            "id": 90323,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "One of my favorites. Not a big fan of floral but this one is good it has a very clean crisp smell to it as well. I can always smell it on my clothes the next day. Love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 859636,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/859636",
                "username": "Felicia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-03T01:59:43.396",
            "dislikeCount": 0,
            "id": 89867,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I love this fragrance! It is an all around perfect fragrance for anytime!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11618186401480730803886.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 842862,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 17,
                "url": "/p/842862",
                "username": "torina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-03T00:46:35.993",
            "dislikeCount": 0,
            "id": 89751,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I didn't care for this perfume, and I usually love floral scents. It actually smelled like hairspray to me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 816818,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 13,
                "url": "/p/816818",
                "username": "Juli"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-03T00:35:56.486",
            "dislikeCount": 0,
            "id": 89739,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Much too floral.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 349643,
                "likeTotal": 4,
                "reviewsTotal": 7,
                "shippedCount": 10,
                "url": "/p/349643",
                "username": "Brenda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T23:56:19.972",
            "dislikeCount": 0,
            "id": 89666,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I did not like this scent and went for me!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 505900,
                "likeTotal": 6,
                "reviewsTotal": 8,
                "shippedCount": 23,
                "url": "/p/505900",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T23:30:04.629",
            "dislikeCount": 0,
            "id": 89622,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I absolutely love love love this sent. This has been my favorite one so far. Please get this one.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 467921,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 18,
                "url": "/p/467921",
                "username": "Tiffany"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T22:59:02.531",
            "dislikeCount": 0,
            "id": 89560,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I absolutely love this scent, highly recommend to anyone who loves floral scents that aren't overpowering.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7364020721480719585663.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 743963,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 9,
                "url": "/p/743963",
                "username": "Denise"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T22:54:53.752",
            "dislikeCount": 0,
            "id": 89551,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Absolutely love this perfume! It's very clean smelling!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 836261,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/836261",
                "username": "Amanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T22:53:24.816",
            "dislikeCount": 0,
            "id": 89549,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 876480,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 19,
                "url": "/p/876480",
                "username": "ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T22:23:51.487",
            "dislikeCount": 0,
            "id": 89480,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "My favorite by far!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 559266,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 12,
                "url": "/p/559266",
                "username": "daphne"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T22:20:29.373",
            "dislikeCount": 0,
            "id": 89475,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Didn't like at all!!!  Only lasted 10 minutes!!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 812946,
                "likeTotal": 2,
                "reviewsTotal": 6,
                "shippedCount": 13,
                "url": "/p/812946",
                "username": "Desirre"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T22:18:45.105",
            "dislikeCount": 0,
            "id": 89472,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Hated it!!!! Only lasted 10mins!!!  Smelt horrible!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 812946,
                "likeTotal": 2,
                "reviewsTotal": 6,
                "shippedCount": 13,
                "url": "/p/812946",
                "username": "Desirre"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T21:26:33.466",
            "dislikeCount": 0,
            "id": 89367,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "My new favorite scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 476825,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/476825",
                "username": "Courtney"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T21:22:51.923",
            "dislikeCount": 0,
            "id": 89364,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I'm in love with this scent! It's definitely a must try!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 476825,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/476825",
                "username": "Courtney"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T21:01:50.585",
            "dislikeCount": 0,
            "id": 89316,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "love it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/19772806871451501028021.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 157677,
                "likeTotal": 6,
                "reviewsTotal": 8,
                "shippedCount": 17,
                "url": "/p/157677",
                "username": "Sophia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T20:34:52.318",
            "dislikeCount": 0,
            "id": 89271,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Disappointed, Too much floral and type of perfume my grandmother  would wear.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-185174891477001124794.jpeg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 858008,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/858008",
                "username": "Mimiann"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T20:16:04.775",
            "dislikeCount": 0,
            "id": 89232,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love!!\ud83d\udc95",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 878281,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 18,
                "url": "/p/878281",
                "username": "connie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T20:15:37.146",
            "dislikeCount": 0,
            "id": 89231,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This is by far my FAV scent to date!  The only way I can describe it is clean, definitive, and classic.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7503858771480709759111.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 738085,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 21,
                "url": "/p/738085",
                "username": "Colleen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T19:58:56.586",
            "dislikeCount": 0,
            "id": 89188,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It's not my favorite but it was ok.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17054775391476607933351.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 851507,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/851507",
                "username": "Nicole"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T19:54:39.143",
            "dislikeCount": 0,
            "id": 89179,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is my favorite so far. Soft and lasting without being overpowering. Beautiful!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3021244361474324944328.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 798507,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/798507",
                "username": "melissa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T19:53:28.878",
            "dislikeCount": 0,
            "id": 89176,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It was fine, I just like a heavier sent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/21232641091476418680533.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 862746,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/862746",
                "username": "Paula"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T18:23:47.342",
            "dislikeCount": 0,
            "id": 89008,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Terrible",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "male",
                "id": 849635,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/849635",
                "username": "Tiana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T18:00:47.071",
            "dislikeCount": 0,
            "id": 88944,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Lite  and a floral  sent, Love it. Get lot of compliments..",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7285482131467120236666.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 547722,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/547722",
                "username": "teresa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T14:58:12.195",
            "dislikeCount": 0,
            "id": 88574,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "smells wonderful",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 355953,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 16,
                "url": "/p/355953",
                "username": "Temeka"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T08:34:07.489",
            "dislikeCount": 0,
            "id": 88228,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I didn't really care for this one, the scent is a little too sporty fit me, I felt like it smelled more like men's Cologne then perfume, also it wares right off \ud83d\ude15",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 751613,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/751613",
                "username": "Alysha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T08:20:52.511",
            "dislikeCount": 0,
            "id": 88224,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "this is a true rose flower sent. Perfect for women who like something light, work appropriate and not \"sugary\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 517281,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 25,
                "url": "/p/517281",
                "username": "Regine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T02:23:15.556",
            "dislikeCount": 0,
            "id": 87886,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Omg this smells so good. Not your typical girly scent of candy and flowers but old lady like either. I think I've found my new favorite scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 134978,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 12,
                "url": "/p/134978",
                "username": "TYNETTA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T01:37:24.979",
            "dislikeCount": 0,
            "id": 87835,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love the scent, I would definitely buy a full size bottle",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11978131041475297592083.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 416367,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 33,
                "url": "/p/416367",
                "username": "Jermein"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-02T00:40:13.515",
            "dislikeCount": 0,
            "id": 87741,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love it!!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 877497,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/877497",
                "username": "kandida"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-01T21:18:58.685",
            "dislikeCount": 0,
            "id": 87512,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Light but flowery smell.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 716758,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 13,
                "url": "/p/716758",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-01T17:59:26.386",
            "dislikeCount": 0,
            "id": 87390,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Very nice after shower scent. More appropriate for summer and/or daytime wear.  I like to wear it to bed as well.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20323464851460752261587.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 183095,
                "likeTotal": 4,
                "reviewsTotal": 7,
                "shippedCount": 12,
                "url": "/p/183095",
                "username": "Katrina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-01T15:48:33.979",
            "dislikeCount": 0,
            "id": 87311,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "One of the best perfumes I've ever had!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 899485,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 15,
                "url": "/p/899485",
                "username": "lesley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-12-01T00:42:33.632",
            "dislikeCount": 0,
            "id": 86890,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Absolutely love the RL Blue fragrance.  It is very strong at first however it stays on throughout the day.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 890814,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 17,
                "url": "/p/890814",
                "username": "Amy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-30T06:04:08.88",
            "dislikeCount": 0,
            "id": 86525,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "lovely scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 842322,
                "likeTotal": 1,
                "reviewsTotal": 5,
                "shippedCount": 8,
                "url": "/p/842322",
                "username": "candace"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-30T03:35:42.446",
            "dislikeCount": 0,
            "id": 86476,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I really liked this perfume.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 472310,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 22,
                "url": "/p/472310",
                "username": "Michelle"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-29T23:05:55.306",
            "dislikeCount": 0,
            "id": 86349,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "a but too floral for me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 349643,
                "likeTotal": 4,
                "reviewsTotal": 7,
                "shippedCount": 10,
                "url": "/p/349643",
                "username": "Brenda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-29T14:33:08.531",
            "dislikeCount": 0,
            "id": 86121,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not really my scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 591905,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/591905",
                "username": "Elizabeth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-26T17:15:39.793",
            "dislikeCount": 0,
            "id": 85846,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Blue is a scent for.everyday, every occassion. Its not too strong but stays on all day it has a clean sent very inviting one of my favorites all time",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 392347,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 12,
                "url": "/p/392347",
                "username": "Candace"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-23T02:30:52.112",
            "dislikeCount": 0,
            "id": 85542,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "By far one of the best smelling perfume ever....LOVE IT",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 863846,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/863846",
                "username": "Krystal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-23T01:26:48.65",
            "dislikeCount": 0,
            "id": 85529,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I liked this scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 477694,
                "likeTotal": 4,
                "reviewsTotal": 5,
                "shippedCount": 11,
                "url": "/p/477694",
                "username": "Nikki"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-22T04:44:08.863",
            "dislikeCount": 0,
            "id": 85386,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Amazing scent. On my second bottle. Very light and fresh scented. Has a clean smell. Wish it lasted longer on me. Not an over powering scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/2908366301479790191386.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 930243,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 9,
                "url": "/p/930243",
                "username": "kimberly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-21T15:44:43.677",
            "dislikeCount": 0,
            "id": 85268,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Smells really great in the bottle, but once I spray it on me it has a musky smell that I am fond of...",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 889069,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 10,
                "url": "/p/889069",
                "username": "Ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-20T04:25:57.162",
            "dislikeCount": 0,
            "id": 85134,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "love it but the smell fades  quickly",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13292006981477779072082.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 894252,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/894252",
                "username": "Jo"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-20T00:03:58.952",
            "dislikeCount": 0,
            "id": 85100,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Something about the base note is a real turn-off for me. I can't quite determine what it is, though...maybe the musk or the amber. Moschino Cheap & Chic has a similar base notes, and I just can't tolerate either one.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 512319,
                "likeTotal": 11,
                "reviewsTotal": 12,
                "shippedCount": 17,
                "url": "/p/512319",
                "username": "Jana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-18T01:40:03.233",
            "dislikeCount": 0,
            "id": 84893,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This has been one of my favorites for years. It's a clean everyday scent that's amazing! Love love love! \ud83d\udc95",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 425670,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/425670",
                "username": "Tamika"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-12T00:06:26.047",
            "dislikeCount": 0,
            "id": 84430,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "One of my all time favorite scents.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9927289201471707529657.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 484748,
                "likeTotal": 4,
                "reviewsTotal": 10,
                "shippedCount": 6,
                "url": "/p/484748",
                "username": "April"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-11T15:59:19.843",
            "dislikeCount": 0,
            "id": 84290,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "An amazing light scent that lasts. Spray on clothes not body to make last longer. Love love love",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 876115,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 11,
                "url": "/p/876115",
                "username": "Karen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-11T15:29:53.945",
            "dislikeCount": 0,
            "id": 84279,
            "isUseful": null,
            "likeCount": 2,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Too strong for me little to business older women... not a favorite  guys",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 574002,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 3,
                "url": "/p/574002",
                "username": "jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-11T14:44:40.245",
            "dislikeCount": 0,
            "id": 84243,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I have not received my oct fragrance that I have been charged $14.95 for",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 339674,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/339674",
                "username": "Wanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-11T13:11:33.927",
            "dislikeCount": 0,
            "id": 84168,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Very pretty and feminine ...its a nice scent for everyday wear , very lightweight . \nMy only complaint is that it doesn't last very long.Maybe a few hours.  Need to reapply if you want a more long lasting scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8924757991476561976283.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 318741,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 11,
                "url": "/p/318741",
                "username": "Lauren"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-11T12:01:41.634",
            "dislikeCount": 0,
            "id": 84127,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It smells really good but the smell doesn't last.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 541433,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 18,
                "url": "/p/541433",
                "username": "Margaret"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-11T11:21:08.371",
            "dislikeCount": 0,
            "id": 84110,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I took a chance and tried this but for me personally I didn't like it . I thought it was to strong of a floral scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 874841,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/874841",
                "username": "Katherine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-08T19:14:29.306",
            "dislikeCount": 0,
            "id": 83837,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "A little too light for me but very pretty smelling.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 561899,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/561899",
                "username": "Kathryn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-07T20:55:39.236",
            "dislikeCount": 0,
            "id": 83527,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not my favorite...  A little old lady-ish but wearable",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 574002,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 3,
                "url": "/p/574002",
                "username": "jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-07T19:40:13.72",
            "dislikeCount": 0,
            "id": 83491,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "didn't meet the expectations",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 806696,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/806696",
                "username": "Khushbu"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-07T19:22:54.368",
            "dislikeCount": 0,
            "id": 83483,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Loved it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 405084,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 11,
                "url": "/p/405084",
                "username": "Stephanie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-07T16:39:29.152",
            "dislikeCount": 0,
            "id": 83430,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love this scent. Stays with you all day.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 873305,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/873305",
                "username": "Carla"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-07T15:41:47.633",
            "dislikeCount": 0,
            "id": 83411,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I was expecting a great aroma and was not disapointed. What disapointed me was that the sample is Eau de Toilette, instead of Parfum. This makes the aroma last only for about 3 hours after spraying. When buying Eau De Toilette in a perfume shop, it is quite affordable. But i will continue giving it a try but i don't think i will continue with the subscription much longer.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 831901,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/831901",
                "username": "mary"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-07T14:14:08.53",
            "dislikeCount": 0,
            "id": 83389,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 441299,
                "likeTotal": 4,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/441299",
                "username": "YANNI"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-07T11:48:42.392",
            "dislikeCount": 0,
            "id": 83350,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love love love",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 622244,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 24,
                "url": "/p/622244",
                "username": "Nicole"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-06T12:27:16.449",
            "dislikeCount": 0,
            "id": 83038,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love this scent. I always get compliments on it. Timeless!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 725735,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 9,
                "url": "/p/725735",
                "username": "Caitlin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-05T19:04:22.679",
            "dislikeCount": 0,
            "id": 82773,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It was ok",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 428818,
                "likeTotal": 4,
                "reviewsTotal": 11,
                "shippedCount": 28,
                "url": "/p/428818",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-05T02:55:09.377",
            "dislikeCount": 0,
            "id": 82495,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "It was a perfect scent to begin with, especially if you are use to sweet or floral sweet scent. totally recommend. It casual but can still make you feel sexy.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 789362,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/789362",
                "username": "Martha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-05T02:20:19.309",
            "dislikeCount": 0,
            "id": 82473,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                }
            ],
            "text": "It wasn't the scent that I remember smelling. \nIt was 5 years ago when I smelt it tho! Still smells great!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 819678,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 21,
                "url": "/p/819678",
                "username": "kathryn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-04T22:10:19.928",
            "dislikeCount": 0,
            "id": 82323,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 739734,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 14,
                "url": "/p/739734",
                "username": "Megan"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-04T20:37:40.689",
            "dislikeCount": 0,
            "id": 82246,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Stronger floral sent then I anticipated.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 858423,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/858423",
                "username": "Brittany"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-04T16:11:06.8",
            "dislikeCount": 0,
            "id": 81963,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Didn't care for the smell too woodsy for me",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 752290,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 10,
                "url": "/p/752290",
                "username": "Leesa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-04T16:06:04.77",
            "dislikeCount": 0,
            "id": 81951,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Holds scent very well!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 859882,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/859882",
                "username": "Megan"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-04T14:14:33.121",
            "dislikeCount": 0,
            "id": 81645,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This is a nice, light floral scent. It's a little young for me (age 46), but my young adult daughters really like it.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10104860411480701347574.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 770272,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 8,
                "url": "/p/770272",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-04T12:48:30.471",
            "dislikeCount": 0,
            "id": 81578,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "its a really nice smelling scent. it just doesn't last very long",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8104853361474475043597.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 217817,
                "likeTotal": 3,
                "reviewsTotal": 5,
                "shippedCount": 13,
                "url": "/p/217817",
                "username": "Edith"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-04T00:58:37.02",
            "dislikeCount": 0,
            "id": 81339,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I love the smell of blue . It doesn't have a strong smell to it , it's a light fragrance that you can wear all day .",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 664668,
                "likeTotal": 3,
                "reviewsTotal": 7,
                "shippedCount": 10,
                "url": "/p/664668",
                "username": "Erica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-04T00:52:41.349",
            "dislikeCount": 0,
            "id": 81333,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "It's ok. I don't  like a strong flowery sent and when you first spray it, that's what you get. It mellows out in like an hour tho then its nice.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 559132,
                "likeTotal": 2,
                "reviewsTotal": 7,
                "shippedCount": 15,
                "url": "/p/559132",
                "username": "Kenisha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T19:43:54.83",
            "dislikeCount": 0,
            "id": 81126,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Awesome",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 728441,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/728441",
                "username": "Sheryl"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T18:51:50.895",
            "dislikeCount": 0,
            "id": 81084,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 740525,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 12,
                "url": "/p/740525",
                "username": "Rose"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T18:36:15.357",
            "dislikeCount": 0,
            "id": 81068,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this scent.  Nice fresh, light, but stays with me.  I get compliments every time I wear this scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9661960951481921021073.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 737888,
                "likeTotal": 2,
                "reviewsTotal": 8,
                "shippedCount": 12,
                "url": "/p/737888",
                "username": "Amber"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T17:03:24.438",
            "dislikeCount": 0,
            "id": 80993,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I absolutely LOVE this scent. I am a little sensitive to extremely rich smells, so this scent has enough boldness to be smelt and a softness to pleasant. It has an underlining powdery smell which i am in love with. Gonna buy this one for sure!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 411725,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 12,
                "url": "/p/411725",
                "username": "Christy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T14:13:24.602",
            "dislikeCount": 0,
            "id": 80785,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Loved it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 677439,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 13,
                "url": "/p/677439",
                "username": "Brandi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T13:31:44.903",
            "dislikeCount": 0,
            "id": 80726,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 719907,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 23,
                "url": "/p/719907",
                "username": "Lori"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T12:41:41.642",
            "dislikeCount": 0,
            "id": 80651,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this scent!  For me, it lasts all day.  Nice fragrance, perfect for everyday wear.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 610475,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 12,
                "url": "/p/610475",
                "username": "Tammy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T11:51:52.933",
            "dislikeCount": 0,
            "id": 80604,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Very light scent, not overwhelming. My favorite of all!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 628362,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/628362",
                "username": "Janet"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T10:49:38.924",
            "dislikeCount": 0,
            "id": 80560,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Light, fresh and clean. Not too strong or overpowering.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 848196,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/848196",
                "username": "Lakiesha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T06:43:22.672",
            "dislikeCount": 0,
            "id": 80471,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Simple (but not cheap) office worker smell.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "male",
                "id": 796730,
                "likeTotal": 2,
                "reviewsTotal": 7,
                "shippedCount": 18,
                "url": "/p/796730",
                "username": "Matt"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T03:57:05.98",
            "dislikeCount": 0,
            "id": 80295,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I love the light, fresh, sophisticated smell of Ralph Lauren Blue but I hate the longevity! I can smell it on me for one hour tops, rather I stay indoors or venture out.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8301129801464930044207.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 37350,
                "likeTotal": 17,
                "reviewsTotal": 11,
                "shippedCount": 22,
                "url": "/p/37350",
                "username": "Nikeita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T03:48:59.966",
            "dislikeCount": 0,
            "id": 80282,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Beautiful fragrance!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-1445923131478907570994.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 475693,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 9,
                "url": "/p/475693",
                "username": "Gayle"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T03:03:09.159",
            "dislikeCount": 0,
            "id": 80224,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "When I first spray it on I don't like the smell much at all but as it melts in the smell is nice.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 815315,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/815315",
                "username": "Carmen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T02:53:35.882",
            "dislikeCount": 0,
            "id": 80207,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "LOVE this fragrance\u203c\ufe0f",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 570984,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 22,
                "url": "/p/570984",
                "username": "kathi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T02:41:35.653",
            "dislikeCount": 0,
            "id": 80187,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Looooove this perfume.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 700496,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 15,
                "url": "/p/700496",
                "username": "Christa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-03T00:22:38.065",
            "dislikeCount": 0,
            "id": 79964,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It seemed like it had been watered down it is not as strong as the one I have gotten in the store !An I can't believe I am paying 14.95 a month for something that Small I am not happy!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 514082,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/514082",
                "username": "Samantha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T21:35:43.232",
            "dislikeCount": 0,
            "id": 79649,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Smells awful",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 701485,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/701485",
                "username": "Marcia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T21:25:17.867",
            "dislikeCount": 0,
            "id": 79617,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Very clean fresh scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 242382,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/242382",
                "username": "larisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T20:53:25.596",
            "dislikeCount": 0,
            "id": 79506,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "A very clean scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17761424451450832649108.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 103390,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 19,
                "url": "/p/103390",
                "username": "Kanani"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T20:25:55.741",
            "dislikeCount": 0,
            "id": 79423,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "great smell",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 719060,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/719060",
                "username": "Lorie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T20:03:05.007",
            "dislikeCount": 0,
            "id": 79359,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "not at all what it used to smell like. Not a fan any longer!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 579322,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 11,
                "url": "/p/579322",
                "username": "julie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T19:31:36.478",
            "dislikeCount": 0,
            "id": 79264,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Smelled like a cologne for men. Super masculine.  It for me. I wonder if I received the wrong scent?",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 191650,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 18,
                "url": "/p/191650",
                "username": "Ruth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T18:31:38.841",
            "dislikeCount": 0,
            "id": 79032,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Reminds me of wild musk with a slight hint of something extra.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 829005,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/829005",
                "username": "Sarah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T18:27:56.261",
            "dislikeCount": 0,
            "id": 79020,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Definitely not for me. I thought it was a lot more of a 'clean' scent than a floral one. I will be giving this away for sure.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9729394221482618568592.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 282408,
                "likeTotal": 4,
                "reviewsTotal": 8,
                "shippedCount": 17,
                "url": "/p/282408",
                "username": "Jessie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T18:13:09.81",
            "dislikeCount": 0,
            "id": 78932,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I absolutely love this smell! I  just wish it lingered longer on you.  But overall it's fabulous.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10910242001462355243817.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 417378,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/417378",
                "username": "Bridgette"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T17:54:03.212",
            "dislikeCount": 0,
            "id": 78809,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Smells very old lady ish. Very disappointing",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 749177,
                "likeTotal": 4,
                "reviewsTotal": 5,
                "shippedCount": 26,
                "url": "/p/749177",
                "username": "Devon"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T17:50:04.078",
            "dislikeCount": 0,
            "id": 78776,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I love it, it smells wonderful.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/19415927721465750391077.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 412236,
                "likeTotal": 2,
                "reviewsTotal": 7,
                "shippedCount": 23,
                "url": "/p/412236",
                "username": "Donna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T17:45:50.54",
            "dislikeCount": 0,
            "id": 78747,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This was not a good one for me. It had an after smell that I didn't care for",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9006193001469457722218.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 365574,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 24,
                "url": "/p/365574",
                "username": "Nicole"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T17:43:59.059",
            "dislikeCount": 0,
            "id": 78728,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                }
            ],
            "text": "Scent is great.. Not only have i purchased it before but lucky me!!! It was sent to me in sept & oct!! Scent bird fail!! I emailed company & they havent made it right. Will be cancelling my $15 a month subscription. Thanks but no thanks",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 760097,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/760097",
                "username": "kim"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T17:05:17.67",
            "dislikeCount": 0,
            "id": 78614,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It smells nice, just maybe too floral for me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 371823,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/371823",
                "username": "amber"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T17:00:56.264",
            "dislikeCount": 0,
            "id": 78604,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Beautiful scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 855081,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/855081",
                "username": "Sherry"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T16:41:16.881",
            "dislikeCount": 0,
            "id": 78563,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "...",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 700597,
                "likeTotal": 2,
                "reviewsTotal": 7,
                "shippedCount": 21,
                "url": "/p/700597",
                "username": "SHARON"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T15:07:15.024",
            "dislikeCount": 0,
            "id": 78470,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I initially did not like the scent, but when I wore it a couple of times, it grew on me.  I get many compliments on it. It isn't too strong or floral, which I like.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/15783875661455895822332.jpg",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 279645,
                "likeTotal": 1,
                "reviewsTotal": 7,
                "shippedCount": 20,
                "url": "/p/279645",
                "username": "Asia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T12:11:06.432",
            "dislikeCount": 0,
            "id": 78315,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love this fragrance!! It is soft and clean and it stays a long time. I will order it again.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/18485629651474068568932.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 466169,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 4,
                "url": "/p/466169",
                "username": "Suzette"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T04:15:26.941",
            "dislikeCount": 0,
            "id": 78101,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                }
            ],
            "text": "After wearing it, it started having a manly scent to it. Did not like it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 815667,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 0,
                "url": "/p/815667",
                "username": "April"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-02T03:34:12.588",
            "dislikeCount": 0,
            "id": 78054,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It wasn't for me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 849153,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/849153",
                "username": "Brittney"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-01T23:57:47.026",
            "dislikeCount": 0,
            "id": 77644,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is great!   I really like this scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 852566,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 20,
                "url": "/p/852566",
                "username": "Karen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-11-01T18:17:42.767",
            "dislikeCount": 0,
            "id": 77199,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Smelled too mature for a 25 yr old to be wearing, maybe one day i'll like it.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20132511841449602075859.jpg",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 216404,
                "likeTotal": 8,
                "reviewsTotal": 11,
                "shippedCount": 63,
                "url": "/p/216404",
                "username": "janet"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-31T18:54:25.943",
            "dislikeCount": 0,
            "id": 76615,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This scent is not a favorite of mine...it smells better in the bottle than it does on me. It's a bit too brash of a smell for me; doesn't smell too feminine.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 819496,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 9,
                "url": "/p/819496",
                "username": "Terri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-31T17:18:03.316",
            "dislikeCount": 0,
            "id": 76575,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                }
            ],
            "text": "Love",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/21195441291467489092241.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 438283,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 10,
                "url": "/p/438283",
                "username": "Myrna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-31T02:23:15.204",
            "dislikeCount": 0,
            "id": 76404,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 566368,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/566368",
                "username": "Lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-24T23:59:03.223",
            "dislikeCount": 0,
            "id": 75229,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "LOVE this scent!  My mom wears it also.  Another reason I got it!   I love the floral notes in this though!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/725417201477353481406.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 841599,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 21,
                "url": "/p/841599",
                "username": "belinda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-24T01:25:21.512",
            "dislikeCount": 0,
            "id": 75155,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I absolutely love this perfume! Such a soft scent for everyday wear!! Will definitely be purchasing in the future. Highly recommend to everyone!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3866996231476064147928.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 845279,
                "likeTotal": 2,
                "reviewsTotal": 6,
                "shippedCount": 12,
                "url": "/p/845279",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-23T12:42:14.092",
            "dislikeCount": 0,
            "id": 75102,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I really despise 'clean' scents and this is definitely a clean scent. I will admit that of this family of scents I hate this one the least, but it really didn't do it for me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9729394221482618568592.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 282408,
                "likeTotal": 4,
                "reviewsTotal": 8,
                "shippedCount": 17,
                "url": "/p/282408",
                "username": "Jessie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-23T01:13:37.472",
            "dislikeCount": 0,
            "id": 75086,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love the clean, fresh smell",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3270501481461853988071.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 400996,
                "likeTotal": 4,
                "reviewsTotal": 5,
                "shippedCount": 7,
                "url": "/p/400996",
                "username": "Christy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-23T00:17:53.171",
            "dislikeCount": 0,
            "id": 75081,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "My favorite\u2764Fresh clean scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16462977521476153225528.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 772814,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 10,
                "url": "/p/772814",
                "username": "Rebecca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-21T20:16:15.604",
            "dislikeCount": 0,
            "id": 74934,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I absolutely love this perfume! Such a soft scent for everyday wear!! Will definitely be purchasing in the future.  Highly recommend to everyone!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3866996231476064147928.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 845279,
                "likeTotal": 2,
                "reviewsTotal": 6,
                "shippedCount": 12,
                "url": "/p/845279",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-21T14:30:24.818",
            "dislikeCount": 0,
            "id": 74901,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "very nice scent, a little flirty and playful I love it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/996410441459529598775.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 287268,
                "likeTotal": 7,
                "reviewsTotal": 6,
                "shippedCount": 13,
                "url": "/p/287268",
                "username": "Cynthia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-20T19:01:22.05",
            "dislikeCount": 0,
            "id": 74810,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Smells good!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/19215445781474387514885.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 737585,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/737585",
                "username": "Natalie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-19T13:18:46.125",
            "dislikeCount": 0,
            "id": 74655,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I like it as a day perfume because its nice and fresh. Not so much for an evening out.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 781422,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 19,
                "url": "/p/781422",
                "username": "pam"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-19T12:34:05.003",
            "dislikeCount": 0,
            "id": 74646,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I didn't hate it but definitely not for me.  Made a good little gift for a friend though.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 622137,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 22,
                "url": "/p/622137",
                "username": "lelia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-19T09:07:41.578",
            "dislikeCount": 0,
            "id": 74640,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "LOVE, LOVE, LOVE!  This 3.4 size sits on my dresser and I absolutely love it.  I don't favor musk type smells so this crisp,fresh,sweet, scent was a dream come true for me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11604689981476817729895.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 869940,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/869940",
                "username": "Tanya"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-19T06:20:29.264",
            "dislikeCount": 0,
            "id": 74634,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Not a fan of this scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 705981,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/705981",
                "username": "shawnda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-19T00:23:58.656",
            "dislikeCount": 0,
            "id": 74582,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I didnt care for this scent but I tried to wear it because it was the only perfume I had since I seem to use them up before I get my next shipment.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 629067,
                "likeTotal": 1,
                "reviewsTotal": 5,
                "shippedCount": 21,
                "url": "/p/629067",
                "username": "Tonia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-19T00:19:32.141",
            "dislikeCount": 0,
            "id": 74581,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I didn't care much for it ....I wore it because it was the only perfume I had.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 629067,
                "likeTotal": 1,
                "reviewsTotal": 5,
                "shippedCount": 21,
                "url": "/p/629067",
                "username": "Tonia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-18T23:31:22.698",
            "dislikeCount": 0,
            "id": 74574,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It's an ok scent. Not something I would buy. Although, it's light and airy it's too sweet smelling for my taste. I'm looking forward to try next month's scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 799073,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 22,
                "url": "/p/799073",
                "username": "Adrienne"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-18T20:48:20.384",
            "dislikeCount": 0,
            "id": 74553,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love love LOVE this one! By the end of the day, it smells divine. I have started to use this as my every day perfume since it arrived.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5578967401476823708192.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 294658,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 21,
                "url": "/p/294658",
                "username": "Raquel"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-17T13:33:29.924",
            "dislikeCount": 0,
            "id": 74388,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is the scent I wore daily throughout high school and college. It's such a soft, beautiful smell perfect for any woman or occasion. I got so many compliments on this scent throughout my many years of wearing it, that I still put it on sometimes just to reminisce.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3938357141476711246071.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 855343,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 10,
                "url": "/p/855343",
                "username": "Katie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-12T20:09:40.503",
            "dislikeCount": 0,
            "id": 74059,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Blue, first scent I received. It is ok, I could've picked worse but it isn't my favorite type of scent. I do wear it but I am looking forward to trying the Versace scent in October. I love the idea of getting a new scent every month without paying a hundred bucks. Not that it's not worth the money for the scent you like but for 15 bucks I get a huge variety and I am nowhere near out of Blue so the 30/day supply goes a long way past 30 days, at least for me. And the more scents you get the longer they last! Thanks scentbird",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 802020,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 20,
                "url": "/p/802020",
                "username": "christine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-12T01:16:02.211",
            "dislikeCount": 0,
            "id": 73795,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I love this service and Blue is a wonderful scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 793266,
                "likeTotal": 3,
                "reviewsTotal": 5,
                "shippedCount": 6,
                "url": "/p/793266",
                "username": "Cheryl"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-10T00:32:53.737",
            "dislikeCount": 0,
            "id": 73653,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Don't let the name fool you! Not a \"water\" scent,  it goes on a bit floral but dries to a musk.  Stays on for 3 hours.  Great fall every day scent .",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 545706,
                "likeTotal": 10,
                "reviewsTotal": 8,
                "shippedCount": 26,
                "url": "/p/545706",
                "username": "Rhonda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-08T12:55:36.644",
            "dislikeCount": 0,
            "id": 73524,
            "isUseful": null,
            "likeCount": 2,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I love this scent! It is almost like a musky baby powder ??? I love it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 748778,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/748778",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-08T01:52:21.177",
            "dislikeCount": 0,
            "id": 73467,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I can't wait too receive my new perfume.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 735242,
                "likeTotal": 7,
                "reviewsTotal": 4,
                "shippedCount": 15,
                "url": "/p/735242",
                "username": "belinda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-07T16:21:46.506",
            "dislikeCount": 0,
            "id": 73270,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "loved it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 328233,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 10,
                "url": "/p/328233",
                "username": "aubrey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-07T15:18:28.884",
            "dislikeCount": 0,
            "id": 73231,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "LOVE IT",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 720962,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 9,
                "url": "/p/720962",
                "username": "Joanna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-07T14:35:10.877",
            "dislikeCount": 0,
            "id": 73203,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I have never work but I'd love the chance",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 512383,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 18,
                "url": "/p/512383",
                "username": "peggy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-07T12:29:52.413",
            "dislikeCount": 0,
            "id": 73157,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Really like this. It is soft, clean, fresh...I can really smell the jasmine in this one. My only complaint would be that it doesn't seem to stay very long..",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/12731659881480804035129.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 698884,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 26,
                "url": "/p/698884",
                "username": "Holly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-07T01:55:05.027",
            "dislikeCount": 0,
            "id": 73072,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I love it. A little goes a long way.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 707692,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 19,
                "url": "/p/707692",
                "username": "samantha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-06T05:04:59.569",
            "dislikeCount": 0,
            "id": 72737,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It was a amazing perfume I love the scent",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7188980151473522176018.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 740637,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/740637",
                "username": "Jolene"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-06T03:41:43.325",
            "dislikeCount": 0,
            "id": 72708,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                }
            ],
            "text": "Did not like it at all!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 467591,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/467591",
                "username": "Katherine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-06T00:33:18.216",
            "dislikeCount": 0,
            "id": 72610,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This scent is very lady like and classy !!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 691925,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/691925",
                "username": "margie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-05T22:07:02.223",
            "dislikeCount": 0,
            "id": 72516,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love it !",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 620501,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 25,
                "url": "/p/620501",
                "username": "christine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-05T13:05:44.266",
            "dislikeCount": 0,
            "id": 72173,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I love this deal!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "male",
                "id": 636147,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/636147",
                "username": "Kristyn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-05T04:51:44.913",
            "dislikeCount": 0,
            "id": 72000,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "\ud83d\udc4d\ud83c\udffb\ud83d\udc4d\ud83c\udffb\ud83d\udc4d\ud83c\udffb",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 796601,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 14,
                "url": "/p/796601",
                "username": "Anastasia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-05T02:59:59.991",
            "dislikeCount": 0,
            "id": 71922,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love this",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 745068,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/745068",
                "username": "Julie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-05T00:32:56.222",
            "dislikeCount": 0,
            "id": 71667,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love this smell, it's not over powering, a light and classy scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 743948,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/743948",
                "username": "susan"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-04T23:21:25.088",
            "dislikeCount": 0,
            "id": 71525,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "My fav ever",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 681112,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/681112",
                "username": "karissa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-04T23:05:46.713",
            "dislikeCount": 0,
            "id": 71485,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Oh my goodness it smells wonderful!! It's light, floral, and very feminine. I received so many compliments every where I went. You can't go wrong with this fragrance.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-7391678781486760800735.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 675864,
                "likeTotal": 3,
                "reviewsTotal": 5,
                "shippedCount": 10,
                "url": "/p/675864",
                "username": "Gwendolyn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-04T23:04:01.46",
            "dislikeCount": 0,
            "id": 71482,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Loved my first Scentbird. It's a classy/sexy scent.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 755335,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 15,
                "url": "/p/755335",
                "username": "Janely"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-04T21:23:04.41",
            "dislikeCount": 0,
            "id": 71190,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I personally didn't like the smell  on me. It's too much",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 762280,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/762280",
                "username": "leah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-03T20:54:41.335",
            "dislikeCount": 0,
            "id": 69966,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Received my first Scentbird fragrance yesterday, Ralph Lauren Blue.  Wow! What a wonderful scent! I love the ease of the applicator too.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 793266,
                "likeTotal": 3,
                "reviewsTotal": 5,
                "shippedCount": 6,
                "url": "/p/793266",
                "username": "Cheryl"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-03T20:39:21.529",
            "dislikeCount": 0,
            "id": 69954,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Just received this today. Was supposedly shipped on Sep. 22 and received it 10/3.  Too long. Not impressed with the scent.  Will not be wearing it.  It  has an old lady smell undertone to it. This was my first (and last) perfume.  Just cancelled my subscription.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 799169,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/799169",
                "username": "Christine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-03T18:08:14.423",
            "dislikeCount": 0,
            "id": 69851,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I really liked this one. Smells heavenly.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 498677,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 22,
                "url": "/p/498677",
                "username": "Cathy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-03T15:33:53.828",
            "dislikeCount": 0,
            "id": 69767,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "beautiful fresh scent ....love it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-8348619551477409655568.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 597171,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 14,
                "url": "/p/597171",
                "username": "vanessa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-03T12:25:16.924",
            "dislikeCount": 0,
            "id": 69667,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This is not for me, it has an older lady feel to the scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16107771851473262844545.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 590096,
                "likeTotal": 6,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/590096",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-03T12:08:50.881",
            "dislikeCount": 0,
            "id": 69661,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Hands down best fragrance ever! I love Ralph Lauren and this is his best yet!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3517311881473185456230.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 601790,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 4,
                "url": "/p/601790",
                "username": "julie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-03T06:51:03.183",
            "dislikeCount": 0,
            "id": 69623,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This fragrance was not good...smelled horrible.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 532562,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 22,
                "url": "/p/532562",
                "username": "Sharene"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-03T03:20:45.103",
            "dislikeCount": 0,
            "id": 69552,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This is my absolute favorite.  And it's hard to find for purchase.  Glad scentbird has it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20802462111455998712741.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 291068,
                "likeTotal": 10,
                "reviewsTotal": 10,
                "shippedCount": 18,
                "url": "/p/291068",
                "username": "felicia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-02T20:34:40.386",
            "dislikeCount": 0,
            "id": 69374,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I love this fragrance",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 468014,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 22,
                "url": "/p/468014",
                "username": "Montressa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-02T17:10:04.404",
            "dislikeCount": 0,
            "id": 69267,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                }
            ],
            "text": "I am sorry, but I don't like this scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9964785061468684045912.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 591504,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 20,
                "url": "/p/591504",
                "username": "Mariana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-02T15:05:27.561",
            "dislikeCount": 0,
            "id": 69192,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Oh my goodness. This fragrance is my number one go to now. Fabulous fresh clean scent that isn't too heavy for every day.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "male",
                "id": 429017,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 17,
                "url": "/p/429017",
                "username": "Hope"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-02T14:54:44.335",
            "dislikeCount": 0,
            "id": 69188,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love, love ,love this scent. It is sweet, clean and delicious.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 353058,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 22,
                "url": "/p/353058",
                "username": "Michelle"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-02T13:56:31.806",
            "dislikeCount": 0,
            "id": 69155,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "love this sent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 717539,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 11,
                "url": "/p/717539",
                "username": "Juanita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-02T13:51:23.582",
            "dislikeCount": 0,
            "id": 69150,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It was ok. More like a Fall scent than a summer scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20539678301468870602643.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 438540,
                "likeTotal": 4,
                "reviewsTotal": 7,
                "shippedCount": 17,
                "url": "/p/438540",
                "username": "Nadia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-02T11:02:03.808",
            "dislikeCount": 0,
            "id": 69103,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love :))))",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 432954,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 11,
                "url": "/p/432954",
                "username": "Martha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-02T04:02:46.535",
            "dislikeCount": 0,
            "id": 69006,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Absolutely  love the smell.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10910242001462355243817.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 417378,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/417378",
                "username": "Bridgette"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-02T00:51:45.279",
            "dislikeCount": 0,
            "id": 68867,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I really do like Ralph Lauren Blue!! But my first month was Dolce Gabbana and the name of it was DOLCE and I absolutely loved it!!!! If I knew I should have ordered that through here I would have ordered it but I went to another website I hope I didn't get some kind of knock off. I'm absolutely loving scentbird!!!!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 622100,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 30,
                "url": "/p/622100",
                "username": "cindy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T22:57:48.441",
            "dislikeCount": 0,
            "id": 68765,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I love this one! It's light flowery smell.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 727941,
                "likeTotal": 0,
                "reviewsTotal": 4,
                "shippedCount": 15,
                "url": "/p/727941",
                "username": "alice"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T22:46:16.939",
            "dislikeCount": 0,
            "id": 68755,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love this",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 730737,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 16,
                "url": "/p/730737",
                "username": "Patricia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T19:30:11.468",
            "dislikeCount": 0,
            "id": 68582,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It's a light, professional scent.  Perfect for work!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 798943,
                "likeTotal": 0,
                "reviewsTotal": 5,
                "shippedCount": 23,
                "url": "/p/798943",
                "username": "Jenny"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T19:25:36.508",
            "dislikeCount": 0,
            "id": 68579,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "It's smell so good...",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/6076336431474146623906.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 735327,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 25,
                "url": "/p/735327",
                "username": "Narfi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T16:24:43.26",
            "dislikeCount": 0,
            "id": 68403,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "light ok smell, not what I'm looking for as signature or as apart of my collection of favorites",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 196089,
                "likeTotal": 1,
                "reviewsTotal": 24,
                "shippedCount": 26,
                "url": "/p/196089",
                "username": "Devondria"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T14:47:39.696",
            "dislikeCount": 0,
            "id": 68276,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love love love this",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 314202,
                "likeTotal": 1,
                "reviewsTotal": 6,
                "shippedCount": 26,
                "url": "/p/314202",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T12:46:10.212",
            "dislikeCount": 0,
            "id": 68111,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not really pleased with it. It has a very strong scent as soon as you spray it then dies down very quickly. And the smell is not that great.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 535217,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 16,
                "url": "/p/535217",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T11:18:20.465",
            "dislikeCount": 0,
            "id": 68035,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love this scent. It's very clean and refreshing!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 624929,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 6,
                "url": "/p/624929",
                "username": "melissa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T05:39:17.009",
            "dislikeCount": 0,
            "id": 67872,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love love love",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 353569,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 10,
                "url": "/p/353569",
                "username": "Susan"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T04:01:53.257",
            "dislikeCount": 0,
            "id": 67755,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I liked this perfume, although on me it seemed that the longer I wore it it smelled a little more musky.  Still debating if I want to purchase a bottle, but it didn't blow me away",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 623943,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 13,
                "url": "/p/623943",
                "username": "shawna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T03:10:23.553",
            "dislikeCount": 0,
            "id": 67673,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This smelled like something my granny would wear. Think about giving it to my mother.I wasn't happy about this months choice.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 455701,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/455701",
                "username": "Nora"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T02:20:07.969",
            "dislikeCount": 0,
            "id": 67588,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Smells like something my mother would wear. Hence why I have it to her would not recommend for anyone under the age of 50.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8770889161468419294032.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 488490,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/488490",
                "username": "Samantha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T02:11:36.571",
            "dislikeCount": 0,
            "id": 67576,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Clean scent that is light and airy.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 638226,
                "likeTotal": 2,
                "reviewsTotal": 7,
                "shippedCount": 12,
                "url": "/p/638226",
                "username": "Kayla"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-10-01T01:44:06.371",
            "dislikeCount": 0,
            "id": 67521,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "clean scent, reminds me a little of soap. Doesn't last very long",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 677740,
                "likeTotal": 5,
                "reviewsTotal": 9,
                "shippedCount": 16,
                "url": "/p/677740",
                "username": "Nicole"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-30T19:28:19.743",
            "dislikeCount": 0,
            "id": 67037,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Smell so good and I get a lot of compliments",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 71574,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 30,
                "url": "/p/71574",
                "username": "Cassandra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-30T16:57:11.81",
            "dislikeCount": 0,
            "id": 66842,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "didn't like it......can't even smell it 5 minutes after  you put it on",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 624975,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/624975",
                "username": "Roberta"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-30T01:24:40.447",
            "dislikeCount": 0,
            "id": 66609,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Super happy w/this scent. Perfect work scent, very soft. Except I got up from my desk a million times to smell myself. True story.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 551065,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 23,
                "url": "/p/551065",
                "username": "Christy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-29T13:21:37.918",
            "dislikeCount": 0,
            "id": 66270,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I love this one!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5732782091468097284642.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 573423,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 15,
                "url": "/p/573423",
                "username": "Crystal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-29T12:31:22.547",
            "dislikeCount": 0,
            "id": 66255,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I didn't care for my new sent & it was also late!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 648044,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 13,
                "url": "/p/648044",
                "username": "Dana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-28T23:30:44.997",
            "dislikeCount": 0,
            "id": 66157,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 649538,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/649538",
                "username": "Brei"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-28T17:42:47.076",
            "dislikeCount": 0,
            "id": 66121,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Musky! Not sweet.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 628053,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/628053",
                "username": "Christina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-27T21:33:05.682",
            "dislikeCount": 0,
            "id": 66031,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I love it!!!! I was really disiponted when a ran out lol",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 192725,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 15,
                "url": "/p/192725",
                "username": "Timothea"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-24T22:35:40.841",
            "dislikeCount": 0,
            "id": 65777,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I was so scared after my first sample lol! This is fantastic!! Really. The first spray is pure jasmine and I don't love jasmine and thought my husband would hate it. But he loves it! It's not a bridesmaid floral, more of a clean water floral. And it dries to not over powering clean linen and slight musk but very well done.\nAll scents smell expensive and merge well.\nI feel like a million bucks in this one!\nDefinatly an after dinner walk on the beach smiling happy scent<3",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 366656,
                "likeTotal": 5,
                "reviewsTotal": 6,
                "shippedCount": 4,
                "url": "/p/366656",
                "username": "Alicia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-24T20:59:02.407",
            "dislikeCount": 0,
            "id": 65764,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I was pretty disappointed by this one, after hearing so many good things.  I don't get ANY floral notes at all, just the musk.  And unfortunately, it's really more musty than musky.  Afraid I'll be washing this one off.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5208825231467088203581.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 342873,
                "likeTotal": 14,
                "reviewsTotal": 18,
                "shippedCount": 27,
                "url": "/p/342873",
                "username": "Ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-20T17:12:07.11",
            "dislikeCount": 0,
            "id": 65248,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Get lots of compliments at the office w this scent. Highly recommended",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 527219,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/527219",
                "username": "Zindy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-19T03:49:50.727",
            "dislikeCount": 0,
            "id": 65126,
            "isUseful": null,
            "likeCount": 3,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "This was my first subscription and was not impressed. Too musky for my liking. Maybe next one will be better! *fingers crossed*",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8132125061474256852738.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 742302,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/742302",
                "username": "Heather"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-18T04:07:47.016",
            "dislikeCount": 0,
            "id": 65086,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Not a fan,this is my first subscription and it wasn't a good choice...",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 671124,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/671124",
                "username": "Tia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-18T04:04:50.013",
            "dislikeCount": 0,
            "id": 65085,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                }
            ],
            "text": "I don't like it,smells too flowery and cheap",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 671124,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/671124",
                "username": "Tia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-16T23:53:47.949",
            "dislikeCount": 0,
            "id": 65023,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "OMG this is my all time favorite so far! I just bought a full size bottle! With be a keeper for sure.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13752854881474068155340.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 272172,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 14,
                "url": "/p/272172",
                "username": "melissa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-15T03:44:21.179",
            "dislikeCount": 0,
            "id": 64869,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Now this is my scent! It's so lovely. I just love it!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16092201301468209358652.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 584759,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/584759",
                "username": "Angel"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-13T11:53:27.611",
            "dislikeCount": 0,
            "id": 64754,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is my all-time favorite perfume. I found it almost as soon as it hit the shelf the year it was released and it's been fav ever since.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 709477,
                "likeTotal": 1,
                "reviewsTotal": 9,
                "shippedCount": 18,
                "url": "/p/709477",
                "username": "Mary"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-12T20:28:29.29",
            "dislikeCount": 0,
            "id": 64705,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This is my favorite scent. i recommend everyone spray this fragrance on and let it consume you. its light and it lasts all day on me. love love it.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9974297071479922118684.JPG",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 719203,
                "likeTotal": 9,
                "reviewsTotal": 4,
                "shippedCount": 17,
                "url": "/p/719203",
                "username": "Rebecca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-12T13:31:45.449",
            "dislikeCount": 0,
            "id": 64659,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I absolutely love this scent.  I remember I used to wear Ralph Lauren's \"lauren\" all the time.  I think I like the Blue scent the best.  Clean and refreshing.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8055579971473687154681.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 600676,
                "likeTotal": 3,
                "reviewsTotal": 8,
                "shippedCount": 13,
                "url": "/p/600676",
                "username": "Rebecca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-11T20:26:15.634",
            "dislikeCount": 0,
            "id": 64578,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "This is a wonderful perfume I been using it for years.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13357230031475356943913.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 782427,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/782427",
                "username": "JEANETTE W"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-10T16:09:22.565",
            "dislikeCount": 0,
            "id": 64268,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this scent alot!! Only wish it would last longer on the skin.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 676977,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/676977",
                "username": "Lashawnna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-09T20:09:36.57",
            "dislikeCount": 0,
            "id": 64119,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "too powdery and didnt last long felt like I smelled like a grandma in it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5252276121473444532283.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 491427,
                "likeTotal": 5,
                "reviewsTotal": 4,
                "shippedCount": 4,
                "url": "/p/491427",
                "username": "Jillian"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-08T18:56:36.307",
            "dislikeCount": 0,
            "id": 63988,
            "isUseful": null,
            "likeCount": 2,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Hated it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 554763,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/554763",
                "username": "Shawntra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-08T13:53:20.067",
            "dislikeCount": 0,
            "id": 63902,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I really like this scent, it's very light and feminine, however it doesn't last long on me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 583294,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/583294",
                "username": "samantha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-08T13:25:04.155",
            "dislikeCount": 0,
            "id": 63892,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It has a nice light scent that is not overwhelming.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 382418,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 24,
                "url": "/p/382418",
                "username": "Nicole"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-08T13:15:06.373",
            "dislikeCount": 0,
            "id": 63889,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "In love\nI wore this perfume for the first time today.  I instantly thought, where has this perfume been all my life.  It smells like a hot, sultry summer night in the south when you get a bit of a breeze carrying the smell of the garden to your porch.  The floral notes are perfect.  The undertone of musk is subtle.  When my husband smelled this on me he didn't want me to leave for work.  (Let's be honest.  That is the way we want our significant others to feel about our perfume.)  I think I see a bottle of this in my future.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 310886,
                "likeTotal": 22,
                "reviewsTotal": 13,
                "shippedCount": 21,
                "url": "/p/310886",
                "username": "Penelope"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-08T07:23:44.274",
            "dislikeCount": 0,
            "id": 63852,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "My favorite love it TY",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14822192321471372597312.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 614602,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/614602",
                "username": "Vaitia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-08T01:23:55.591",
            "dislikeCount": 0,
            "id": 63772,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 649350,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 24,
                "url": "/p/649350",
                "username": "lorena"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-07T21:55:37.168",
            "dislikeCount": 0,
            "id": 63709,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 620236,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/620236",
                "username": "Shellie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-07T20:27:03.197",
            "dislikeCount": 0,
            "id": 63686,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "At first I thought this smelled a little to \"old lady\" like but once I sprayed it on me and it had sort of a moment to settle into my skin and my natural scents it was wonderful and I really loved it!  Can't wait to get next months scent!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 633073,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 10,
                "url": "/p/633073",
                "username": "Candice"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-07T01:35:27.479",
            "dislikeCount": 0,
            "id": 63426,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Not a big fan of this scent on me. It smells masculine on me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3604628611470899176877.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 674681,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/674681",
                "username": "Lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-04T19:56:19.954",
            "dislikeCount": 0,
            "id": 62468,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I do like this perfume. A little sweet and I was hoping it would dry down to a long lasting perfume but.. it didn't last long. So onto the next one.....",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 674845,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 9,
                "url": "/p/674845",
                "username": "Vikki"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-04T12:29:41.44",
            "dislikeCount": 0,
            "id": 62325,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Im not sure how this rating business works.  I already gave a review.  In it I mentioned that I absolutely hate this scent.  Still do.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 370590,
                "likeTotal": 7,
                "reviewsTotal": 10,
                "shippedCount": 22,
                "url": "/p/370590",
                "username": "Candace"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-03T22:11:51.76",
            "dislikeCount": 0,
            "id": 62134,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "\u2764\ufe0f",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13278058171467663527929.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 555410,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/555410",
                "username": "jaime"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-03T20:28:53.128",
            "dislikeCount": 0,
            "id": 62091,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Loved it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10881370081463688473074.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 418156,
                "likeTotal": 10,
                "reviewsTotal": 9,
                "shippedCount": 24,
                "url": "/p/418156",
                "username": "Sheena"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-03T14:07:35.105",
            "dislikeCount": 0,
            "id": 61912,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Absolutely LOVED!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 297889,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/297889",
                "username": "TANYA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-03T12:52:33.495",
            "dislikeCount": 0,
            "id": 61871,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                }
            ],
            "text": "I wore Blue years ago, and this one had a different scent. Not bad, but not very pleasant. And the amount of perfume is generous!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 622017,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/622017",
                "username": "Amanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-03T08:28:05.865",
            "dislikeCount": 0,
            "id": 61825,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Familiar with the scent...love the fresh scent.  Blue normally lasts all day but the scent I received obviously was diluted since it didn't last very long at all....",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 522759,
                "likeTotal": 4,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/522759",
                "username": "Denise"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-03T05:05:00.754",
            "dislikeCount": 0,
            "id": 61778,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Fresh clean scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 558738,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/558738",
                "username": "Karen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-03T02:56:59.74",
            "dislikeCount": 0,
            "id": 61702,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This is and has been for many years my favorite smell!! Absolutely adore it!!!\ud83d\udc96",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 661995,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/661995",
                "username": "vanessa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-03T00:27:43.35",
            "dislikeCount": 0,
            "id": 61599,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Hated it.  Gave it away,  it was so bad.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 370590,
                "likeTotal": 7,
                "reviewsTotal": 10,
                "shippedCount": 22,
                "url": "/p/370590",
                "username": "Candace"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T22:38:57.655",
            "dislikeCount": 0,
            "id": 61499,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Classic.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 89603,
                "likeTotal": 0,
                "reviewsTotal": 5,
                "shippedCount": 27,
                "url": "/p/89603",
                "username": "Gabriela"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T21:32:57.011",
            "dislikeCount": 0,
            "id": 61407,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "great",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 358639,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/358639",
                "username": "Zelma"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T21:06:06.116",
            "dislikeCount": 0,
            "id": 61360,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It's ok but fades fast. Would like something a little less flower smelling.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 558076,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/558076",
                "username": "doug"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T20:21:28.244",
            "dislikeCount": 0,
            "id": 61284,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                }
            ],
            "text": "Not my type. The smell fades quickly",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/2007112331470599256777.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 518944,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/518944",
                "username": "Dianne"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T16:07:48.345",
            "dislikeCount": 0,
            "id": 60926,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Amazing loved it!!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 614717,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 25,
                "url": "/p/614717",
                "username": "Denise"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T07:42:29.953",
            "dislikeCount": 0,
            "id": 60583,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I adore this perfume but I love that you can change your mind every month!!!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1711238551471342683207.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 700973,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 10,
                "url": "/p/700973",
                "username": "Jamie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T05:03:10.06",
            "dislikeCount": 0,
            "id": 60525,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Not impressed. Pleasant, but theres nothing special here. And it doesnt last more than 30 minutes either. Two thumbs down.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 225432,
                "likeTotal": 27,
                "reviewsTotal": 20,
                "shippedCount": 18,
                "url": "/p/225432",
                "username": "Gerri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T01:34:35.031",
            "dislikeCount": 0,
            "id": 60316,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Loved it it's sweet but not overpowering",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 611182,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 24,
                "url": "/p/611182",
                "username": "Catalina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-02T01:31:39.678",
            "dislikeCount": 0,
            "id": 60310,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love the whole bouquet of flowers; however it doesn't last very long on me.  Wish it did, as I find it very pleasing.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 467901,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/467901",
                "username": "Patricia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-01T14:17:58.638",
            "dislikeCount": 0,
            "id": 59557,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I haven't received it yet",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 712938,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 20,
                "url": "/p/712938",
                "username": "lydia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-01T13:02:14.596",
            "dislikeCount": 0,
            "id": 59525,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Loved it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 507966,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 17,
                "url": "/p/507966",
                "username": "Karla"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-01T12:30:50.572",
            "dislikeCount": 0,
            "id": 59513,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I like it not sure if it is my favorite. Very floral you can really smell the gardenia. A very nice day scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16521048451480551262761.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 675228,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 8,
                "url": "/p/675228",
                "username": "Nancy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-01T05:53:05.196",
            "dislikeCount": 0,
            "id": 59447,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "one of my favorites of all time!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 668918,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/668918",
                "username": "Amy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-01T05:28:45.897",
            "dislikeCount": 0,
            "id": 59439,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love this smell",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 691878,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/691878",
                "username": "Monica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-01T02:46:26.722",
            "dislikeCount": 0,
            "id": 59359,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "One of my all time favs...anytime scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 544895,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/544895",
                "username": "Dana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-09-01T01:13:57.216",
            "dislikeCount": 0,
            "id": 59297,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Always love this Scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 564993,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 25,
                "url": "/p/564993",
                "username": "vilmary"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T22:40:22.478",
            "dislikeCount": 0,
            "id": 59178,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I'm not sure I'm a fan of this scent.  I was hoping it would be more fresh and crisp ...sorta fell flat for me",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 627026,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 18,
                "url": "/p/627026",
                "username": "Julia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T20:48:21.405",
            "dislikeCount": 0,
            "id": 59062,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Smelled like old people",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 555137,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/555137",
                "username": "Ahlena"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T20:09:31.882",
            "dislikeCount": 0,
            "id": 59017,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love, love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 713873,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 12,
                "url": "/p/713873",
                "username": "Sheila"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T20:07:08.338",
            "dislikeCount": 0,
            "id": 59013,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not a perfume I would have picked for myself if I had smelled it before but it is pretty. Reminds me of the ocean.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 593429,
                "likeTotal": 0,
                "reviewsTotal": 5,
                "shippedCount": 21,
                "url": "/p/593429",
                "username": "Whitney"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T19:59:38.048",
            "dislikeCount": 0,
            "id": 59005,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Ok",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1668181881447328957465.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 196075,
                "likeTotal": 6,
                "reviewsTotal": 6,
                "shippedCount": 30,
                "url": "/p/196075",
                "username": "Delores"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T18:45:56.684",
            "dislikeCount": 0,
            "id": 58928,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I have always LOVED this scent.....is def. in my top 5 of favorite scents for me. Is a fun, sexy smell and makes me feel younger!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-4173337501473433912323.jpg",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 702129,
                "likeTotal": 4,
                "reviewsTotal": 6,
                "shippedCount": 19,
                "url": "/p/702129",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T17:05:53.776",
            "dislikeCount": 0,
            "id": 58785,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "ABSOLUTELY love this scent! Very clean smelling.  This is my go-to everyday perfume. Always get compliments!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 706903,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/706903",
                "username": "Elizabeth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T16:12:41.083",
            "dislikeCount": 0,
            "id": 58639,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 450946,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/450946",
                "username": "Tracy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T12:44:06.153",
            "dislikeCount": 0,
            "id": 58465,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love this smell... Clean and fresh",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 471831,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 15,
                "url": "/p/471831",
                "username": "Kasandra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T03:28:20.108",
            "dislikeCount": 0,
            "id": 58289,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                }
            ],
            "text": "Really not what I expected.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10426543061470432387566.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 661835,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/661835",
                "username": "Ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-31T01:40:36.989",
            "dislikeCount": 0,
            "id": 58199,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Loved it one of my favs",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 361523,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/361523",
                "username": "Jackie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-30T17:51:56.269",
            "dislikeCount": 0,
            "id": 57601,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Blue is a light and clean floral scent.  It lasted quite a long time on my skin and clothing.  Not only did I find myself enjoying the scent throughout the day, but received compliments from numerous others.  This was a surprising favorite and I intend on purchasing full size in the future.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 106261,
                "likeTotal": 17,
                "reviewsTotal": 15,
                "shippedCount": 22,
                "url": "/p/106261",
                "username": "Joni"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-30T13:41:44.263",
            "dislikeCount": 0,
            "id": 57209,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I absolutely LOVE Ralph Lauren Blue. I had never smelled it before and was absolutely blown away. Wonderful!b",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9318481641469329397986.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 626269,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 13,
                "url": "/p/626269",
                "username": "angie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-30T13:03:22.353",
            "dislikeCount": 0,
            "id": 57137,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love it.....",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 465410,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 25,
                "url": "/p/465410",
                "username": "K.T."
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-30T11:27:46.955",
            "dislikeCount": 0,
            "id": 57033,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Nice scent. Smelled very fresh and not overpowering. Very long lasting. I could smell this on me 15 hours after spraying it! That's definitely a first for me, as perfume usually does not linger that long on my skin!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 626213,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/626213",
                "username": "Tracy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-30T03:45:15.043",
            "dislikeCount": 0,
            "id": 56840,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "I love this fragrance!!! I'd love it more if the bottle I got didn't leak..",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 491263,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 10,
                "url": "/p/491263",
                "username": "Paula"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-30T02:45:05.311",
            "dislikeCount": 0,
            "id": 56817,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Not as good as I remember but long lasting",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13093370061487727345615.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 656552,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 23,
                "url": "/p/656552",
                "username": "ANDREA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-29T22:13:16.877",
            "dislikeCount": 0,
            "id": 56718,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It smells like flowers, more of a summer scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 563943,
                "likeTotal": 10,
                "reviewsTotal": 4,
                "shippedCount": 4,
                "url": "/p/563943",
                "username": "Vanessa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-28T23:48:48.158",
            "dislikeCount": 0,
            "id": 56425,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "did not like it. was looking for a summery aquatic scent and this smell like chemicals and cleaning products",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 193137,
                "likeTotal": 12,
                "reviewsTotal": 17,
                "shippedCount": 25,
                "url": "/p/193137",
                "username": "Sarah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-28T21:34:10.811",
            "dislikeCount": 0,
            "id": 56414,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Didn't like it smells old",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/653602601468108986227.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 580153,
                "likeTotal": 3,
                "reviewsTotal": 6,
                "shippedCount": 25,
                "url": "/p/580153",
                "username": "Shanik"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-28T00:45:05.858",
            "dislikeCount": 0,
            "id": 56362,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I wanted to like this, but I didn't.  It had a chemical smell.  It was okay layered under other scents, but really, I just preferred the other scents",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 114725,
                "likeTotal": 4,
                "reviewsTotal": 6,
                "shippedCount": 25,
                "url": "/p/114725",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-27T05:13:35.123",
            "dislikeCount": 0,
            "id": 56321,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I picked this as my first order for my subscription, and i am surprised that i love it, i dont usually like the floral scent in perfumes because its too strong for my taste..but this one is pretty light. Its not to strong but still enough to where u can still smell it thru out the day. I love it and im thinking about buying the bottle :)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 413759,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/413759",
                "username": "Ruta"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-26T21:18:12.043",
            "dislikeCount": 0,
            "id": 56288,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Have always loved this scent and nothing has changed.  It's a very fresh clean smell with some floral scent as well.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/3864766571472246361345.JPG",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 396682,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 20,
                "url": "/p/396682",
                "username": "Heidi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-23T19:28:53.917",
            "dislikeCount": 0,
            "id": 55853,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I do not smell any of the notes listed or floral. maybe this product was labelled wrong and i received the wrong perfume?  this smells really cheap to me, no real smell, kinda like generic drug store perfume.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1297337471456774750540.png",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 263737,
                "likeTotal": 14,
                "reviewsTotal": 11,
                "shippedCount": 6,
                "url": "/p/263737",
                "username": "victoria"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-20T18:12:29.916",
            "dislikeCount": 0,
            "id": 55580,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I L\ud83d\udc9cVE this scent!!!! I WILL choose it again!!  ...possibly my new FAVORITE!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 651347,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 18,
                "url": "/p/651347",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-20T17:19:57.519",
            "dislikeCount": 0,
            "id": 55578,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I just received my first scent bird and I chose Ralph Lauren Blue based on the reviews. I dont usually love flower scents because they are usually too strong but this is perfect not to strong and I love the smell!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10532502341471129905956.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 575649,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/575649",
                "username": "Ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-19T22:46:10.773",
            "dislikeCount": 0,
            "id": 55507,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I just received my first ScentBird package today and I actually started with a perfume that I have loved for years.  I  love the size of the  bottle and the case that will keep it from breaking in my purse!  I can't wait to get my next bottle in September.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 645010,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/645010",
                "username": "Becky"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-15T00:16:04.398",
            "dislikeCount": 0,
            "id": 55195,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Great light airy scent. Doesn't last long but beautiful scen .",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20620232371468385376864.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 506859,
                "likeTotal": 6,
                "reviewsTotal": 13,
                "shippedCount": 19,
                "url": "/p/506859",
                "username": "Alyssa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-12T17:39:12.466",
            "dislikeCount": 0,
            "id": 55064,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "has a bad smell after settling in to your clothes.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/21317167691471023475991.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 286149,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/286149",
                "username": "Nilaris"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-11T23:36:46.973",
            "dislikeCount": 0,
            "id": 54951,
            "isUseful": null,
            "likeCount": 5,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                }
            ],
            "text": "I joined this club 2 months ago and have yet to receive 1 perfume. I believe this is a scam. And now they r asking me to review something I have never received. I emailed the company and have yet to receive a response. I give this a big fat ZERO!!!\nSigned \nUnhappy...Invisible Customer who's money you have taken and produces no product",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 611921,
                "likeTotal": 9,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/611921",
                "username": "Lawrenceen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-11T15:04:54.565",
            "dislikeCount": 0,
            "id": 54700,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Has been my fav for years \nEveryone loves it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11582293311471464758855.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 580305,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/580305",
                "username": "lynn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-11T14:42:37.272",
            "dislikeCount": 0,
            "id": 54678,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love this scent",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/19618117801468030273395.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 158070,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/158070",
                "username": "kerry"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-08T02:18:32.358",
            "dislikeCount": 0,
            "id": 54342,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I\ud83d\udc99\ud83d\udc9a\ud83d\udc9c this scent and it's very hard to find \ud83d\udc4d.I get so many compliments on it as well",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 637593,
                "likeTotal": 4,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/637593",
                "username": "melissa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-07T13:33:53.766",
            "dislikeCount": 0,
            "id": 54194,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "My favorite.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/4039557881470577333318.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 464505,
                "likeTotal": 4,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/464505",
                "username": "Tameka"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-07T12:41:38.845",
            "dislikeCount": 0,
            "id": 54188,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "My Favorite so far.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 297060,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 13,
                "url": "/p/297060",
                "username": "Brandi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-06T19:49:25.002",
            "dislikeCount": 0,
            "id": 54041,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "My favorite sent thus far.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10517381541455245506364.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 286560,
                "likeTotal": 9,
                "reviewsTotal": 6,
                "shippedCount": 3,
                "url": "/p/286560",
                "username": "Marcia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-06T18:47:42.246",
            "dislikeCount": 0,
            "id": 54032,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "InitiallyI,  didn't think I'd like it, it smelled like old ladies that use moth balls.  But after it settled some, it became very watery and fresh.  It's probably the lotus that's so fresh and clean.  I also smell lemon and melon.  I may purchase a FB, not sure yet. No regrets!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 45014,
                "likeTotal": 61,
                "reviewsTotal": 26,
                "shippedCount": 21,
                "url": "/p/45014",
                "username": "Monie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-06T17:07:06.013",
            "dislikeCount": 0,
            "id": 54007,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I love the scent and I'm glad I got it for \"on the go\"! Don't ever need a larger version.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 618723,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/618723",
                "username": "Elizabeth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-05T22:55:08.185",
            "dislikeCount": 0,
            "id": 53817,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love love love this scent!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/6916793301470437719484.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 618423,
                "likeTotal": 4,
                "reviewsTotal": 1,
                "shippedCount": 14,
                "url": "/p/618423",
                "username": "MaryAnne"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-04T17:07:44.45",
            "dislikeCount": 0,
            "id": 53202,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "love is all i can say",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 397095,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/397095",
                "username": "Zatia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-04T14:49:58.948",
            "dislikeCount": 0,
            "id": 53088,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love this perfume!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 501759,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 16,
                "url": "/p/501759",
                "username": "Leigh Ann"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-02T13:12:51.63",
            "dislikeCount": 0,
            "id": 51948,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This perfume smelled like bug spray on me. No joke. Smelled wonderful in the vial and once I sprayed it, it morphed into a totally different smell.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 364981,
                "likeTotal": 1,
                "reviewsTotal": 4,
                "shippedCount": 9,
                "url": "/p/364981",
                "username": "Courtney"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-02T11:15:27.98",
            "dislikeCount": 0,
            "id": 51907,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "It was great",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 211526,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/211526",
                "username": "Chiara"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-01T14:48:03.522",
            "dislikeCount": 0,
            "id": 51345,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 225824,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 9,
                "url": "/p/225824",
                "username": "Kristi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-08-01T12:07:42.42",
            "dislikeCount": 0,
            "id": 51226,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Loved it fragrance doesn't last that long though",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 423108,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/423108",
                "username": "Lorie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T18:19:51.597",
            "dislikeCount": 0,
            "id": 50568,
            "isUseful": null,
            "likeCount": 5,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "LOVE LOVE LOVE\u2763\u2763\u2763 Every where I go everyone ask what I am wearing, even the husbands & boyfriens that want to find it for their woman! LOVE LOVE LOVE\u2763\u2763\u2763",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 541536,
                "likeTotal": 5,
                "reviewsTotal": 5,
                "shippedCount": 27,
                "url": "/p/541536",
                "username": "Kimberly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T13:55:19.073",
            "dislikeCount": 0,
            "id": 50301,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "All time favorite perfume ever!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 526130,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/526130",
                "username": "Denise"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T11:25:36.47",
            "dislikeCount": 0,
            "id": 50169,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It's nice. Very floral, just has a little bit of an old lady smell to me",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 557890,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/557890",
                "username": "Allyson"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T10:41:13.31",
            "dislikeCount": 0,
            "id": 50152,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love \ud83d\udc98  it.  So fresh and clean.  Ralph Lauren blue is perfect scent if you want fresh and flowers",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 551715,
                "likeTotal": 8,
                "reviewsTotal": 11,
                "shippedCount": 24,
                "url": "/p/551715",
                "username": "Linda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T10:19:36.865",
            "dislikeCount": 0,
            "id": 50147,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this fragrance",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 518368,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/518368",
                "username": "Terry"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T06:20:02.65",
            "dislikeCount": 0,
            "id": 50058,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "LOVE LOVE LOVE! Need I say more?",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 447649,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/447649",
                "username": "Rebecca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T04:40:38.86",
            "dislikeCount": 0,
            "id": 49967,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "loved it, smells great",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/2508107621467291555671.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 551659,
                "likeTotal": 4,
                "reviewsTotal": 9,
                "shippedCount": 26,
                "url": "/p/551659",
                "username": "Shari"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T03:24:18.635",
            "dislikeCount": 0,
            "id": 49891,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love this one!!! It smells soo clean!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 320464,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 21,
                "url": "/p/320464",
                "username": "Erica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T01:34:42.511",
            "dislikeCount": 0,
            "id": 49762,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Its nice, dry down is lovely, feminine, doesnt have quite the staying power after a few hours",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 484023,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 9,
                "url": "/p/484023",
                "username": "Jami lyn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T01:09:24.115",
            "dislikeCount": 0,
            "id": 49730,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                }
            ],
            "text": "Too strong",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 647357,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/647357",
                "username": "Dena"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T00:42:49.783",
            "dislikeCount": 0,
            "id": 49709,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Very strong, not for me, so sad!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 482110,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 4,
                "url": "/p/482110",
                "username": "Sheri"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-31T00:20:37.599",
            "dislikeCount": 0,
            "id": 49688,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Perfect Summer Scent! My new favorite, reminds me of a field of honey suckles! And last all day...",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 527669,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/527669",
                "username": "Christina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T23:15:36.148",
            "dislikeCount": 0,
            "id": 49617,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is my all time fav\nSmells so good and clean\nI get compliments from men n women constantly",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11582293311471464758855.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 580305,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/580305",
                "username": "lynn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T22:35:18.592",
            "dislikeCount": 0,
            "id": 49596,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Don't like the scent on me very much might smell better on some one else",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 32506,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/32506",
                "username": "Rachael"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T22:26:47.926",
            "dislikeCount": 0,
            "id": 49591,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love it, it has a crisp clean scent..that's also very delicate and I love that the smell lingers.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 362027,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 22,
                "url": "/p/362027",
                "username": "Shelia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T20:49:48.08",
            "dislikeCount": 0,
            "id": 49548,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Absolutely love the scent.. So soft . Great for anytime of day .",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 568592,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 22,
                "url": "/p/568592",
                "username": "Betty"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T19:43:26.158",
            "dislikeCount": 0,
            "id": 49512,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "My boyfriend loves this perfume on me!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 90581,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 19,
                "url": "/p/90581",
                "username": "LaCree"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T16:40:03.471",
            "dislikeCount": 0,
            "id": 49396,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5859515881464232342911.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 453110,
                "likeTotal": 4,
                "reviewsTotal": 8,
                "shippedCount": 18,
                "url": "/p/453110",
                "username": "Nazaret"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T15:33:01.619",
            "dislikeCount": 0,
            "id": 49339,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This perfume has a strange lingering smell I can't put my finger on it. Not a huge fan anymore.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 540048,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/540048",
                "username": "sabrina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T12:14:31.224",
            "dislikeCount": 0,
            "id": 49172,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love it! Smell so sexy!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 337952,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 17,
                "url": "/p/337952",
                "username": "Tashina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T08:19:14.289",
            "dislikeCount": 0,
            "id": 49080,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Fresh smell",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 459275,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 13,
                "url": "/p/459275",
                "username": "Deanna"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-30T00:38:00.677",
            "dislikeCount": 0,
            "id": 48968,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this scent very feminine .",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 518368,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 8,
                "url": "/p/518368",
                "username": "Terry"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-29T16:01:22.122",
            "dislikeCount": 0,
            "id": 48725,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love this fragrance. I used it for the first time this morning. I don't know how long it will last.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 351587,
                "likeTotal": 6,
                "reviewsTotal": 6,
                "shippedCount": 20,
                "url": "/p/351587",
                "username": "Margo"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-29T15:55:53.585",
            "dislikeCount": 0,
            "id": 48721,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "love it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8334562141462558938038.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 424384,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/424384",
                "username": "VERONICA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-29T09:02:59.91",
            "dislikeCount": 0,
            "id": 48506,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                }
            ],
            "text": "Smells like old lady perfume.  Don't think I could even give this one away to someone else!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 199436,
                "likeTotal": 11,
                "reviewsTotal": 7,
                "shippedCount": 27,
                "url": "/p/199436",
                "username": "Vera"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-28T19:56:26.422",
            "dislikeCount": 0,
            "id": 48451,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Smells like a cleaning product...strong and gross. I feel jipped for the month.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 488379,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/488379",
                "username": "Chrissie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-28T19:55:40.3",
            "dislikeCount": 0,
            "id": 48450,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Maybe I have a bad batch but I found it to be very similar to a cleaning product. I feel jipped for the month.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 488379,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/488379",
                "username": "Chrissie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-26T15:08:59.193",
            "dislikeCount": 0,
            "id": 48267,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "This is my signature fragrance\n..never leave home without it! Actually, I wear it @ home as well. Get tons of compliments too from both men and womwn. My husband does not like perfume or Cologne at all, but he LOVES  BLUE on Me!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 636336,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/636336",
                "username": "Kimberly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-23T23:37:43.115",
            "dislikeCount": 0,
            "id": 48061,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Smells nice but it doesn't last long very disappointed.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 419144,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/419144",
                "username": "Vickie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-21T19:11:46.722",
            "dislikeCount": 0,
            "id": 47871,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love Love Love this scent!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20373397461466845984280.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 520589,
                "likeTotal": 7,
                "reviewsTotal": 12,
                "shippedCount": 12,
                "url": "/p/520589",
                "username": "Bobbie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-19T21:28:55.276",
            "dislikeCount": 0,
            "id": 47683,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Just received my first perfume and I LOVE LOVE LOVE IT!! \nI definitely smell jasmine first, then Gardenia and Lily-of-the-Valley.\nFloral but fresh. I could wear this everyday and maybe in the office if I don't bathe in it haha",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14630365091468963504350.jpeg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 526097,
                "likeTotal": 8,
                "reviewsTotal": 9,
                "shippedCount": 22,
                "url": "/p/526097",
                "username": "Minaka"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-19T17:20:25.586",
            "dislikeCount": 0,
            "id": 47662,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I love this perfume to the moon and back. The scent lasts a very long time. It's a light scent, but it's far from silent in it's presence. It's perfect for a breezy summer day. Love love love Blue!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/6741945291462110607197.jpg",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 340369,
                "likeTotal": 15,
                "reviewsTotal": 6,
                "shippedCount": 9,
                "url": "/p/340369",
                "username": "SHANIA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-19T03:47:55.299",
            "dislikeCount": 0,
            "id": 47635,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love it.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 614701,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/614701",
                "username": "Pat"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-17T17:41:56.347",
            "dislikeCount": 0,
            "id": 47555,
            "isUseful": null,
            "likeCount": 2,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I'm new to Scentbird and received my first scent yesterday. My immediate thought was that I disliked this perfume. It has a hint of patchouli - or something similar - that I dislike. I'm wearing it today just to give it a chance. I don't think it's appropriate for my body chemistry. It's a little \"heavy\" for me as I tend to lean towards a lighter sweeter aromas.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7789100581467594414181.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 440184,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/440184",
                "username": "Katrina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-12T22:16:59.549",
            "dislikeCount": 0,
            "id": 47331,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I thought it smells good, but its not very strong, and it has that hint of old lady smell.  But it still smells good",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14802666051464369205023.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 462752,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 16,
                "url": "/p/462752",
                "username": "Diona"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-06T20:49:12.992",
            "dislikeCount": 0,
            "id": 46556,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This is one of my favorite perfumes ever. I always get so many compliments on this.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 487469,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 14,
                "url": "/p/487469",
                "username": "Kristi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-05T16:19:01.491",
            "dislikeCount": 0,
            "id": 45987,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "perfect for any time of day....scents lasts all day, no need to reapply.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 204729,
                "likeTotal": 6,
                "reviewsTotal": 7,
                "shippedCount": 3,
                "url": "/p/204729",
                "username": "Danielle"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-05T15:58:11.67",
            "dislikeCount": 0,
            "id": 45975,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Light,airy one of my favorites.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 424832,
                "likeTotal": 5,
                "reviewsTotal": 5,
                "shippedCount": 25,
                "url": "/p/424832",
                "username": "Regina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-05T01:55:56.693",
            "dislikeCount": 0,
            "id": 45729,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The scent smells amazing! I really love it :)!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 459490,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/459490",
                "username": "Sara"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-05T00:57:56.654",
            "dislikeCount": 0,
            "id": 45693,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Smell nice...not my favorite scent. But I did receive complents on it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 245852,
                "likeTotal": 5,
                "reviewsTotal": 8,
                "shippedCount": 12,
                "url": "/p/245852",
                "username": "Kalyn"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-04T20:12:58.763",
            "dislikeCount": 0,
            "id": 45616,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "A long time favorite. An almost warm, yet clean scent. Not overwhelming, but has a little more staying power than I remembered. Still love it.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10793560811466015373109.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 493292,
                "likeTotal": 6,
                "reviewsTotal": 7,
                "shippedCount": 27,
                "url": "/p/493292",
                "username": "Jenny"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-04T14:12:55.903",
            "dislikeCount": 0,
            "id": 45547,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It has an earthy scent. Nice for going out.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 366125,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 21,
                "url": "/p/366125",
                "username": "Debra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-04T02:02:26.205",
            "dislikeCount": 0,
            "id": 45444,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It has a beautiful light scent, but it doesn't last very long. I am a bit disapointed.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1285475621466119621762.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 498285,
                "likeTotal": 6,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/498285",
                "username": "Linda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-02T21:05:04.738",
            "dislikeCount": 0,
            "id": 44917,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I really enjoy the smell.  It almost reminds me of honeysuckle, even though that scent isn't listed in the notes.  The gardenia comes through very well.  It's very crisp and clean and usable all year round.  The only reason I knocked off one heart was because I feel that it isn't super interesting/unique smelling to me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-6616556311463876474004.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 408446,
                "likeTotal": 1,
                "reviewsTotal": 5,
                "shippedCount": 12,
                "url": "/p/408446",
                "username": "Alyssa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-02T16:14:03.72",
            "dislikeCount": 0,
            "id": 44727,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Very dissapointed I did not receive the perfume I ordered and so far my emails have been ignored,will definitely cancel asap.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 450937,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/450937",
                "username": "Martha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-02T07:49:10.818",
            "dislikeCount": 0,
            "id": 44378,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I Love the smell!! It last the whole day!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 449303,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/449303",
                "username": "Nofoaga"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-02T06:27:31.239",
            "dislikeCount": 0,
            "id": 44337,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Absolutly love the smell",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9220716751479149397094.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 410898,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/410898",
                "username": "Yesenia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-02T02:51:27.589",
            "dislikeCount": 0,
            "id": 44102,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "loved it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 245465,
                "likeTotal": 1,
                "reviewsTotal": 5,
                "shippedCount": 9,
                "url": "/p/245465",
                "username": "Marisol"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-02T02:36:20.803",
            "dislikeCount": 0,
            "id": 44073,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "One of my favorites! I can't find the full size anymore, this was a nice find! Very clean & fresh smelling!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1533370901471565101631.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 332374,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 9,
                "url": "/p/332374",
                "username": "Andrea"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-02T00:29:08.936",
            "dislikeCount": 0,
            "id": 43742,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Didn't like at all",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 435691,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/435691",
                "username": "Amanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-01T23:31:13.664",
            "dislikeCount": 0,
            "id": 43589,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "My new favorite scent!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16200852701458183643585.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 314683,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 7,
                "url": "/p/314683",
                "username": "Lorraine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-01T19:46:13.577",
            "dislikeCount": 0,
            "id": 43208,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "OMGGGGGG I'm in love with this scent! So much, I'm going to buy the full size!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 204986,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/204986",
                "username": "Anjail"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-01T19:35:27.985",
            "dislikeCount": 0,
            "id": 43179,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love it!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 447246,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/447246",
                "username": "lenise"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-01T18:52:34.103",
            "dislikeCount": 0,
            "id": 43103,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love it ! Been using this perfume for years !",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 401653,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/401653",
                "username": "Patty"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-01T18:35:01.525",
            "dislikeCount": 0,
            "id": 43071,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love Blue! It has a fresh lemon scent on me which I love.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 256739,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 19,
                "url": "/p/256739",
                "username": "Grace"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-01T18:31:47.817",
            "dislikeCount": 0,
            "id": 43065,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Absolutly loved this clean fresh watery floral scent",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17806541241454455569270.jpg",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 81133,
                "likeTotal": 10,
                "reviewsTotal": 19,
                "shippedCount": 23,
                "url": "/p/81133",
                "username": "sharla"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-01T18:21:46.102",
            "dislikeCount": 0,
            "id": 43045,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I love the  floral scent I got so many compliments it smells so nice",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 297501,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/297501",
                "username": "Tina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-01T18:09:59.899",
            "dislikeCount": 0,
            "id": 43024,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Luv it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 241950,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/241950",
                "username": "Anissa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-07-01T14:53:53.098",
            "dislikeCount": 0,
            "id": 42583,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "This has always been an old favorite of mine!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17040978771465565131751.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 491511,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 17,
                "url": "/p/491511",
                "username": "Ellie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-30T20:09:43.159",
            "dislikeCount": 0,
            "id": 42284,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "OMG I just got my first scent in the mail and i Love it. i would recommend this scent to my friends...",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20373397461466845984280.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 520589,
                "likeTotal": 7,
                "reviewsTotal": 12,
                "shippedCount": 12,
                "url": "/p/520589",
                "username": "Bobbie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-30T16:06:19.166",
            "dislikeCount": 0,
            "id": 42199,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "yummy for summer",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 283462,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 22,
                "url": "/p/283462",
                "username": "Danielle"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-30T14:39:19.5",
            "dislikeCount": 0,
            "id": 42171,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love it!!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20640335161464946009843.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 358971,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 16,
                "url": "/p/358971",
                "username": "Sarah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-30T12:25:01.204",
            "dislikeCount": 0,
            "id": 42123,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 439903,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 23,
                "url": "/p/439903",
                "username": "kim"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-30T02:04:00.137",
            "dislikeCount": 0,
            "id": 42015,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love the smell! Definitely a everyday light scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/19916150861472859555994.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 508182,
                "likeTotal": 0,
                "reviewsTotal": 3,
                "shippedCount": 14,
                "url": "/p/508182",
                "username": "Twandria"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-24T12:14:27.347",
            "dislikeCount": 0,
            "id": 41387,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Too light for me",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 426540,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/426540",
                "username": "GENEVA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-22T04:48:18.998",
            "dislikeCount": 0,
            "id": 41127,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I liked this one a lot!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13901516751465846732241.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 441353,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/441353",
                "username": "mONICA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-22T03:59:58.894",
            "dislikeCount": 0,
            "id": 41121,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "disappointing, did not like this scent at all. smelled like hairspray. not fresh, aquatic and summery like i was expecting.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 193137,
                "likeTotal": 12,
                "reviewsTotal": 17,
                "shippedCount": 25,
                "url": "/p/193137",
                "username": "Sarah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-21T19:52:13.718",
            "dislikeCount": 0,
            "id": 41024,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "One of my all time favorite !! Lasts forever. So clean and refreshing.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 422547,
                "likeTotal": 10,
                "reviewsTotal": 29,
                "shippedCount": 27,
                "url": "/p/422547",
                "username": "Helen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-18T02:15:35.38",
            "dislikeCount": 0,
            "id": 40808,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "This is my most absolute all time favorite perfume in the world! It's such a sexy fragrance! I put like 3 sprays on and and everyone is like heeeey! It smells delicious, therefore you smell delicious!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 519889,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 0,
                "url": "/p/519889",
                "username": "Scentbird"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-10T15:56:24.391",
            "dislikeCount": 0,
            "id": 40533,
            "isUseful": null,
            "likeCount": 3,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Goes on clean and smells great at first but has a strong after musk smell that almost stinks. Very weird combo of clean, fresh and then funk.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 345102,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 25,
                "url": "/p/345102",
                "username": "Kristy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-09T11:50:40.831",
            "dislikeCount": 0,
            "id": 40495,
            "isUseful": null,
            "likeCount": 4,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Disappointed! To much musk for my taste. For it to be a Eau de Toilette scent  last all day.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 375488,
                "likeTotal": 4,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/375488",
                "username": "Lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-08T16:59:19.908",
            "dislikeCount": 0,
            "id": 40385,
            "isUseful": null,
            "likeCount": 5,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "love this perfume! It is my favorite so far!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 295549,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 8,
                "url": "/p/295549",
                "username": "diana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-08T02:11:14.738",
            "dislikeCount": 0,
            "id": 40252,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This refreshing scent is so different from other perfumes I own.  It is light and very tropical, not sweet and just pleasant.  I've received several compliments on it yet they have to really smell ME to enjoy the fragrance.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 449364,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/449364",
                "username": "marlene"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-07T18:35:43.71",
            "dislikeCount": 0,
            "id": 40139,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "It was okay. I like a more some my scent. Clean scent, or like a going out on the town.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 412359,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/412359",
                "username": "Kyra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-05T23:15:46.036",
            "dislikeCount": 0,
            "id": 39554,
            "isUseful": null,
            "likeCount": 0,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Loved it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/15201881331462350673459.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 410691,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/410691",
                "username": "lisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-04T20:16:14.798",
            "dislikeCount": 0,
            "id": 39247,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I loved this fragrance!! Floral and flirty, perfect for spring into summer!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 422284,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/422284",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-04T20:15:11.811",
            "dislikeCount": 0,
            "id": 39245,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this scent!!! It is perfect for Spring into summer...floral and",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 422284,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 7,
                "url": "/p/422284",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-04T19:26:42.561",
            "dislikeCount": 0,
            "id": 39229,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "One of the best I've owned",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 209528,
                "likeTotal": 5,
                "reviewsTotal": 7,
                "shippedCount": 40,
                "url": "/p/209528",
                "username": "Swethkamal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-04T18:26:06.592",
            "dislikeCount": 0,
            "id": 39217,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It has such a beautiful smell",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 398904,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/398904",
                "username": "KEISHA"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-04T16:21:10.867",
            "dislikeCount": 0,
            "id": 39187,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Hate it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 181010,
                "likeTotal": 9,
                "reviewsTotal": 8,
                "shippedCount": 5,
                "url": "/p/181010",
                "username": "Carmen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-04T00:52:16.689",
            "dislikeCount": 0,
            "id": 38970,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love it. Not too strong lite and fresh\u2764\ufe0f",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 328138,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 17,
                "url": "/p/328138",
                "username": "Angelia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T19:40:57.045",
            "dislikeCount": 0,
            "id": 38719,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This is a nice pleasant smell. I will definitely go out and buy this one. Worth the money",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11286178811452971479962.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 194950,
                "likeTotal": 6,
                "reviewsTotal": 5,
                "shippedCount": 28,
                "url": "/p/194950",
                "username": "Karen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T19:30:46.633",
            "dislikeCount": 0,
            "id": 38694,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "The smell was kind of bland. Nothing too special",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 358082,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/358082",
                "username": "marline"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T15:12:46.802",
            "dislikeCount": 0,
            "id": 38454,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Overall I liked this scent. It's good for daily wear. The only time I didn't care for it was when I was working out. It made me a bit nauseous when I was hot & sweaty, lol.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 416657,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 13,
                "url": "/p/416657",
                "username": "Amelia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T14:18:48.421",
            "dislikeCount": 0,
            "id": 38391,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I am beyond in love with this scent.  It has great staying power, is just powdery enough to be feminine and the floral isn't overpowering.  My absolute favorite so far.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/1358544181466019882366.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 340211,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 5,
                "url": "/p/340211",
                "username": "Stacy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T12:38:05.258",
            "dislikeCount": 0,
            "id": 38248,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I am madly in love with this perfume. It has such a fresh, clean and crisp smell. Perfect for work because I hate to wear sweet perfumes for work. The scent lasts all day long and I always get compliments anytime I wear it. I am definitely going to buy a full size very soon.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 356806,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 8,
                "url": "/p/356806",
                "username": "Kay"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T11:26:19.146",
            "dislikeCount": 0,
            "id": 38160,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Sweet and light. I really like this perfume.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 238913,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 16,
                "url": "/p/238913",
                "username": "LESLIE"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T11:07:26.022",
            "dislikeCount": 0,
            "id": 38144,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Light and fresh. Smells wonderful",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 253968,
                "likeTotal": 16,
                "reviewsTotal": 13,
                "shippedCount": 17,
                "url": "/p/253968",
                "username": "Jayme"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T02:17:51.087",
            "dislikeCount": 0,
            "id": 37744,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Loved it!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/20905322021443634968967.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 92974,
                "likeTotal": 12,
                "reviewsTotal": 9,
                "shippedCount": 25,
                "url": "/p/92974",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-03T00:17:26.573",
            "dislikeCount": 0,
            "id": 37458,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I wasnt sure about this RL Blue, but figured I would try it out based on other reviees and I must say that Iabsolutely love this smell. Not too strong and one spray lasts all day!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 370653,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 19,
                "url": "/p/370653",
                "username": "Heather"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-02T21:06:42.115",
            "dislikeCount": 0,
            "id": 37160,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This scent is amazing. Very light and clean smell. If you love Ralph products this is a for sure buy!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10448582901461778256653.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 407154,
                "likeTotal": 5,
                "reviewsTotal": 6,
                "shippedCount": 10,
                "url": "/p/407154",
                "username": "Tina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-02T20:32:42.12",
            "dislikeCount": 0,
            "id": 37127,
            "isUseful": null,
            "likeCount": 1,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Beautiful clean smell,very light.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 424832,
                "likeTotal": 5,
                "reviewsTotal": 5,
                "shippedCount": 25,
                "url": "/p/424832",
                "username": "Regina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-02T00:38:26.027",
            "dislikeCount": 0,
            "id": 36780,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Wasn't to sure about it at first, once it sets for a few mins it's smells wonderful!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 438380,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/438380",
                "username": "Megan"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-01T21:19:24.423",
            "dislikeCount": 0,
            "id": 36711,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "LOVE",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 304503,
                "likeTotal": 3,
                "reviewsTotal": 5,
                "shippedCount": 33,
                "url": "/p/304503",
                "username": "Amber"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-01T19:39:58.489",
            "dislikeCount": 0,
            "id": 36672,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This perfume smells amazing.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 449059,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/449059",
                "username": "Javita"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-06-01T17:00:28.008",
            "dislikeCount": 0,
            "id": 36623,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Favorite perfume, long lasting scent. Good for every day wear. Lots of compliments",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 426404,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/426404",
                "username": "MICHELLE"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-31T21:20:27.967",
            "dislikeCount": 0,
            "id": 36326,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "My name is Gerald Williams. This is a new account and I received my first fragrance, a woman's fragrance. From here on out please! Send me male fragrances. Thank you!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "male",
                "id": 426355,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/426355",
                "username": "Gerald"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-31T20:35:27.544",
            "dislikeCount": 0,
            "id": 36297,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I really liked this perfume! The only downside of the smell is that it's a little on the cologne side. Still, I loved the smell and it lasted a long time on me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 282079,
                "likeTotal": 1,
                "reviewsTotal": 5,
                "shippedCount": 10,
                "url": "/p/282079",
                "username": "Krystal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-31T17:50:44.096",
            "dislikeCount": 0,
            "id": 36219,
            "isUseful": null,
            "likeCount": 0,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-13967555681461269514730.JPG",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 321319,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/321319",
                "username": "Robert"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-31T04:26:54.578",
            "dislikeCount": 0,
            "id": 36005,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Smells amazing. A perfecy scent for summer. It's light, fresh and makes me want to go to a beach!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 190092,
                "likeTotal": 2,
                "reviewsTotal": 6,
                "shippedCount": 12,
                "url": "/p/190092",
                "username": "Bethany"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-30T15:55:49.433",
            "dislikeCount": 0,
            "id": 35879,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I didn't like the perfume.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 387408,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/387408",
                "username": "DeE Dee"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-29T16:20:07.576",
            "dislikeCount": 0,
            "id": 35697,
            "isUseful": null,
            "likeCount": 2,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I loves scent bird. The perfumes are awesome!!! I'm about to set an account up for my husband.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 411464,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 17,
                "url": "/p/411464",
                "username": "Ellyia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-27T03:00:23.495",
            "dislikeCount": 0,
            "id": 35565,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Love the FRESH scent of Blue.  Perfect for summer!  I usually like sweet scents but this one is great!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 311199,
                "likeTotal": 5,
                "reviewsTotal": 5,
                "shippedCount": 4,
                "url": "/p/311199",
                "username": "Susan"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-25T18:14:24.484",
            "dislikeCount": 0,
            "id": 35480,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I prefer this to Romance by Ralph Lauren. This is flirty, soft, sweet and a bit fruity. It just doesn't last very long on me.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/14417799231456767490664.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 272539,
                "likeTotal": 15,
                "reviewsTotal": 28,
                "shippedCount": 27,
                "url": "/p/272539",
                "username": "Leslie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-25T17:06:26.897",
            "dislikeCount": 0,
            "id": 35467,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Not really a fan of how strong and floral this one is.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/97687741460120268567.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 304294,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 1,
                "url": "/p/304294",
                "username": "Sammy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-24T14:20:11.103",
            "dislikeCount": 0,
            "id": 35355,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                }
            ],
            "text": "Sorry I had to give this perfume a 1 star. I usually love Ralph Lauren but this one was way to strong and sweet. I felt like I literally had a flower taped to my nose at work.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 314957,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/314957",
                "username": "Diana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-23T16:19:45.015",
            "dislikeCount": 0,
            "id": 35261,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I dont really love this scent. Thought I would because I like all other Ralph Lauren fragrances, but this is not one of them.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/7979424951469495453581.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 155492,
                "likeTotal": 10,
                "reviewsTotal": 8,
                "shippedCount": 29,
                "url": "/p/155492",
                "username": "Bianca"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-21T22:25:29.001",
            "dislikeCount": 0,
            "id": 35134,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This is a fresh clean smelling scent. It can go from day to night easily. It's soft, feminine, and sexy.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/4696695231462160646215.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 282601,
                "likeTotal": 5,
                "reviewsTotal": 10,
                "shippedCount": 28,
                "url": "/p/282601",
                "username": "Toni"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-21T17:10:18.764",
            "dislikeCount": 0,
            "id": 35112,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Thank you for the opportunity to try this perfume it smells so beautiful. Is there a more discounted price for this perfume I don't mind paying for what I love but right now $75 is a bit much.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/8780712461463841587496.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 360908,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 24,
                "url": "/p/360908",
                "username": "Diane"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-20T22:04:33.275",
            "dislikeCount": 0,
            "id": 35058,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I love this scent. It is my very first in my subscription and I am very pleased!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10448582901461778256653.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 407154,
                "likeTotal": 5,
                "reviewsTotal": 6,
                "shippedCount": 10,
                "url": "/p/407154",
                "username": "Tina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-19T16:25:01.663",
            "dislikeCount": 0,
            "id": 34919,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I like this perfume. Definitely something to wear when you go out on a date.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/170928331474518671152.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 395024,
                "likeTotal": 4,
                "reviewsTotal": 3,
                "shippedCount": 14,
                "url": "/p/395024",
                "username": "Aundraya"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-10T21:02:49.907",
            "dislikeCount": 0,
            "id": 34440,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": ".",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 361435,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/361435",
                "username": "Ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-09T17:03:25.272",
            "dislikeCount": 0,
            "id": 34354,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "X",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 366990,
                "likeTotal": 2,
                "reviewsTotal": 6,
                "shippedCount": 9,
                "url": "/p/366990",
                "username": "Lindsey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-09T17:02:24.683",
            "dislikeCount": 0,
            "id": 34353,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": ".",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 366990,
                "likeTotal": 2,
                "reviewsTotal": 6,
                "shippedCount": 9,
                "url": "/p/366990",
                "username": "Lindsey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-06T16:39:45.021",
            "dislikeCount": 0,
            "id": 33876,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I did not like this particular scent. Smelled more like a scent for a mature women.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 379111,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 17,
                "url": "/p/379111",
                "username": "Dominique"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-05T11:07:48.296",
            "dislikeCount": 0,
            "id": 33406,
            "isUseful": null,
            "likeCount": 1,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I never received my order and for this I'm very dissatisfied.  This is the sent I wanted most of all.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/10517381541455245506364.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 286560,
                "likeTotal": 9,
                "reviewsTotal": 6,
                "shippedCount": 3,
                "url": "/p/286560",
                "username": "Marcia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-04T23:02:16.498",
            "dislikeCount": 0,
            "id": 33256,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "<3",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/13946244181462402942630.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 321874,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/321874",
                "username": "Sara"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-04T03:18:44.116",
            "dislikeCount": 0,
            "id": 32899,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Ralph Lauren Blue Eau de Toilette is a very interesting scent.  It is definitely a strong floral scent, but strong in a good way.  I'm surprised that it's a toilette because it lasts just like a parfum.  The scent lingers.  I love sweet florals, but this is very different.  It has a clean, fresh scent.  Every time I smell it, I pick up another note.  The notes I pick up most are definitely TUBEROSE & GARDENIA (which are my all time favorites).  RL Blue is described as a \"rich floral scent featuring notes of lotus flower, peony, lilac, rose, tuberose, jasmine, gardenia and muguet.  The scent closes on notes of sandalwood, musk and Amber.\"  This has been my favorite scent since signing up for Scentbird (this is my 3rd perfume to date).  Kudos to Ralph Lauren on capturing a garden scent in a bottle.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5285571331454560392962.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 210131,
                "likeTotal": 7,
                "reviewsTotal": 3,
                "shippedCount": 5,
                "url": "/p/210131",
                "username": "T J"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-03T21:27:35.681",
            "dislikeCount": 0,
            "id": 32793,
            "isUseful": null,
            "likeCount": 0,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 319124,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/319124",
                "username": "Kyle"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-03T07:44:27.79",
            "dislikeCount": 0,
            "id": 32592,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "My all time favorite!!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/15693497901479089475916.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 377602,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/377602",
                "username": "Natali"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-01T21:41:26.145",
            "dislikeCount": 0,
            "id": 32149,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                }
            ],
            "text": "Loved the smell I end up keeping it and not giving it to my mom Hahaha check my channel on youtube It's Arrem's Day ToDay!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17753746511462138973065.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 350817,
                "likeTotal": 6,
                "reviewsTotal": 5,
                "shippedCount": 23,
                "url": "/p/350817",
                "username": "ArrEM"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-01T13:29:53.823",
            "dislikeCount": 0,
            "id": 32033,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "LOVE BLUE AND BLUE LOVES ME BACK! This is the only perfume I've ever gotten so many complements on. Even after a long days work and I can no longer smell it; people will still ask \"What type of perfum are you wearing?!\" Keeps a pep in your step throughout the day. \ud83d\ude18",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 276469,
                "likeTotal": 3,
                "reviewsTotal": 6,
                "shippedCount": 23,
                "url": "/p/276469",
                "username": "Brenia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-05-01T00:05:11.449",
            "dislikeCount": 0,
            "id": 31907,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "The smell is fresh, crisp and clean that tickles the nose but settles into a smooth smell.  A little too manly, like aftershave for a smooth operator instead of a classy woman.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "male",
                "id": 392119,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 17,
                "url": "/p/392119",
                "username": "Catherine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-30T19:14:48.531",
            "dislikeCount": 0,
            "id": 31819,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Like it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 318067,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/318067",
                "username": "Laura"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-30T18:12:55.019",
            "dislikeCount": 0,
            "id": 31798,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "plain. unfortunately smells watered down. absolutely no staying power. I have liked all other RL perfumes in the past... not so sure about this one",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 198467,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 1,
                "url": "/p/198467",
                "username": "Pamela"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-26T19:27:26.646",
            "dislikeCount": 0,
            "id": 30909,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I agree with the others that it smells more mature than what I typically go towards.  There's something off with this scent in my opinion.  I can definitely smell the floral, but it's just not the right mix for my chemistry.  If you like strong florals, this is for you.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 346818,
                "likeTotal": 0,
                "reviewsTotal": 6,
                "shippedCount": 20,
                "url": "/p/346818",
                "username": "Erin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-25T15:12:46.541",
            "dislikeCount": 0,
            "id": 30771,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This is a strong floral and leans a little bit toward a mature woman scent for me. I'm sure it would be lovely for those that like the heavy florals but I prefer something a little lighter.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 366990,
                "likeTotal": 2,
                "reviewsTotal": 6,
                "shippedCount": 9,
                "url": "/p/366990",
                "username": "Lindsey"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-24T07:08:20.948",
            "dislikeCount": 0,
            "id": 30692,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It is a very pleasurable and clean scent. I love it for a everyday. I keep smelling myself and it is really delicious!! It lasts long as well, which is a major plus for me I cannot stand a perfume to wear off a few hours after putting it on.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9887297091459241981897.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 296877,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/296877",
                "username": "Scentbird"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-23T06:13:29.361",
            "dislikeCount": 0,
            "id": 30635,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is my all time favorite!!! Nothing can replace it!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 142538,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/142538",
                "username": "Scentbird"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-08T15:56:17.827",
            "dislikeCount": 0,
            "id": 29954,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "This perfume has been my signature scent for years. I actually call it, \"Jenny Blue\" because I get compliments from everyone, everywhere o go.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 371427,
                "likeTotal": 4,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/371427",
                "username": "Jenny"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-05T10:12:07.04",
            "dislikeCount": 0,
            "id": 29261,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I liked this perfume very much.. I wear perfumes according to mood and this one FOR ME is for when I just want to have fun. Either do something sporty or just go hang out with my friends. It smells really good.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 240056,
                "likeTotal": 7,
                "reviewsTotal": 3,
                "shippedCount": 3,
                "url": "/p/240056",
                "username": "Patricia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-04T21:37:35.88",
            "dislikeCount": 0,
            "id": 29183,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "I like that it is not to strong.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9718051771481245319563.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 162431,
                "likeTotal": 1,
                "reviewsTotal": 9,
                "shippedCount": 26,
                "url": "/p/162431",
                "username": "Trisha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-03T03:15:17.045",
            "dislikeCount": 0,
            "id": 28947,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I love this perfume!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 305282,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/305282",
                "username": "Heather"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-02T22:15:33.966",
            "dislikeCount": 0,
            "id": 28920,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "gooooood!!!!!!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 330549,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/330549",
                "username": "Elisa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-01T10:53:56.148",
            "dislikeCount": 0,
            "id": 28673,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "My absolute favorite. Always complimented on my fragrance when wearing this. Feels beautiful.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 361184,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/361184",
                "username": "Maggie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-04-01T10:47:57.508",
            "dislikeCount": 0,
            "id": 28672,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Love the convenient way of getting my favorite fragrances <3",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 361184,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 4,
                "url": "/p/361184",
                "username": "Maggie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-30T13:27:58.398",
            "dislikeCount": 0,
            "id": 27945,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love this perfume, and it lasts all day long.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 293246,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 16,
                "url": "/p/293246",
                "username": "Crystal"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-30T12:38:17.605",
            "dislikeCount": 0,
            "id": 27909,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "Love, Love, Love this scent!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 237601,
                "likeTotal": 6,
                "reviewsTotal": 6,
                "shippedCount": 12,
                "url": "/p/237601",
                "username": "Lindsay"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-30T02:43:54.646",
            "dislikeCount": 0,
            "id": 27694,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I will always love Ralph Lauren Blue. I've loved it for about 9 years now, I just haven't had $90 laying around to buy another bottle.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/9915216771459305865473.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 214469,
                "likeTotal": 15,
                "reviewsTotal": 9,
                "shippedCount": 9,
                "url": "/p/214469",
                "username": "Rachel"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T22:58:01.178",
            "dislikeCount": 0,
            "id": 27349,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Blue is a beautiful fragrance, it is a very pretty, fresh scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/21392631461445182425442.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 29564,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/29564",
                "username": "Janeen"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T21:20:13.955",
            "dislikeCount": 0,
            "id": 27057,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Nice fresh scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 184334,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/184334",
                "username": "Shari"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T19:40:21.465",
            "dislikeCount": 0,
            "id": 26770,
            "isUseful": null,
            "likeCount": 4,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Very nice.  Spring night out.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 109112,
                "likeTotal": 7,
                "reviewsTotal": 7,
                "shippedCount": 26,
                "url": "/p/109112",
                "username": "Tysha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-29T18:25:49.431",
            "dislikeCount": 0,
            "id": 26539,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I love this fragrance. It is light and powdery.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 190361,
                "likeTotal": 3,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/190361",
                "username": "Deborah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-09T14:53:00.194",
            "dislikeCount": 0,
            "id": 25141,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I have been a fan of this perfume for at least 10 years! It is such a clean, feminine scent! I've had trouble finding it in stores, (unless I wanted to pay $80+ a bottle) SO glad I found it here! One of my faves!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/19030064981454376524525.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 277282,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 12,
                "url": "/p/277282",
                "username": "Tabitha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-03-01T00:01:27.295",
            "dislikeCount": 0,
            "id": 23203,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is a Wonderful scent for Spring an Summer Love It",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 146197,
                "likeTotal": 5,
                "reviewsTotal": 9,
                "shippedCount": 26,
                "url": "/p/146197",
                "username": "Bridget"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-29T14:08:59.241",
            "dislikeCount": 0,
            "id": 22742,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is one of my favorite perfumes!  It's a light scent that reminds me of summer and oceans, but I can still wear it inside because it is not overpowering.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 308465,
                "likeTotal": 6,
                "reviewsTotal": 4,
                "shippedCount": 13,
                "url": "/p/308465",
                "username": "Jessica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-06T01:09:47.744",
            "dislikeCount": 0,
            "id": 21428,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Nice.  Not my favorite.  Not a lot of staying power",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 179944,
                "likeTotal": 4,
                "reviewsTotal": 5,
                "shippedCount": 28,
                "url": "/p/179944",
                "username": "Tracy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-02T21:37:52.648",
            "dislikeCount": 0,
            "id": 20091,
            "isUseful": null,
            "likeCount": 5,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                }
            ],
            "text": "One of my fav's!!!! Love this sent!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 90621,
                "likeTotal": 15,
                "reviewsTotal": 7,
                "shippedCount": 11,
                "url": "/p/90621",
                "username": "Leslie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-02-01T22:29:08.927",
            "dislikeCount": 0,
            "id": 19408,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Not really a fan of this scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5595930611449322267019.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 228312,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 0,
                "url": "/p/228312",
                "username": "deidre"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-30T12:32:08.463",
            "dislikeCount": 0,
            "id": 18927,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                }
            ],
            "text": "H",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 203148,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 6,
                "url": "/p/203148",
                "username": "Danielle"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-29T18:33:52.483",
            "dislikeCount": 0,
            "id": 18734,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "This is a unique fragrance, smells pleasant rather long lasting - love the notes detected when first spray and how the fragrance changes throughout the day.  Will purchase a full-size",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 223451,
                "likeTotal": 4,
                "reviewsTotal": 3,
                "shippedCount": 17,
                "url": "/p/223451",
                "username": "Tammy"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-16T01:53:00.077",
            "dislikeCount": 0,
            "id": 18087,
            "isUseful": null,
            "likeCount": 3,
            "rating": 0,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Received this today hated it I think it smells like Aqua net. Out of all the 4 subscriptions I have to sentbird this is the first I can say I think they filled with something fake I hate it. Myself and hubby have 12 scents now and have loved them all this is awful what a waste!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 204856,
                "likeTotal": 4,
                "reviewsTotal": 3,
                "shippedCount": 1,
                "url": "/p/204856",
                "username": "Kim"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-04T17:28:26.617",
            "dislikeCount": 0,
            "id": 17194,
            "isUseful": null,
            "likeCount": 3,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I like the this scent, but wouldn't purchase a full bottle.  It is a great perfume for longevity.  It sprays on very strong and does not fade once it dries down.  It's a nice scent, but not one of my favorites.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 228876,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 15,
                "url": "/p/228876",
                "username": "Katie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2016-01-04T06:30:50.305",
            "dislikeCount": 0,
            "id": 17142,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 76059,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/76059",
                "username": "Kara"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-19T19:59:50.553",
            "dislikeCount": 0,
            "id": 14022,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Can't go wrong with Ralph Lauren...A lovely soft scent....",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11582543181450554930668.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 184394,
                "likeTotal": 9,
                "reviewsTotal": 7,
                "shippedCount": 17,
                "url": "/p/184394",
                "username": "Elizabeth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-29T05:02:57.731",
            "dislikeCount": 0,
            "id": 11459,
            "isUseful": null,
            "likeCount": 3,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love this soft fresh scent.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17233041821479678251324.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 51134,
                "likeTotal": 20,
                "reviewsTotal": 21,
                "shippedCount": 26,
                "url": "/p/51134",
                "username": "Taiji"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-28T04:38:03.378",
            "dislikeCount": 0,
            "id": 11404,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Nice smell.  One of my aunts faves and has grown on me as well",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 206580,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 5,
                "url": "/p/206580",
                "username": "lyndsay"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-24T20:35:48.03",
            "dislikeCount": 0,
            "id": 10687,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I love this scent, I have for a long time.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 103919,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 18,
                "url": "/p/103919",
                "username": "Jolene"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-19T03:20:33.517",
            "dislikeCount": 0,
            "id": 10299,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I love wearing this to work. its so fresh and pretty.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/-10432477921446307658958.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 156591,
                "likeTotal": 3,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/156591",
                "username": "Daniella"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-02T02:57:41.198",
            "dislikeCount": 0,
            "id": 8509,
            "isUseful": null,
            "likeCount": 4,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2050,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/summer.svg",
                    "name": "Summer"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I absolutely love this scent. I have purchased it years before so I wanted to experience it again before committing to the large size. I think it's best wearing in the summer.. It's light and crisp, very feminine.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 123660,
                "likeTotal": 6,
                "reviewsTotal": 2,
                "shippedCount": 2,
                "url": "/p/123660",
                "username": "StephaNie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-29T22:51:26.729",
            "dislikeCount": 0,
            "id": 7249,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Good perfume for the spring I guess. Doesn't last long on me though.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 170876,
                "likeTotal": 11,
                "reviewsTotal": 9,
                "shippedCount": 16,
                "url": "/p/170876",
                "username": "Dipti"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-29T16:59:41.976",
            "dislikeCount": 0,
            "id": 6879,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 2052,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/aquatic.svg",
                    "name": "Aquatic"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "This is my favorite perfume. I love how soft it is. I have tried to find a different scent and always come back to this one.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 92774,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 2,
                "url": "/p/92774",
                "username": "Cynthia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-22T15:44:11.44",
            "dislikeCount": 0,
            "id": 6442,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It's good. Not as strong as Romance. This very much reminds me of Jennifer Aniston's perfume. Very floral - jasmine whiff goes unnoticed. \nI've always wanted to try this but didn't want to spend $65 for a bottle that may or may not sit well with me. So far I like it. Staying power is low-moderate.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 170876,
                "likeTotal": 11,
                "reviewsTotal": 9,
                "shippedCount": 16,
                "url": "/p/170876",
                "username": "Dipti"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-06T13:56:22.365",
            "dislikeCount": 0,
            "id": 5548,
            "isUseful": null,
            "likeCount": 3,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Absolutely beautiful!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 83120,
                "likeTotal": 6,
                "reviewsTotal": 3,
                "shippedCount": 2,
                "url": "/p/83120",
                "username": "Susan"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-02T17:25:04.112",
            "dislikeCount": 0,
            "id": 4899,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "good quality scent, but not the scent im searching for. i would recommend trying it for yourself.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 72769,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/72769",
                "username": "Michael"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-22T17:21:39.739",
            "dislikeCount": 0,
            "id": 2828,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Ralph Lauren Blue is in that category of light happy fresh scents baby powdery scent that makes you smell nice vs. brings the boys to the yard. It will do the trick and is youthful and bright for the spring, summer, and pre or post workout. I prefer Vera Wang Sheer Vale or my favorite in the category Michael Kors. However, when I received this as a gift one year, I did not hate it and used it in the light scent rotation.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 32307,
                "likeTotal": 10,
                "reviewsTotal": 10,
                "shippedCount": 24,
                "url": "/p/32307",
                "username": "AthenaRunninginHeels"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-18T19:51:43.461",
            "dislikeCount": 0,
            "id": 2656,
            "isUseful": null,
            "likeCount": 4,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "I used to wear this is my 20s and haven't owned it since then.  20 years later,  the scent brings me back to simple times before kids and marriage and responsibilities .  It's a light scent that lasts all day, and is appropriate for any time of day.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 115956,
                "likeTotal": 21,
                "reviewsTotal": 12,
                "shippedCount": 3,
                "url": "/p/115956",
                "username": "Diana"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-01T17:49:44.861",
            "dislikeCount": 0,
            "id": 2095,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "LOVE IT",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 70731,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/70731",
                "username": "Ennidh"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-31T19:30:58.606",
            "dislikeCount": 0,
            "id": 2013,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "One of my favorites!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 87610,
                "likeTotal": 2,
                "reviewsTotal": 4,
                "shippedCount": 5,
                "url": "/p/87610",
                "username": "Ashley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T22:13:05.978",
            "dislikeCount": 0,
            "id": 1695,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it!!!!!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 105323,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 30,
                "url": "/p/105323",
                "username": "Angela"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-07T16:00:19.125",
            "dislikeCount": 0,
            "id": 741,
            "isUseful": null,
            "likeCount": 4,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Liked this perfume because it's light/summery, is not too sweet smelling, and ages well on my skin over the day.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 78216,
                "likeTotal": 4,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/78216",
                "username": "Emily"
            },
            "userAge": null
        }
    ],
    "userReview": null
}