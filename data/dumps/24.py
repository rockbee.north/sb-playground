{
    "expertReview": null,
    "rating": {
        "average": 2.7,
        "reviewCount": 12,
        "reviews": {
            "1": 3,
            "2": 3,
            "3": 2,
            "4": 3,
            "5": 1
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2015-10-06T01:44:15.884",
            "dislikeCount": 0,
            "id": 5447,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I did not like this scent. Just not my style. :) I gave it to my sister who prefers heavier musky notes and she adores it. Better luck next month!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 106953,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/106953",
                "username": "Heidi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-01T23:22:26.704",
            "dislikeCount": 0,
            "id": 4749,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Not pleased with this scent at all.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 142344,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/142344",
                "username": "Elizabeth"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-01T18:16:10.159",
            "dislikeCount": 0,
            "id": 4685,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "A little strong at first but then it mellows out and is nice!",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17368099131454436171355.jpg",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 74198,
                "likeTotal": 7,
                "reviewsTotal": 7,
                "shippedCount": 6,
                "url": "/p/74198",
                "username": "Sharon"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T14:10:41.515",
            "dislikeCount": 0,
            "id": 4238,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Now this scent is very extraordinary..when you first spray this fragrance on it comes off a little strong but once it lingers on its absolutely breathtaking..the lasting power of this perfume is pretty good it lasted a good 8 hrs on me before I had to reapply.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 36579,
                "likeTotal": 3,
                "reviewsTotal": 6,
                "shippedCount": 26,
                "url": "/p/36579",
                "username": "sophira"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-03T02:03:15.196",
            "dislikeCount": 0,
            "id": 2215,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I thought this was the Ralph Lauren summer? In the turquoise bottle... I hated it. Smelled like old lady.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 87670,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/87670",
                "username": "Emmalee"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-31T21:10:47.019",
            "dislikeCount": 0,
            "id": 2021,
            "isUseful": null,
            "likeCount": 1,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Good scent, didn't last long though.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 86075,
                "likeTotal": 5,
                "reviewsTotal": 5,
                "shippedCount": 10,
                "url": "/p/86075",
                "username": "Christi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T23:53:59.299",
            "dislikeCount": 0,
            "id": 1721,
            "isUseful": null,
            "likeCount": 2,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "It was okay nothing i'd wear normally and definitely not something i'd buy. At first spray it has an extremely strong smell that i'm not fond of.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 42905,
                "likeTotal": 2,
                "reviewsTotal": 3,
                "shippedCount": 12,
                "url": "/p/42905",
                "username": "Vanessa"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-07T20:05:29.23",
            "dislikeCount": 0,
            "id": 756,
            "isUseful": null,
            "likeCount": 4,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Scent was too strong. It smelled more like alcohol as if it was sitting too long. \ud83d\ude14",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 60055,
                "likeTotal": 13,
                "reviewsTotal": 13,
                "shippedCount": 24,
                "url": "/p/60055",
                "username": "Tonya"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T05:36:03.12",
            "dislikeCount": 0,
            "id": 350,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "pleasant smell, is suitable for daily use",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17164572481446235753383.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 36304,
                "likeTotal": 5,
                "reviewsTotal": 5,
                "shippedCount": 19,
                "url": "/p/36304",
                "username": "Sibil"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-03T18:09:01.952",
            "dislikeCount": 1,
            "id": 78,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Love this smell.  Enters the room before you",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 81271,
                "likeTotal": 4,
                "reviewsTotal": 4,
                "shippedCount": 6,
                "url": "/p/81271",
                "username": "Lindsay"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-05-26T23:10:30",
            "dislikeCount": 1,
            "id": -112,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"being a Ralph lover and &quot;blue&quot; by ralph one of my very favorite scents, I had high hopes with this one. Disappointed in this scent. Had an older feel to it and not at all airy but heavy and stayed that way for way too long.\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 45064,
                "likeTotal": 11,
                "reviewsTotal": 5,
                "shippedCount": 17,
                "url": "/p/45064",
                "username": "charlene"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-05-20T17:53:23",
            "dislikeCount": 0,
            "id": -488,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I like the smell but it reminds me of the mall I'm 1992 or something I can't put my finger on it I probably won't be purchasing this perfume",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 3,
                "gender": "female",
                "id": 64203,
                "likeTotal": 7,
                "reviewsTotal": 10,
                "shippedCount": 7,
                "url": "/p/64203",
                "username": "odessa"
            },
            "userAge": null
        }
    ],
    "userReview": null
}