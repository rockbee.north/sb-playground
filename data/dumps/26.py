{
    "expertReview": null,
    "rating": {
        "average": 3.8,
        "reviewCount": 4,
        "reviews": {
            "1": 0,
            "2": 1,
            "3": 0,
            "4": 2,
            "5": 1
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2015-08-04T11:58:08.864",
            "dislikeCount": 1,
            "id": 397,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2044,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/vacation.svg",
                    "name": "Vacation"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Waaay too masculine on me, and not in a good way. Smells like fancy grandfather. The dry down is tolerable though--a bit sweeter than the initial burst of alcohol and spice.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/11925721401443712022732.jpg",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 54565,
                "likeTotal": 10,
                "reviewsTotal": 4,
                "shippedCount": 11,
                "url": "/p/54565",
                "username": "Marisola"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-07-31T12:00:07.245",
            "dislikeCount": 0,
            "id": 11,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Scentbird recommended this to me, and I was pretty shocked since I don't think of myself as liking either patchouli or tobacco. But I was intrigued, so I tried it. I like it much more than I thought I would-- the patchouli is very mild, almost more like a rosewood scent, and the tobacco is well-blended with the spices. I get some of the masculine edge to it (that woodiness is a little reminiscent of an aftershave) but I think it's not too masculine for women to wear, especially women like me who don't mind a little androgyny! The overall profile is very sophisticated and classic, to the point that it reminds me a little (but not too much) of my grandmother's perfumes. I think it suits the style of a quirky woman who loves vintage styles (that would be me!). It has nice throw, and it's very long-lasting. I recommend only one small spritz, and not on your wrists-- when I first tested it, I put some on my wrist and the smell was too intense every time I brought my hand up to my face. On my neck, it's just powerful enough for me to enjoy it, and I imagine it's not too overpowering to people who aren't standing too close to me!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 75722,
                "likeTotal": 64,
                "reviewsTotal": 26,
                "shippedCount": 33,
                "url": "/p/75722",
                "username": "Alysson"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-03-22T09:51:07",
            "dislikeCount": 1,
            "id": -278,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"I can see another reviewer's point about keeping this away from your boyfriend, even though he'd probably smell fantastic in it too! It's a heavy scent, very sexy and sensual. I feel.. powerful when I wear it. It brings to mind an image of a 50's style woman in silk stockings, red lipstick and cat eyes poured into a pair of red stilettos.. the kind of hot dame who's always trouble in those old private eye movies. Classier than Jessica Rabbit, but just as sexy. Definitely date night perfume. Not something you throw on for a walk on the beach, unless you want to make your own waves!\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 44359,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 11,
                "url": "/p/44359",
                "username": "kayley"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-02-24T22:22:56",
            "dislikeCount": 0,
            "id": -315,
            "isUseful": null,
            "likeCount": 2,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"While this scent is spicy and sweet I definitely see this leaning towards a more manly smell. Be aware that when you get this, your boyfriend might end up wearing it (like mine did).\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 38760,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 24,
                "url": "/p/38760",
                "username": "Krisnelly"
            },
            "userAge": null
        }
    ],
    "userReview": null
}