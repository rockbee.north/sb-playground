{
    "expertReview": null,
    "rating": {
        "average": 3.3,
        "reviewCount": 18,
        "reviews": {
            "1": 2,
            "2": 5,
            "3": 2,
            "4": 3,
            "5": 6
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2015-10-30T15:21:00.345",
            "dislikeCount": 0,
            "id": 7980,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I wish I liked this:(. Alas, it's not for me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 50817,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 7,
                "url": "/p/50817",
                "username": "Kimberly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-30T02:46:41.701",
            "dislikeCount": 0,
            "id": 7701,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Its a beautiful scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 31243,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 3,
                "url": "/p/31243",
                "username": "Debra"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-29T20:35:26.018",
            "dislikeCount": 0,
            "id": 7159,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I really like this scent. It's fun, flirty, and unique, but still okay for an office setting. A little sweet, but not overtly immature.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/5818854971478136946140.jpg",
                "dislikeTotal": 1,
                "gender": "female",
                "id": 106445,
                "likeTotal": 22,
                "reviewsTotal": 13,
                "shippedCount": 29,
                "url": "/p/106445",
                "username": "Natalie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-10T14:30:59.13",
            "dislikeCount": 0,
            "id": 5917,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I really like this scent! It has become an everyday favorite for me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 29446,
                "likeTotal": 2,
                "reviewsTotal": 2,
                "shippedCount": 12,
                "url": "/p/29446",
                "username": "Shaneice"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T02:09:55.52",
            "dislikeCount": 0,
            "id": 3832,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I really love this scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 33847,
                "likeTotal": 1,
                "reviewsTotal": 7,
                "shippedCount": 11,
                "url": "/p/33847",
                "username": "Shaneice"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T00:45:55.377",
            "dislikeCount": 0,
            "id": 3630,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 75397,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/75397",
                "username": "Tabby"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-29T23:07:59.087",
            "dislikeCount": 0,
            "id": 3316,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Nothing special to me. I liked it, but would not buy it again.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 130886,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/130886",
                "username": "Jennifer"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-20T18:20:06.55",
            "dislikeCount": 0,
            "id": 2736,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 33,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sweet.svg",
                    "name": "Sweet"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2051,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/winter.svg",
                    "name": "Winter"
                }
            ],
            "text": "Very sweet, synthetic, and juvenile.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/492883201452054139589.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 104286,
                "likeTotal": 3,
                "reviewsTotal": 6,
                "shippedCount": 16,
                "url": "/p/104286",
                "username": "Samantha"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-11T20:36:45.145",
            "dislikeCount": 0,
            "id": 2568,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 33,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sweet.svg",
                    "name": "Sweet"
                },
                {
                    "id": 2040,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/date_night.svg",
                    "name": "Date Night"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "love love  love",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/17739941251446140090124.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 81062,
                "likeTotal": 10,
                "reviewsTotal": 6,
                "shippedCount": 13,
                "url": "/p/81062",
                "username": "Holly"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T16:11:01.938",
            "dislikeCount": 0,
            "id": 1535,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 5,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sexy.svg",
                    "name": "Sexy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": ":-)",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 74690,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 11,
                "url": "/p/74690",
                "username": "Marishia"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T08:32:46.062",
            "dislikeCount": 0,
            "id": 1270,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Great scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 105175,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 11,
                "url": "/p/105175",
                "username": "Fran"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-29T04:49:24.103",
            "dislikeCount": 0,
            "id": 1242,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "love it!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 60623,
                "likeTotal": 5,
                "reviewsTotal": 3,
                "shippedCount": 7,
                "url": "/p/60623",
                "username": "Ami"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-20T00:11:57.093",
            "dislikeCount": 0,
            "id": 959,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 33,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sweet.svg",
                    "name": "Sweet"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "I want to change my rating. Too powdery and old lady smelling once it dries down.",
            "title": "old lady smelling",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 82877,
                "likeTotal": 10,
                "reviewsTotal": 11,
                "shippedCount": 7,
                "url": "/p/82877",
                "username": "Jordin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-20T00:11:23.446",
            "dislikeCount": 0,
            "id": 958,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I want to change my rating. Too powdery and old lady smelling once it dries down.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 82877,
                "likeTotal": 10,
                "reviewsTotal": 11,
                "shippedCount": 7,
                "url": "/p/82877",
                "username": "Jordin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-19T23:31:52.389",
            "dislikeCount": 0,
            "id": 953,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 58,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/elegant.svg",
                    "name": "Elegant"
                },
                {
                    "id": 2043,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/party.svg",
                    "name": "Party"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I like it, but I don't love it. It is very unique which I love, it's got a smoky, musky, but yet very sweet. It almost reminds me of lipstick. Weird way to explain it. Hopefully it will grow on me. I feel it is a good transition scent into fall.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 82877,
                "likeTotal": 10,
                "reviewsTotal": 11,
                "shippedCount": 7,
                "url": "/p/82877",
                "username": "Jordin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-19T23:30:41.593",
            "dislikeCount": 0,
            "id": 952,
            "isUseful": null,
            "likeCount": 2,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I like it, but I don't love it. It is very unique which I love, it's got a smoky, musky, but yet very sweet. It almost reminds me of lipstick. Weird way to explain it. Hopefully it will grow on me. I feel it is a good transition scent into fall.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 82877,
                "likeTotal": 10,
                "reviewsTotal": 11,
                "shippedCount": 7,
                "url": "/p/82877",
                "username": "Jordin"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T13:42:05.348",
            "dislikeCount": 3,
            "id": 439,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 132,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/woody.svg",
                    "name": "Woody"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "it's so so, I need the type of fragrance that makes me want to smell myself... this just didn't do that...",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 3,
                "gender": "female",
                "id": 79561,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 1,
                "url": "/p/79561",
                "username": "Fatimah"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-05-29T21:49:43",
            "dislikeCount": 3,
            "id": -19,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 33,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/sweet.svg",
                    "name": "Sweet"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "\"The perfume is so strong, it's actually overwhelming and not in a good way. It reminds me of when you walk into a Bath and Body Works and all of the candy scents hit you at once. It's a perfume for a teenager.\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 3,
                "gender": "female",
                "id": 48060,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 8,
                "url": "/p/48060",
                "username": "Alice"
            },
            "userAge": null
        }
    ],
    "userReview": null
}