{
    "expertReview": null,
    "rating": {
        "average": 3.3,
        "reviewCount": 20,
        "reviews": {
            "1": 4,
            "2": 4,
            "3": 2,
            "4": 3,
            "5": 7
        }
    },
    "reviews": [
        {
            "ageCategory": null,
            "date": "2015-12-02T17:32:40.001",
            "dislikeCount": 0,
            "id": 12213,
            "isUseful": null,
            "likeCount": 0,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2042,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/office.svg",
                    "name": "Office"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "didnt really like it too fruity",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/166753751449077564210.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 84476,
                "likeTotal": 4,
                "reviewsTotal": 5,
                "shippedCount": 4,
                "url": "/p/84476",
                "username": "Katherine"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-12-02T03:24:01.19",
            "dislikeCount": 0,
            "id": 12131,
            "isUseful": null,
            "likeCount": 0,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 147718,
                "likeTotal": 1,
                "reviewsTotal": 3,
                "shippedCount": 2,
                "url": "/p/147718",
                "username": "Grace"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-11-02T14:06:06.002",
            "dislikeCount": 0,
            "id": 8557,
            "isUseful": null,
            "likeCount": 0,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 48,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/flirty.svg",
                    "name": "Flirty"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "I liked the strength and long lasting of this perfume but I found it to be too fruity and too sweet for me to wear.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 113902,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 20,
                "url": "/p/113902",
                "username": "Catalina"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-30T12:24:02.414",
            "dislikeCount": 0,
            "id": 7900,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 185,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/citrusy.svg",
                    "name": "Citrusy"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Nice fresh scent!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 52641,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 26,
                "url": "/p/52641",
                "username": "Vivian"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-08T21:09:21.367",
            "dislikeCount": 0,
            "id": 5882,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "OMG! My favorite of all of them!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 62895,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 12,
                "url": "/p/62895",
                "username": "Darling"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-02T19:00:19.049",
            "dislikeCount": 0,
            "id": 4924,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Not what I was expecting after reading the description. It smells like more of an \"old lady\" scent to me, not horrible, but it's not for me. It's also really really strong when you spray it, almost a little too strong.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 103919,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 18,
                "url": "/p/103919",
                "username": "Jolene"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-10-01T12:20:50.869",
            "dislikeCount": 0,
            "id": 4594,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Love it!!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 88514,
                "likeTotal": 0,
                "reviewsTotal": 2,
                "shippedCount": 3,
                "url": "/p/88514",
                "username": "Lexi"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T22:10:15.234",
            "dislikeCount": 0,
            "id": 4471,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "It smell nice, but doesn't last long on me.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 150908,
                "likeTotal": 6,
                "reviewsTotal": 6,
                "shippedCount": 11,
                "url": "/p/150908",
                "username": "Jenny"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T21:00:25.196",
            "dislikeCount": 0,
            "id": 4450,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "This perfume smells beautiful and last all day",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 166719,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 6,
                "url": "/p/166719",
                "username": "Gale"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-30T00:15:32.359",
            "dislikeCount": 0,
            "id": 3519,
            "isUseful": null,
            "likeCount": 1,
            "rating": 4,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "My favorite one I have gotten so far. It is unique and interesting.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 80647,
                "likeTotal": 7,
                "reviewsTotal": 2,
                "shippedCount": 4,
                "url": "/p/80647",
                "username": "Janie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-08T16:27:41.345",
            "dislikeCount": 0,
            "id": 2522,
            "isUseful": null,
            "likeCount": 4,
            "rating": 3,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                },
                {
                    "id": 21,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fresh.svg",
                    "name": "Fresh"
                }
            ],
            "text": "Nice scent, starts out very fruity, settles into a soft floral.  Not very long lasting sillage, though.  Good for Spring and Summer.",
            "title": "",
            "user": {
                "avatar": "https://cdn.scentbird.com/user/fullsize/16585312071452636147608.jpg",
                "dislikeTotal": 0,
                "gender": "female",
                "id": 83072,
                "likeTotal": 11,
                "reviewsTotal": 7,
                "shippedCount": 8,
                "url": "/p/83072",
                "username": "Heather"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-09-02T06:23:13.503",
            "dislikeCount": 0,
            "id": 2153,
            "isUseful": null,
            "likeCount": 0,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 53,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fruity.svg",
                    "name": "Fruity"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                },
                {
                    "id": 2049,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spring.svg",
                    "name": "Spring"
                }
            ],
            "text": "Didn't care for this scent very much. It didn't wear well on me at all.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 69385,
                "likeTotal": 3,
                "reviewsTotal": 3,
                "shippedCount": 6,
                "url": "/p/69385",
                "username": "Erica"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-30T14:12:16.758",
            "dislikeCount": 0,
            "id": 1818,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I love it",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 57647,
                "likeTotal": 0,
                "reviewsTotal": 1,
                "shippedCount": 4,
                "url": "/p/57647",
                "username": "Amanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-25T18:49:12.305",
            "dislikeCount": 0,
            "id": 1096,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "really didn't like the scent",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 71816,
                "likeTotal": 3,
                "reviewsTotal": 4,
                "shippedCount": 19,
                "url": "/p/71816",
                "username": "Jamie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-07T15:38:34.729",
            "dislikeCount": 0,
            "id": 739,
            "isUseful": null,
            "likeCount": 0,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "Loved this! Very light, fresh, and fruity with hints of floral!",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 1,
                "gender": "female",
                "id": 31118,
                "likeTotal": 1,
                "reviewsTotal": 2,
                "shippedCount": 36,
                "url": "/p/31118",
                "username": "Nicole"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T13:54:31.149",
            "dislikeCount": 0,
            "id": 447,
            "isUseful": null,
            "likeCount": 2,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 117,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/spicy.svg",
                    "name": "Spicy"
                },
                {
                    "id": 2041,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/everyday.svg",
                    "name": "Everyday"
                },
                {
                    "id": 2047,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/fall.svg",
                    "name": "Fall"
                }
            ],
            "text": "Smells a bit like incense at first, but then it just smells nicely spicy",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 73373,
                "likeTotal": 2,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/73373",
                "username": "Marie"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-08-04T03:55:07.509",
            "dislikeCount": 0,
            "id": 323,
            "isUseful": null,
            "likeCount": 1,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [
                {
                    "id": 36,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/floral.svg",
                    "name": "Floral"
                },
                {
                    "id": 2045,
                    "img": "https://cdn.scentbird.com/assets/rate-icons-svg/workout.svg",
                    "name": "Workout"
                }
            ],
            "text": "If this was the intended July scent, it is not the one I received how often does this happen?",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 80263,
                "likeTotal": 1,
                "reviewsTotal": 1,
                "shippedCount": 5,
                "url": "/p/80263",
                "username": "Mary"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-05-29T23:02:17",
            "dislikeCount": 0,
            "id": -113,
            "isUseful": null,
            "likeCount": 1,
            "rating": 2,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"It leaves me wanting more. It is lovely in the vial, but it comes out very fruity and I didn't get any of the middle or finishing notes. All I could smell was fruity. Not a winner for me.\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 0,
                "gender": "female",
                "id": 64284,
                "likeTotal": 2,
                "reviewsTotal": 5,
                "shippedCount": 7,
                "url": "/p/64284",
                "username": "Adrian"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-05-23T20:09:09",
            "dislikeCount": 1,
            "id": -105,
            "isUseful": null,
            "likeCount": 2,
            "rating": 1,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "\"This is the craziest thing ever! Sniffing the vial this is great. Spritzing it - on my skin or even on another surface (shirt, jacket, etc) - turns it into a bewildering sweaty armpit smell! It's crazy. And I'm so confused. The description is dreamy but the application smells like dirt and unwashed sweaty armpit. I don't get it.\"",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 2,
                "gender": "female",
                "id": 47712,
                "likeTotal": 18,
                "reviewsTotal": 4,
                "shippedCount": 9,
                "url": "/p/47712",
                "username": "Amanda"
            },
            "userAge": null
        },
        {
            "ageCategory": null,
            "date": "2015-05-22T00:26:04",
            "dislikeCount": 1,
            "id": -316,
            "isUseful": null,
            "likeCount": 1,
            "rating": 5,
            "replies": [],
            "skinType": null,
            "tags": [],
            "text": "I am in love with this perfume! Smells fresh and sensual. It gives you that sophisticated classy smell. I have added it to my list! Will be definitely buying the big size bottle. Can't go wrong with this one.",
            "title": "",
            "user": {
                "avatar": null,
                "dislikeTotal": 2,
                "gender": "female",
                "id": 52125,
                "likeTotal": 7,
                "reviewsTotal": 4,
                "shippedCount": 36,
                "url": "/p/52125",
                "username": "Krissyl"
            },
            "userAge": null
        }
    ],
    "userReview": null
}