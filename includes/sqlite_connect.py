# SQLite connect and data read

import sqlite3

path = "../reviewsx.db"

# Create the connection
connect_sqlite = sqlite3.connect(path)

# create the dataframe from a query
data = pd.read_sql_query("SELECT * FROM reviews", connect_sqlite)