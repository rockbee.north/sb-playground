import csv
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from recommender import routes, models




###################################################




templist = []

def replacer(incoming_text):
    skips = [".", ", ", ":", ";", "'", '"', "'", "!"]
    for ch in skips: 
        incoming_text = incoming_text.replace(ch, "")
    return incoming_text

def exists(incoming):
    if incoming == "":
        return 0
    else:
        return incoming


def csv_to_dictionary():
    with open("data/product_reviews_edit.csv", "r") as f:
        csvreader = csv.reader(f, delimiter=";") # , quotechar='"'
        for row in csvreader:
            newrow = {
                "id": int(row[0]),
                "perfume_id" : int(row[1]),
                "age_category" : f"{row[2]}",
                "review_date" : f"{row[3]}",
                # "review_text" : f"{replacer(row[4])}",
                "review_text" : f"{row[4]}",
                "title" : f"{row[5]}",
                "skin_type" : f"{row[6]}",
                "rate" : int(exists(row[7])),
                "user_gender" : f"{row[8]}",
                "like_count" : row[9],
                "dislike_count" : row[10],
                "tags" : f"{row[11]}",
                "replies" : f"{row[12]}",
            }
            templist.append(newrow)
        print("I'm done with csv to dictionary")
    return templist

def add_to_database(dictionary):
    for item in dictionary:
        data = models.Reviews(**item)
        db.session.add(data)
        print(f"Added item")
        db.session.commit()

# csv_to_dictionary()
# add_to_database(templist)