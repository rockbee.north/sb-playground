import csv
from datetime import datetime

from recommender import db

class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), index=True, unique=False, nullable=True)
    brand = db.Column(db.String(120), index=True, unique=False, nullable=True)
    img = db.Column(db.String(255), index=True, unique=False, nullable=True)
    product_type = db.Column(db.String(80), index=True, unique=False, nullable=True)
    sex = db.Column(db.String(80), index=True, unique=False, nullable=True)
    description = db.Column(db.Text, index=True, unique=False, nullable=True)
    note_names = db.Column(db.String, index=True, unique=False, nullable=True)
    product_reviews = db.relationship("Reviews", backref="product", lazy="dynamic")

    def __repr__(self):
        return f"<Product {self.id}: {self.product_type} by {self.brand} ({self.name})>"

class Reviews(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    perfume_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    age_category = db.Column(db.String(140), nullable=True)
    # review_date = db.Column(db.DateTime, default=datetime.utcnow, nullable=True)
    review_date = db.Column(db.String(200))
    review_text = db.Column(db.Text, nullable=True)
    title = db.Column(db.Text, nullable=True)
    skin_type = db.Column(db.String(80), nullable=True)
    rate = db.Column(db.Integer, nullable=True)
    user_gender = db.Column(db.String(12), nullable=True)
    like_count = db.Column(db.Integer, nullable=True)
    dislike_count = db.Column(db.Integer, nullable=True)
    tags = db.Column(db.Text, nullable=True)
    replies = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return f"<Review {self.id}, {self.rate}*: {self.title}\n{self.review_text} \n{self.review_date}>"
